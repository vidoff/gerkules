var gulp = require('gulp'),                                        // Сборщик Gulp
    // del = require('del'),                                       // Удаление файлов и папок
    gutil = require('gulp-util'),                                  // Утилиты для Gulp плагинов 
    plumber = require('gulp-plumber'),                             // Обработчик ошибок сборщика
    path = require('path'),                                        // Утилита для работы с путями
    concat = require('gulp-concat'),                               // Объединяет файлы в один бандл
    cleanCSS = require('gulp-clean-css'),                          // Сжимает css
    uglify = require('gulp-uglify'),                               // Сжимает js
    // babelify = require('gulp-babel'),
    rename = require('gulp-rename'),                               // Переименовывает
    sourcemaps = require('gulp-sourcemaps'),                       // Генерация Source-maps
    stylelint = require('stylelint'),                              // Линтинг стилей
    eslint = require('gulp-eslint'),                               // Линтинг скриптов
    browserSync = require('browser-sync');                         // Подключаем Browser Sync

    // ES6
var browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    watchify = require('watchify'),
    assign = require('lodash.assign');

    // PostCSS
var postcss = require('gulp-postcss'),
    // postcss_assets = require('postcss-assets'),
    postcss_mixins = require('postcss-mixins'),
    postcss_nested = require('postcss-nested'),
    postcss_simple_vars = require('postcss-simple-vars'),
    postcss_easings = require('postcss-easings'),
    postcss_import = require('postcss-import'),
    postcss_cssnext = require('postcss-cssnext'),
    postcss_hexrgba = require('postcss-hexrgba'),
    postcss_extend = require('postcss-extend'),
    postcss_units = require('postcss-units'),
    postcss_object_fit_images = require('postcss-object-fit-images'),
    postcss_flexibility = require('postcss-flexibility'),
    postcss_math = require('postcss-math'),
    postcss_conditionals = require('postcss-conditionals'),
    postcss_reporter = require('postcss-reporter');

var task_env = 'dev';                                              // Идентификатор цели запуска задачи (dev - для наблюдателей, prod - для финального билда)


// --------------- CSS ---------------

var cssPath = './static/css/';                                            // Директория, где располагаются CSS-файлы
var cssMapPath = '/maps/';                                         // Директория с map-файлами для css
var cssOutFile = 'style.css';                                      // Название выходного файла css
var cssOutPath = path.join(cssPath, 'c');                          // папка со склеенным и уменьшенным css. Файл имеет имя cssOutFile
var cssMapOutPath = path.join('..', cssMapPath, 'c');              // Директория с map-файлами для собранного css

var cssSourcemapsOptions = { sourceMappingURLPrefix: '/css' };     // Опции для настройка sourceMaps

// --------------- PostCSS ---------------

var post_css_path = path.join(cssPath, 'postcss');
var post_css_main_file = path.join(post_css_path, cssOutFile);
var post_css_files_mask = path.join(post_css_path, '**', '*.pcss');

var clean_css_params = {
    keepBreaks: true,
    restructuring: false,
    aggressiveMerging: false
};


// --------------- JS ---------------

var jsPath = './static/js/';                                              // Директория JS
var jsMapPath = '/maps/';
var jsOutFile = 'script.js';                                       // Название выходного js файла
var jsOutPath = path.join(jsPath, 'c');                            // Папка для выходного js файла
var jsMapOutPath = path.join('..', jsMapPath);                     // Папка для map файлов

var jsSourcemapsOptions = { sourceMappingURLPrefix: '/js' };       // Опции для настройка sourceMaps

// Список js файлов для сборки, названия файлов указываются от jsPath
var jsFiles = [
    // 'lib/jquery3.js',
    // 'lib/jquery3/plugins/jquery-migrate-3.0.0.min.js',
    // 'lib/jquery/plugins/fancybox2/jquery.fancybox.pack.js',

    // 'plugins/serialize-object.js',
    // 'plugins/slick.js',

    'bundle.js',
    'main.js'
];
// --------------- ES6 ---------------

var es6_source_path = path.join(jsPath, 'es6');                    // Путь к исходникам
var es6_bundle_name = 'bundle.js';                                 // Название основого файла сборки
var es6_bundle_path = path.join(es6_source_path, es6_bundle_name); // Местоположение основного файла
var es6_destination = jsPath;                                      // Папка, куда нужно положить собранный файл

var onError = function (err) {
    // gutil.beep();
    console.log(err);
};


// Сборка PostCSS файлов
gulp.task('build-postcss', function(done) {

    var resultMapPath = cssMapPath;
    var resultCssPath = cssPath;

    if (task_env === 'prod') {
        clean_css_params = { aggressiveMerging: false };
        resultMapPath = cssMapOutPath;
        resultCssPath = cssOutPath;
    }

    var processors = [
        postcss_import(),
        postcss_mixins(),
        postcss_nested(),
        // postcss_assets(), как-нибудь в другой раз
        postcss_simple_vars(),
        postcss_hexrgba(),
        postcss_units(),
        postcss_cssnext({
            browsers: ['> 0.5%', 'last 10 versions']
        }),
        postcss_easings(),
        postcss_extend(),
        postcss_object_fit_images(),
        postcss_flexibility(),
        postcss_math(),
        postcss_conditionals()
    ];

    return gulp.src(post_css_main_file)
        // .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: onError
        }))
        //.pipe(postcss(postcss_processors))
        .pipe(postcss(processors))
        .pipe(plumber.stop())
        .pipe(cleanCSS(clean_css_params))
        .pipe(rename(cssOutFile))
        // .pipe(sourcemaps.write(resultMapPath, cssSourcemapsOptions))
        .pipe(gulp.dest(resultCssPath))
        // .pipe(browserSync.reload({stream: true}));
    done();
});


function buildScript(watch) {
    var opts = assign(
        {},
        watchify.args,
        {
            entries: [ es6_bundle_path ],
            debug: ((task_env === 'prod') ? false : true)
        });

    var bundler = watch ? watchify(browserify(opts)) : browserify(opts);

    bundler.transform(babelify, {
        // global: true,
        // ignore: /\/node_modules\/(?!app\/)/,
        // presets: ['es2015'],
        // presets: ['@babel/preset-env'],
        presets: ["@babel/preset-env"],
        // presets: ['env'],
        // plugins: [['transform-es2015-classes', { loose: true }]]
    });

    function rebundle() {
        var stream = bundler.bundle();
        return stream.on('error', gutil.log.bind(gutil, 'Browserify Error'))
            // .on('error', gutil.log.bind(gutil, 'Browserify Error'))
            .pipe(source(es6_bundle_name))
            // optional, remove if you don't need to buffer file contents
            .pipe(buffer())
            // optional, remove if you dont want sourcemaps
            .pipe(sourcemaps.init({ loadMaps: true })) // loads map from browserify file
            // Add transformation tasks to the pipeline here.
            .pipe(sourcemaps.write(jsMapPath, jsSourcemapsOptions)) // writes .map file
            .pipe(gulp.dest(es6_destination));
    }
    bundler.on('update', function() {
        rebundle();
        gutil.log('Rebundle...');
    });
    bundler.on('log', gutil.log);
    return rebundle();
}

gulp.task('watch-js', function(done) {
    return buildScript(true);
    done();
});

// gulp.task('build-es6', function() {
//     return buildScript(false);
// });


gulp.task('watch-css', function(done) {
    // gulp.watch(post_css_files_mask, ['build-postcss']);
    gulp.watch(post_css_files_mask, gulp.series('build-postcss'));
    done();
});

// Установка окружения для сборки проекта
// gulp.task('production', function() {
//     task_env = 'prod';
// });

// Сборка и минимизация CSS через PostCss
// gulp.task('build-css', ['production', 'build-postcss']);

// // Сборка и минимизация JS
// gulp.task('build-js', ['production', 'build-es6'], function() {
//     gulp.src(jsFiles, { cwd: jsPath })
//         .pipe(sourcemaps.init())
//         .pipe(concat(jsOutFile))
//         .pipe(uglify())
//         .pipe(sourcemaps.write(jsMapOutPath, jsSourcemapsOptions))
//         .pipe(gulp.dest(jsOutPath));
// });

// gulp.task('browser-sync', function(){
//     browserSync({
//         server: {
//             baseDir: '.',
//         },
//         notify: false,
//     });
// });

// Верстка
// gulp.task('watch', ['browser-sync', 'build-postcss'], function(){
//     gulp.watch(post_css_files_mask, ['build-postcss']);
//     gulp.watch('*.html', browserSync.reload);

// });
