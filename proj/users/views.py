from django.http import Http404
from django.views.generic import TemplateView
from .models import VendorUser
from django.core.exceptions import ObjectDoesNotExist


class ProfileView(TemplateView):
    @staticmethod
    def get_user(request, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except ObjectDoesNotExist:
            raise Http404
        return vendor

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'vendor': self.get_user(self.request),
        })
        return context
