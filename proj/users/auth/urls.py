from django.urls import path
from . import views

urlpatterns = [
    path('', views.auth_index, name='auth'),
    path('logout/', views.AuthLogout.as_view(), name="logout"),
    path('login/', views.AuthLogin.as_view(), name="login"),
    path('change-password/', views.ChangePassword.as_view(), name="change_password"),
    path('change-password/success/', views.ChangeSuccess.as_view(), name="change_success")
]