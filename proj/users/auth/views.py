from django.shortcuts import render, reverse, redirect, resolve_url
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth import login as auth_login
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView


def auth_index(request):
    return redirect('/auth/login/')


class AuthLogin(LoginView):
    template_name = 'auth/login.html'

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        url = self.get_redirect_url()
        user = self.request.user
        # if user.is_superuser or user.is_staff:
        #     url = '/staff'
        return url or resolve_url(settings.LOGIN_REDIRECT_URL)

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())


class AuthLogout(LogoutView):
    template_name = 'auth/logout.html'

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return redirect(reverse('index'))


class ChangePassword(PasswordChangeView):
    template_name = 'auth/change_password.html'
    success_url = 'change_success'


class ChangeSuccess(PasswordChangeDoneView):
    template_name = 'auth/change_success.html'
    title = 'Смена пароля - успешно'
