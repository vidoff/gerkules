from django.contrib import admin
from .models import VendorUser, ManagerUser, Document, Car, Driver, VendorSettings

admin.site.register(VendorUser)
admin.site.register(ManagerUser)
admin.site.register(Document)
admin.site.register(Car)
admin.site.register(Driver)
admin.site.register(VendorSettings)
