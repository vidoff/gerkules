from django.db import models
from django.contrib.auth.models import User
from app.models import TASK_UNLOADING, Tender
from geo.models import City as GeoCity
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.shortcuts import reverse
from django.utils import timezone


class PersonModel(models.Model):
    name = models.CharField("ФИО", max_length=120)
    email = models.EmailField("E-mail", max_length=63)
    phone = models.CharField("Телефон", blank=True, max_length=63)
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class ManagerUser(PersonModel):
    class Meta:
        verbose_name = "Менеджер"
        verbose_name_plural = "Менеджеры"


class VendorUser(PersonModel):
    company_name = models.CharField("Компания", max_length=100, null=False, blank=False)
    company_addr = models.CharField(
        "Юридический адрес", max_length=255, null=True, blank=True
    )
    company_post_addr = models.CharField(
        "Почтовый адрес", max_length=255, null=True, blank=True
    )
    company_inn = models.CharField("ИНН", max_length=20, null=True, blank=True)
    company_phone = models.CharField(
        "Телефон/факс", max_length=63, null=False, blank=False, default=""
    )
    company_seo = models.CharField(
        "ФИО директора", max_length=120, null=True, blank=True
    )
    company_site = models.CharField("Веб-сайт", max_length=63, null=True, blank=True)

    class Meta:
        verbose_name = "Поставщик"
        verbose_name_plural = "Поставщики"


class VendorSettings(models.Model):
    vendor = models.OneToOneField(
        VendorUser, on_delete=models.CASCADE, related_name="settings"
    )

    tender_created_notice = models.BooleanField("Включить уведомления", default=True)
    tender_created_email = models.BooleanField(
        "Включить уведомления на почту", default=True
    )
    city_notices = models.ManyToManyField(GeoCity)

    class Meta:
        verbose_name = "Конфигурация"
        verbose_name_plural = "Конфигурации поставщиков"

    def city_count(self):
        return self.city_notices.count()


class Driver(models.Model):
    vendor = models.ForeignKey(
        "VendorUser",
        related_name="drivers",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    publication = models.BooleanField("Публиковать", default=True)
    name = models.CharField("ФИО", max_length=63)
    passport = models.TextField("Паспорт", max_length=1000, null=True, blank=True)
    license = models.CharField(
        "Водительское удостоверение", max_length=30, null=False, blank=False, default=""
    )
    phone = models.CharField("Телефон", max_length=63, null=False, blank=False)
    experience = models.IntegerField("Стаж лет", null=True, blank=True, default=0)
    comment = models.TextField("Комментарий", null=True, blank=True)
    manager_comment = models.TextField("Комментарий менеджера", null=True, blank=True)


class Document(models.Model):
    vendor = models.ForeignKey(
        "VendorUser",
        related_name="documents",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    publication = models.BooleanField("Публиковать", default=True)
    num = models.CharField("Номер договора", max_length=63, null=False, blank=False)
    sign_date = models.DateField("Дата заключения", null=False)
    file_scan = models.FileField("Скан договора", blank=True)
    comment = models.TextField("Комментарий", null=True, blank=True)
    manager_comment = models.TextField("Комментарий менеджера", null=True, blank=True)


class Car(models.Model):
    vendor = models.ForeignKey(
        "VendorUser",
        related_name="cars",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    publication = models.BooleanField("Публиковать", default=True)
    brand = models.CharField("Марка/модель", max_length=63, null=False, blank=False)
    gos_num = models.CharField("Гос. номер", max_length=20, null=False, blank=False)
    trailer_num = models.CharField(
        "Номер прицепа", max_length=20, null=True, blank=True
    )
    carrying = models.CharField("Грузоподъемность", max_length=5, null=True, blank=True)
    axis_num = models.CharField(
        "Колчичество осей с прицепом", max_length=20, null=True, blank=True
    )
    unloading = models.CharField(
        "Тип выгрузки", choices=TASK_UNLOADING, max_length=20, default="other"
    )
    comment = models.TextField("Комментарий", null=True, blank=True)
    manager_comment = models.TextField("Комментарий менеджера", null=True, blank=True)

    def __str__(self):
        return self.brand


@receiver(post_save, sender=VendorUser)
def create_settings(sender, instance, created, **kwargs):
    if created:
        try:
            settings = VendorSettings.objects.get(vendor_id=instance.pk)
        except VendorSettings.DoesNotExist:
            print("settings__error, user: {}({})".format(instance.name, instance.pk))
            settings = VendorSettings.objects.create(vendor=instance)
        else:
            print("settings: {}, user: {}".format(settings.pk, settings.vendor.name))
