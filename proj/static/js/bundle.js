(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
"use strict";

var _responsive = require('./proj/responsive.js');

var _profile = require('./proj/profile.js');

var _form = require('./proj/form.js');

var _tender = require('./proj/tender.js');

var _notice = require('./proj/notice.js');

var _messages = require('./proj/messages.js');

var _cities = require('./proj/cities.js');

var Proj = function Proj() {};

Proj.prototype.Form = _form.Form;
Proj.prototype.Responsive = _responsive.Responsive;
Proj.prototype.Profile = _profile.Profile;
Proj.prototype.Tender = _tender.Tender;
Proj.prototype.Notice = _notice.Notice;
Proj.prototype.Messages = _messages.Messages;
Proj.prototype.Cities = _cities.Cities;

global.Proj = Proj;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./proj/cities.js":2,"./proj/form.js":3,"./proj/messages.js":4,"./proj/notice.js":5,"./proj/profile.js":6,"./proj/responsive.js":7,"./proj/tender.js":8}],2:[function(require,module,exports){
'use strict';
/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Cities = undefined;

var _form = require('./form.js');

var _ajax_new = require('../utils/ajax_new');

function Cities(options) {
    var opt = $.extend(true, {
        selectorGeoCaption: '.js-geo-caption',
        selectorGeoChildren: '.js-geo-children',
        selectorGeoCheckbox: '.js-geo-checkbox',
        selectorGeoCity: '.js-geo-city',
        selectorShowForm: '.js-city-create',
        selectorShowCities: '.js-cities-show',
        selectorMailSend: '.js-city-email-send',
        activeClassName: 'is-active',
        formGeo: '.js-form-geo'
    }, options);

    $(document).on("click", opt.selectorGeoCaption, function (e) {
        var $parent = $(this).parent();
        var $children = $parent.next().children(opt.selectorGeoChildren);

        if ($parent.hasClass(opt.activeClassName)) {
            $children.removeClass(opt.activeClassName);
            $parent.removeClass(opt.activeClassName);
        } else {
            $children.addClass(opt.activeClassName);
            $parent.addClass(opt.activeClassName);
        }
    });

    $(document).on("click", opt.selectorShowCities, function (e) {
        e.preventDefault();
        $(this).next().toggleClass(opt.activeClassName);
    });

    $(document).on("change", opt.selectorMailSend, function (e) {
        var opt = {};
        var action = $(this).data('action');

        if ($(this).prop('checked')) {
            opt = JSON.stringify({ 'tender_created_email': true });
        } else {
            opt = JSON.stringify({ 'tender_created_email': false });
        }
        (0, _ajax_new.getAjax)(action, opt, { type: "PATCH", contentType: 'application/json' }).then(function (data) {
            console.log(data);
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    });
    $(document).on("click", opt.selectorGeoCheckbox, function (e) {
        var $parent = $(this).parent();
        var $nextWrapper = $parent.next();
        var ret_str = "";
        if ($(this).prop("checked")) {
            $parent.addClass(opt.activeClassName);
            $nextWrapper.find(opt.selectorGeoChildren).addClass(opt.activeClassName);
            $nextWrapper.find("input:checkbox").prop("checked", true);
        } else {
            $parent.removeClass(opt.activeClassName);
            // $nextWrapper.parent().prev().removeClass(opt.activeClassName);
            $nextWrapper.find(opt.selectorGeoChildren).removeClass(opt.activeClassName);
            $nextWrapper.find("input:checkbox").prop("checked", false);
        }
    });

    //Добавить документы
    $(opt.selectorShowForm).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        (0, _form.ShowForm)($form.html());
    });

    $(document.body).on('submit', opt.formGeo, function (e) {
        e.preventDefault();
        var res_str = "";
        var $cities = $(this).find(opt.selectorGeoCity + ':checked');
        if ($cities.length > 0) {
            $cities.each(function (e) {
                res_str += $(this).val() + ",";
            });
        }
        res_str = res_str.substring(0, res_str.length - 1);
        console.log(res_str);
        var params = $.extend(true, opt, { 'res_str': res_str });
        SubmitForm(this, params);
    });

    function SubmitForm(el, options) {
        var $form = $(el);
        var action = $form.attr('action').trim();
        var opt = $.extend({}, options);
        var ajaxParams = $.extend({}, { type: opt.type });

        var $fields = $form.find(opt.fieldSelector);
        var formData = new FormData();
        formData.append('cities', opt.res_str);

        // $(opt.fieldErrorText).text('');

        (0, _ajax_new.getAjax)(action, formData, ajaxParams).then(function (data) {
            if (opt.resultFunction) {
                opt.resultFunction($form, data);
            } else {
                $.fancybox.close();
                location.reload();
            }
        }, function (error) {
            // console.log(error.responseText);
            var errors = JSON.parse(error.responseText);

            var _loop = function _loop(p) {
                $fields.filter('.m--name-' + p).each(function (i) {
                    $(this).addClass(opt.errorClass);
                    $(this).parents(opt.formGroup).find(opt.fieldErrorText).text(errors[p][0]);
                });
            };

            for (var p in errors) {
                _loop(p);
            }
        }).catch(function (error) {
            throw new Error(error);
        });
    }
}

exports.Cities = Cities;

},{"../utils/ajax_new":9,"./form.js":3}],3:[function(require,module,exports){
'use strict';

/* global $ */

// import {getCookie} from '../utils/cookie.js';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.formSettings = exports.SubmitForm = exports.GetForm = exports.ShowForm = exports.Form = undefined;

var _ajax_new = require('../utils/ajax_new');

var _params = require('../utils/params');

var _cookie = require('../utils/cookie.js');

var formSettings = {
    formSelector: '.js-form',
    formPatch: '.js-form-patch',
    // formUpdate: '.js-form-update',
    formHidden: '.js-form-hidden',
    formGroup: '.form-group',
    fieldErrorText: '.js-field-error',
    linkSelector: '.js-form-link',
    fieldSelector: '.js-form-field',
    errorClass: 'is-invalid',
    selectorSubmitForm: '.js-form-submit',
    selectorCheckbox: '.js-form-field__checkbox'
};

function Form(options) {
    var opt = $.extend(true, {}, formSettings, options);
    $(document.body).on('change', opt.selectorCheckbox, function (e) {
        e.preventDefault();
        if ($(this).prop("checked")) {
            $(this).val("true");
        } else {
            $(this).val("false");
        }
    });

    $(document.body).on('submit', opt.formHidden, function (e) {
        e.preventDefault();
        SubmitForm(this, opt);
    });

    $(document.body).on('submit', opt.formPatch, function (e) {
        e.preventDefault();
        var params = $.extend(true, opt, { type: "PATCH" });
        SubmitForm(this, params);
    });

    $(document.body).on('submit', opt.formSelector, function (e) {
        e.preventDefault();
        var params = $.extend(true, opt, { type: "POST" });
        SubmitForm(this, params);
    });
}

function ShowForm(content, options) {
    var workers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    if (workers) {
        for (var worker in workers) {
            content = workers[worker](content);
        }
    }
    var params = $.extend(true, options, {});
    $.fancybox.open(content, params);
}

function GetForm(action, options) {
    var workers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    var opt = $.extend({}, { format: 'form' }, options);

    (0, _ajax_new.getAjax)(action, opt, { type: "GET" }).then(function (data) {
        ShowForm(data.content, {}, workers);
    }, function (error) {
        console.log(error.responseText);
    }).catch(function (error) {
        throw new Error(error);
    });
}

function SubmitForm(el, options) {
    var $form = $(el);
    var action = $form.attr('action').trim();
    var opt = $.extend({}, options);
    var ajaxParams = $.extend({}, { type: opt.type });

    var $fields = $form.find(opt.fieldSelector);
    var formData = new FormData();

    $fields.each(function (index) {
        $(this).removeClass(opt.errorClass);
        if ($(this).attr("type") != "file") {
            formData.append($(this).attr('name'), $(this).val());
        } else {
            var file = $(this).prop("files")[0];
            formData.append($(this).attr('name'), file);
        }
    });
    $(opt.fieldErrorText).text('');

    (0, _ajax_new.getAjax)(action, formData, ajaxParams).then(function (data) {
        if (opt.resultFunction) {
            opt.resultFunction($form, data);
        } else {
            $.fancybox.close();
            location.reload();
        }
    }, function (error) {
        // console.log(error.responseText);
        var errors = JSON.parse(error.responseText);

        var _loop = function _loop(p) {
            $fields.filter('.m--name-' + p).each(function (i) {
                $(this).addClass(opt.errorClass);
                $(this).parents(opt.formGroup).find(opt.fieldErrorText).text(errors[p][0]);
            });
        };

        for (var p in errors) {
            _loop(p);
        }
    }).catch(function (error) {
        throw new Error(error);
    });
}

exports.Form = Form;
exports.ShowForm = ShowForm;
exports.GetForm = GetForm;
exports.SubmitForm = SubmitForm;
exports.formSettings = formSettings;

},{"../utils/ajax_new":9,"../utils/cookie.js":10,"../utils/params":11}],4:[function(require,module,exports){
'use strict';
/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Messages = undefined;

var _form = require('./form.js');

var _ajax_new = require('../utils/ajax_new');

function Messages(options) {
    var opt = $.extend(true, {
        selectorRoomCreate: '.js-room-create',
        selectorMessageBody: '.js-message-body',
        selectorFormMessage: '.js-form-message',
        fieldSelector: '.js-form-field'
    }, options);

    var $messageBody = $(opt.selectorMessageBody);

    if ($messageBody[0]) {
        $messageBody.scrollTop($messageBody[0].scrollHeight);
    }

    $(document).on('click', opt.selectorRoomCreate, function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        (0, _form.GetForm)(href, {});
    });

    $(document.body).on('submit', opt.selectorFormMessage, function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var formData = new FormData();
        var $fields = $(this).find(opt.fieldSelector);
        var ajaxParams = {};

        $fields.each(function (index) {
            formData.append($(this).attr('name'), $(this).val());
        });

        (0, _ajax_new.getAjax)(action, formData, ajaxParams).then(function (data) {
            // console.log(data);
            location.reload();
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    });
}

exports.Messages = Messages;

},{"../utils/ajax_new":9,"./form.js":3}],5:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Notice = undefined;

var _ajax_new = require('../utils/ajax_new');

var _form = require('./form.js');

function Notice(options) {

    var opt = $.extend(true, {
        selectorNoticeDisable: '.js-notice-disable',
        selectorNoticeCount: '.js-notice-count',
        selectorNoticesClear: '.js-notices-clear'
    }, options);
    $(document).on('click', opt.selectorNoticeDisable, function (e) {
        var action = $(this).data('action');
        var is_active = false;
        var params = JSON.stringify({ 'is_active': is_active });
        var ajax_params = {
            type: "PATCH",
            contentType: 'application/json'
        };
        (0, _ajax_new.getAjax)(action, params, ajax_params).then(function (data) {
            NoticeCheck();
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    });
    $(document).on('click', opt.selectorNoticesClear, function (e) {
        console.log('test');
        var action = $(this).data('action');
        var is_active = false;
        var params = JSON.stringify({ 'is_active': is_active, 'select': 'all' });
        var ajax_params = {
            type: "DELETE",
            contentType: 'application/json'
        };
        (0, _ajax_new.getAjax)(action, params, ajax_params).then(function (data) {
            // console.log(data)
            // NoticeCheck();
            location.reload();
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    });

    function NoticeCheck() {
        var $count = $(opt.selectorNoticeCount).children();
        var num = Number($count.text());
        if (num > 1) {
            num--;
        } else {
            $count.parent().remove();
        }
        $count.text(num);
    }
}

exports.Notice = Notice;

},{"../utils/ajax_new":9,"./form.js":3}],6:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Profile = undefined;

var _ajax_new = require('../utils/ajax_new');

var _cookie = require('../utils/cookie.js');

var _form = require('./form.js');

function Profile(options) {

    var opt = $.extend(true, {
        selectorVendorCreate: '.js-vendor-create',
        selectorItemEdit: '.js-edit-item',
        selectorItemDelete: '.js-delete-item',
        vendorCarCreate: '.js-car-create',
        vendorDocumentCreate: '.js-document-create',
        vendorDriverCreate: '.js-driver-create',
        vendorCityCreate: '.js-city-create',
        vendorEditPerson: '.js-vendor-edit-person',
        vendorEditCompany: '.js-vendor-edit-company',
        formHidden: '.js-form-hidden',
        formBan: '.js-form-ban',
        selectorUserEdit: '.js-user-edit',
        selectorUserBan: '.js-user-ban',
        selectorUserPassword: '.js-user-changepassword',
        selectorProfileItemsWrapper: '.js-profile-items-wrapper',
        selectorProfileItem: '.js-profile-item'
    }, options);

    //добавить поставщика
    $(opt.selectorVendorCreate).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        (0, _form.ShowForm)($form.html());
    });

    //показать форму редактирования персональных данных
    $(opt.vendorEditPerson).click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        (0, _form.GetForm)(href, { type: 'person' });
    });

    //показать форму редактирования данных компании
    $(opt.vendorEditCompany).click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        (0, _form.GetForm)(href, { type: 'company' });
    });

    //показать форму редактирование пользователя
    $(opt.selectorUserEdit).click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        (0, _form.GetForm)(href, { type: 'person' });
    });
    //сменить пароль у пользователя
    $(opt.selectorUserPassword).click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        (0, _form.GetForm)(href, { type: 'password' });
    });

    //изменить is_active у поставщика
    $(opt.selectorUserBan).click(function (e) {
        var _this = this;

        e.preventDefault();
        var action = $(this).attr('href');
        var is_active = $(this).hasClass('m--lock');
        var opt = JSON.stringify({ 'is_active': is_active });
        (0, _ajax_new.getAjax)(action, opt, { type: "PATCH", contentType: 'application/json' }).then(function (data) {
            if (is_active) {
                $(_this).removeClass('m--lock').parents('tr').removeClass('m--lock');
            } else {
                $(_this).addClass('m--lock').parents('tr').addClass('m--lock');
            }
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    });

    //Добавить документы
    $(opt.vendorDocumentCreate).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        (0, _form.ShowForm)($form.html());
    });

    //Добавить машины
    $(opt.vendorCarCreate).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        (0, _form.ShowForm)($form.html());
    });

    //Добавить город
    $(opt.vendorCityCreate).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        console.log('teset');
        // ShowForm($form.html());
    });

    //Добавить водителя
    $(opt.vendorDriverCreate).click(function (e) {
        e.preventDefault();
        var $form = $('.js-form-wrapper');
        (0, _form.ShowForm)($form.html());
    });

    //Редактирование данных
    $(opt.selectorItemEdit).click(function (e) {
        e.preventDefault();
        (0, _form.GetForm)($(this).attr('href'));
    });

    //Удаление данных
    $(document.body).on('click', opt.selectorItemDelete, function (e) {
        e.preventDefault();
        var ajax_options = { type: 'DELETE' };
        var params = {};
        var href = $(this).parents("form").attr("action").trim();

        if (confirm("Подтвердите удаление")) {
            (0, _ajax_new.getAjax)(href, params, ajax_options).then(function (data) {
                console.log($(opt.selectorProfileItem).length);
                if ($(opt.selectorProfileItem).length === 1) {
                    $(opt.selectorProfileItemsWrapper).remove();
                } else {
                    $("#item-" + data.pk).remove();
                }
                $.fancybox.close();
            }, function (error) {
                console.log(error.responseText);
            }).catch(function (error) {
                throw new Error(error);
            });
        } else {
            console.log('false');
        }
    });
}exports.Profile = Profile;

},{"../utils/ajax_new":9,"../utils/cookie.js":10,"./form.js":3}],7:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Responsive = undefined;

var _params = require('../utils/params');

function Responsive(options) {
    var opt = $.extend(true, {
        breakpoints: _params.breakpoints
    }, options);

    var $window = $(window),
        currentPoint = undefined;

    function watchSize() {
        var width = $window.width(),
            point = 'xs';

        if (width > opt.breakpoints.l) {
            point = 'xl';
        } else if (width > opt.breakpoints.m) {
            point = 'l';
        } else if (width > opt.breakpoints.s) {
            point = 'm';
        } else if (width > opt.breakpoints.xs) {
            point = 's';
        }

        if (!currentPoint || point != currentPoint) {
            if (currentPoint) {
                $window.trigger('Responsive.off.' + currentPoint);
            }

            $window.trigger('Responsive.on.' + point);

            currentPoint = point;
        }
    }

    $window.on('resize', watchSize);
    watchSize();
}exports.Responsive = Responsive;

},{"../utils/params":11}],8:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Tender = undefined;

var _ajax_new = require('../utils/ajax_new');

var _form = require('./form.js');

function Tender(options) {

    var opt = $.extend(true, {
        selectorItemEdit: '.js-edit-item',
        selectorBetCancel: '.js-bet-cancel',
        selectorVendorSelectCars: '.js-select-vendor-cars',
        selectorVendorCarField: '.js-vendor-car-field',
        selectorVendorSelectDrivers: '.js-select-vendor-drivers',
        selectorVendorDriverField: '.js-vendor-driver-field',
        selectorTenderCreateCar: '.js-tender-create-car',
        selectorTenderCreateDriver: '.js-tender-create-driver',
        selectorCheckData: '.js-tender-check-data',
        selectorDataBlockInfo: '.js-documents-info',
        selectorStaffTenderConfirm: '.js-staff-tender-confirm',
        formResult: '.js-form-result',
        formVendorData: '.js-form-vendor-data'
    }, options, _form.formSettings);

    $(document.body).on('submit', opt.formResult, function (e) {
        e.preventDefault();
        var params = $.extend(true, opt, {
            type: "POST",
            resultFunction: afterCreate
        });
        (0, _form.SubmitForm)(this, params);
    });
    $(document.body).on("submit", opt.formVendorData, function (e) {
        e.preventDefault();
        SubmitVendorData(this, opt);
    });

    $(opt.selectorStaffTenderConfirm).click(function (e) {
        e.preventDefault();
        var ajax_options = { type: "PATCH", contentType: "application/json" };
        var href = $(this).attr("href"),
            params = {},
            message = "";

        if ($(this).data("confirmed") === true) {
            params = { "confirmed": true };
            message = "Подтвердить данные?";
        } else if ($(this).data("confirmed") === false) {
            params = { "confirmed": false };
            message = "Отменить подтверждение?";
        }

        if (confirm(message)) {
            (0, _ajax_new.getAjax)(href, JSON.stringify(params), ajax_options).then(function (data) {
                console.log('ok');
                location.reload();
            }, function (error) {
                console.log(error.responseText);
            }).catch(function (error) {
                throw new Error(error);
            });
        } else {
            console.log('false');
        }
    });
    function afterCreate($form, data) {
        if ($form.data("formType") === "car") {
            var $select = $(opt.selectorVendorSelectCars);
            $select.children("option").removeAttr("selected");
            $select.append($("<option></option>").attr("value", data.car_id).text(data.content.brand).attr('selected', 'selected'));
            $.each(data.content, function (name, value) {
                $('#vendor-car-' + name).text(value);
            });
            SubmitVendorData($(opt.formVendorData), opt);
        } else if ($form.data("formType") === "driver") {
            var _$select = $(opt.selectorVendorSelectDrivers);
            _$select.children("option").removeAttr("selected");
            _$select.append($("<option></option>").attr("value", data.driver_id).text(data.content.name).attr('selected', 'selected'));
            $.each(data.content, function (name, value) {
                $('#vendor-driver-' + name).text(value);
            });
            SubmitVendorData($(opt.formVendorData), opt);
        }

        $.fancybox.close();
    }

    $(opt.selectorBetCancel).click(function (e) {
        e.preventDefault();
        var ajax_options = { type: 'DELETE' };
        var params = {};
        var href = $(this).parents("form").attr("action").trim();

        if (confirm("Подтвердите отмену ставки")) {
            (0, _ajax_new.getAjax)(href, params, ajax_options).then(function (data) {
                console.log('delete: ok');
                location.reload();
            }, function (error) {
                console.log(error.responseText);
            }).catch(function (error) {
                throw new Error(error);
            });
        } else {
            console.log('false');
        }
    });
    $(opt.selectorVendorSelectCars).on("change", function (e) {
        e.preventDefault();
        var href = $(this).data("href"),
            value = $(this).val();

        var params = {};
        var ajax_options = { type: 'GET' };

        (0, _ajax_new.getAjax)(href + value, params, ajax_options).then(function (data) {
            $.each(data, function (name, value) {
                $('#vendor-car-' + name).text(value);
            });
        }, function (error) {
            $(opt.selectorVendorCarField).each(function (i) {
                $(this).text("");
            });
        }).catch(function (error) {
            throw new Error(error);
        });
    });
    $(opt.selectorVendorSelectDrivers).on("change", function (e) {
        e.preventDefault();
        var href = $(this).data("href"),
            value = $(this).val();

        var params = {};
        var ajax_options = { type: 'GET' };

        (0, _ajax_new.getAjax)(href + value, params, ajax_options).then(function (data) {
            $.each(data, function (name, value) {
                $('#vendor-driver-' + name).text(value);
            });
        }, function (error) {
            $(opt.selectorVendorDriverField).each(function (i) {
                $(this).text("");
            });
        }).catch(function (error) {
            throw new Error(error);
        });
    });

    $(opt.selectorTenderCreateDriver).on("click", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        (0, _form.GetForm)(href, {}, { preloader: preloaderFormDriver });
    });

    $(opt.selectorTenderCreateCar).on("click", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        (0, _form.GetForm)(href, {}, { preloader: preloaderFormCar });
    });

    function preloaderFormCar(data) {
        var $form = $(data);
        $form.data("formType", "car");
        return $form;
    }

    function preloaderFormDriver(data) {
        var $form = $(data);
        $form.data("formType", "driver");
        return $form;
    }

    function SubmitVendorData(form, options) {
        var $form = $(form);
        var action = $form.attr('action').trim();
        var opt = $.extend({}, options);

        var $fields = $form.find(opt.fieldSelector);
        var formData = new FormData();

        $fields.each(function (index) {
            if ($(this).attr("type") != "file") {
                formData.append($(this).attr('name'), $(this).val());
            } else {
                var file = $(this).prop("files")[0];
                formData.append($(this).attr('name'), file);
            }
        });
        (0, _ajax_new.getAjax)(action, formData, { type: "PATCH" }).then(function (data) {
            var $check = $(opt.selectorCheckData);
            if (!data.vendor_driver_id) {
                $check.filter(".m--driver").children(".fas").addClass("d-none");
            } else {
                $check.filter(".m--driver").children(".fas").removeClass("d-none");
            }
            if (!data.vendor_car_id) {
                $check.filter(".m--car").children(".fas").addClass("d-none");
            } else {
                $check.filter(".m--car").children(".fas").removeClass("d-none");
            }
            if (!data.vendor_car_id || !data.vendor_driver_id) {
                toggleDocumentsVisible(false);
            } else {
                toggleDocumentsVisible(true);
            }
        }, function (error) {
            console.log(error.responseText);
        }).catch(function (error) {
            throw new Error(error);
        });
    }

    function toggleDocumentsVisible(show) {
        var $blockInfo = $(opt.selectorDataBlockInfo);
        if (show) {
            $blockInfo.filter('.m--achieved').removeClass('d-none');
            $blockInfo.filter('.m--not-achieved').addClass('d-none');
        } else {
            $blockInfo.filter('.m--achieved').addClass('d-none');
            $blockInfo.filter('.m--not-achieved').removeClass('d-none');
        }
    }
}

exports.Tender = Tender;

},{"../utils/ajax_new":9,"./form.js":3}],9:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getAjax = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _params = require('./params');

function getAjax(act, data, options) {
    var opt = $.extend({}, _params.ajax, options);

    if (opt.SESSION_ID && opt.type.toLowerCase !== 'get') {
        if (data instanceof Array) {
            data.push({ name: 'SESS_ID', value: opt.SESSION_ID });
        } else if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)) === 'object') {
            data.SESS_ID = opt.SESSION_ID;
        } else {
            if (data != '') data += '&';
            data += 'SESS_ID=' + opt.SESSION_ID;
        }
    }

    if (!/^\//.test(act)) {
        opt.url += act;
    } else {
        opt.url = act;
    }
    if (!/\/$/.test(opt.url)) {
        opt.url += '/';
    }

    // opt.data = data;
    if (opt.type.toLowerCase() === 'get') {
        opt.data = $.param(data);
    } else {
        opt.data = data;
    }
    // console.log("==========data==============");
    // console.log(data);
    // console.log("==========end-data==========");

    var handler = new Promise(function (resolve, reject) {
        opt.success = function (data, textStatus, jqXHR) {
            resolve(data, jqXHR);
        };
        opt.error = function (jqXHR) {
            reject(jqXHR, jqXHR);
        };
        $.ajax(opt);
    });

    return handler;
}

exports.getAjax = getAjax;

},{"./params":11}],10:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === name + '=') {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

exports.getCookie = getCookie;

},{}],11:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.fancyboxForm = exports.fancyboxBlue = exports.fancybox = exports.fancyboxDefault = exports.ajax = exports.breakpoints = exports.isDesktop = exports.isMobile = exports.isTablet = exports.interfaceName = undefined;

var _cookie = require('../utils/cookie.js');

function detectSessionID() {
    var sessionId = '';
    if (window.location.toString().match(/SESS_ID=(\d+)/)) {
        sessionId = RegExp.$1;
    }
    return sessionId;
}
var csrftoken = (0, _cookie.getCookie)('csrftoken');

var ajax = {
    type: 'put',
    url: '/json/',
    dataType: 'json',
    // contentType: 'application/json',
    contentType: false,
    timeout: 10000,
    processData: false,
    SESSION_ID: detectSessionID(),
    beforeSend: function beforeSend(xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
};

var fancyboxDefault = {
    padding: 0,
    helpers: {
        title: { type: 'inside' }
    }
};

var fancybox = {
    padding: 0,
    closeBtn: false,
    maxWidth: 960,
    helpers: {
        title: { type: 'inside' },
        overlay: {
            css: {
                'background': 'rgba(0, 0, 0, 0.3)'
            }
        }
    }
};

var fancyboxBlue = {
    padding: 0,
    wrapCSS: 'flexites-fancy',
    closeBtn: false,
    helpers: {
        title: { type: 'inside' },
        overlay: {
            css: {
                'background': 'rgba(29, 62, 129, 0.95)'
            }
        }
    }
};
var fancyboxForm = {
    padding: 0,
    wrapCSS: 'flexites-fancy',
    // closeBtn: false,
    helpers: {
        title: { type: 'inside' },
        overlay: {
            css: {
                // 'background' : 'rgba(255, 255, 255, 0)'
                'background': 'rgba(93, 184, 253, 0.68)'
                // 'opacity' : '1'
            }
        }
    }
};

var $body = $(document.body);

var interfaceName = $body.data('interface_name');

var isTablet = !!parseInt($body.data('is_tablet'));
var isMobile = !!parseInt($body.data('is_mobile'));
var isDesktop = !isMobile && !isTablet;

var breakpoints = {
    xl: Infinity,
    l: 1367,
    m: 1000,
    s: 720,
    xs: 320
};

exports.interfaceName = interfaceName;
exports.isTablet = isTablet;
exports.isMobile = isMobile;
exports.isDesktop = isDesktop;
exports.breakpoints = breakpoints;
exports.ajax = ajax;
exports.fancyboxDefault = fancyboxDefault;
exports.fancybox = fancybox;
exports.fancyboxBlue = fancyboxBlue;
exports.fancyboxForm = fancyboxForm;

},{"../utils/cookie.js":10}]},{},[1])

//# sourceMappingURL=/js/maps/bundle.js.map
