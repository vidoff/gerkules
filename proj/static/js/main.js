"use strict";

$(function(){
    var proj = new Proj();

    proj.Responsive();
    proj.Profile();
    proj.Form();
    proj.Tender();
    proj.Notice();
    proj.Messages();
    proj.Cities();

});
