'use strict';

/* global $ */
import {getCookie} from '../utils/cookie.js';

function detectSessionID() {
    let sessionId = '';
    if (window.location.toString().match(/SESS_ID=(\d+)/)) {
        sessionId = RegExp.$1;
    }
    return sessionId;
}
let csrftoken = getCookie('csrftoken');

let ajax = {
    type: 'put',
    url: '/json/',
    dataType: 'json',
    // contentType: 'application/json',
    contentType: false,
    timeout: 10000,
    processData: false,
    SESSION_ID: detectSessionID(),
    beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
    },
};

let fancyboxDefault = {
    padding: 0,
    helpers: {
        title: {type: 'inside'}
    }
};

let fancybox = {
    padding: 0,
    closeBtn: false,
    maxWidth: 960,
    helpers: {
        title: {type: 'inside'},
        overlay: {
            css: {
                'background': 'rgba(0, 0, 0, 0.3)'
            }
        }
    }
};

let fancyboxBlue = {
    padding: 0,
    wrapCSS: 'flexites-fancy',
    closeBtn: false,
    helpers: {
        title: {type: 'inside'},
        overlay: {
            css: {
                'background': 'rgba(29, 62, 129, 0.95)'
            }
        }
    }
};
let fancyboxForm = {
    padding: 0,
    wrapCSS: 'flexites-fancy',
    // closeBtn: false,
    helpers: {
        title: {type: 'inside'},
        overlay: {
            css: {
                // 'background' : 'rgba(255, 255, 255, 0)'
                'background': 'rgba(93, 184, 253, 0.68)'
                // 'opacity' : '1'
            }
        }
    }
};


let $body = $(document.body);

let interfaceName = $body.data('interface_name');

let isTablet = !!parseInt($body.data('is_tablet'));
let isMobile = !!parseInt($body.data('is_mobile'));
let isDesktop = !isMobile && !isTablet;

let breakpoints = {
    xl: Infinity,
    l: 1367,
    m: 1000,
    s: 720,
    xs: 320
};

export {
    interfaceName,
    isTablet,
    isMobile,
    isDesktop,
    breakpoints,
    ajax,
    fancyboxDefault,
    fancybox,
    fancyboxBlue,
    fancyboxForm,
};
