'use strict';

/* global $ */

import { ajax } from './params';

function getAjax(act, data, options) {
    let opt = $.extend({}, ajax, options);

    if (opt.SESSION_ID && opt.type.toLowerCase !== 'get') {
        if (data instanceof Array) {
            data.push({ name: 'SESS_ID', value: opt.SESSION_ID });
        } else if (typeof data === 'object') {
            data.SESS_ID = opt.SESSION_ID;
        } else {
            if (data != '') data += '&';
            data += 'SESS_ID='+opt.SESSION_ID;
        }
    }

    if (!/^\//.test(act)) {
        opt.url += act;
    } else {
        opt.url = act;
    }
    if (!/\/$/.test(opt.url)) {
        opt.url += '/';
    }

    // opt.data = data;
    if (opt.type.toLowerCase() === 'get'){
        opt.data = $.param(data);
    } else {
        opt.data = data;
    }
    // console.log("==========data==============");
    // console.log(data);
    // console.log("==========end-data==========");

    let handler = new Promise(function(resolve, reject) {
        opt.success = function(data, textStatus, jqXHR) {
            resolve(data, jqXHR);
        };
        opt.error = function(jqXHR) {
            reject(jqXHR, jqXHR);
        };
        $.ajax(opt);
    });

    return handler;
}

export { getAjax };
