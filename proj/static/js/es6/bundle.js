"use strict";

import {Responsive} from './proj/responsive.js';
import {Profile} from './proj/profile.js';
import {Form} from './proj/form.js';
import {Tender} from './proj/tender.js';
import {Notice} from './proj/notice.js';
import {Messages} from './proj/messages.js';
import {Cities} from './proj/cities.js';


var Proj = function () {

};

Proj.prototype.Form = Form;
Proj.prototype.Responsive = Responsive;
Proj.prototype.Profile = Profile;
Proj.prototype.Tender = Tender;
Proj.prototype.Notice = Notice;
Proj.prototype.Messages = Messages;
Proj.prototype.Cities = Cities;

global.Proj = Proj;
