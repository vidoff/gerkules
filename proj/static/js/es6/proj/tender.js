'use strict';

/* global $ */

import {getAjax} from '../utils/ajax_new';
import {GetForm, ShowForm, SubmitForm, formSettings} from './form.js';


function Tender(options) {
    
    let opt = $.extend(true, {
        selectorItemEdit: '.js-edit-item',
        selectorBetCancel: '.js-bet-cancel',
        selectorVendorSelectCars: '.js-select-vendor-cars',
        selectorVendorCarField: '.js-vendor-car-field',
        selectorVendorSelectDrivers: '.js-select-vendor-drivers',
        selectorVendorDriverField: '.js-vendor-driver-field',
        selectorTenderCreateCar: '.js-tender-create-car',
        selectorTenderCreateDriver: '.js-tender-create-driver',
        selectorCheckData: '.js-tender-check-data',
        selectorDataBlockInfo: '.js-documents-info',
        selectorStaffTenderConfirm: '.js-staff-tender-confirm',
        formResult: '.js-form-result',
        formVendorData: '.js-form-vendor-data',
    }, options, formSettings);
    
    $(document.body).on('submit', opt.formResult, function (e) {
        e.preventDefault();
        let params = $.extend(true, opt, {
            type: "POST",
            resultFunction: afterCreate
        });
        SubmitForm(this, params);
    });
    $(document.body).on("submit", opt.formVendorData, function (e) {
        e.preventDefault();
        SubmitVendorData(this, opt)
    });
    
    $(opt.selectorStaffTenderConfirm).click(function (e) {
        e.preventDefault();
        let ajax_options = {type: "PATCH", contentType: "application/json"};
        let href = $(this).attr("href"),
            params = {},
            message = "";
        
        if ($(this).data("confirmed") === true) {
            params = {"confirmed": true}
            message = "Подтвердить данные?";
        } else if ($(this).data("confirmed") === false) {
            params = {"confirmed": false}
            message = "Отменить подтверждение?";
        }
        
        if (confirm(message)) {
            getAjax(href, JSON.stringify(params), ajax_options).then(
                data => {
                    console.log('ok');
                    location.reload();
                },
                error => {
                    console.log(error.responseText);
                }
            ).catch(function (error) {
                throw new Error(error);
            });
            
        } else {
            console.log('false');
        }
    });
    function afterCreate($form, data) {
        if ($form.data("formType") === "car") {
            let $select = $(opt.selectorVendorSelectCars);
            $select.children("option").removeAttr("selected");
            $select.append(
                $("<option></option>")
                    .attr("value", data.car_id)
                    .text(data.content.brand)
                    .attr('selected', 'selected')
            );
            $.each(data.content, function (name, value) {
                $('#vendor-car-' + name).text(value);
            });
            SubmitVendorData($(opt.formVendorData), opt);
        } else if ($form.data("formType") === "driver") {
            let $select = $(opt.selectorVendorSelectDrivers);
            $select.children("option").removeAttr("selected");
            $select.append(
                $("<option></option>")
                    .attr("value", data.driver_id)
                    .text(data.content.name)
                    .attr('selected', 'selected')
            );
            $.each(data.content, function (name, value) {
                $('#vendor-driver-' + name).text(value);
            });
            SubmitVendorData($(opt.formVendorData), opt);
        }
        
        $.fancybox.close();
    }
    
    $(opt.selectorBetCancel).click(function (e) {
        e.preventDefault();
        let ajax_options = {type: 'DELETE'};
        let params = {};
        let href = $(this).parents("form").attr("action").trim();
        
        if (confirm("Подтвердите отмену ставки")) {
            getAjax(href, params, ajax_options).then(
                data => {
                    console.log('delete: ok');
                    location.reload();
                },
                error => {
                    console.log(error.responseText);
                }
            ).catch(function (error) {
                throw new Error(error);
            });
            
        } else {
            console.log('false');
        }
    });
    $(opt.selectorVendorSelectCars).on("change", function (e) {
        e.preventDefault();
        let href = $(this).data("href"),
            value = $(this).val();
        
        let params = {};
        let ajax_options = {type: 'GET'};
        
        getAjax(href + value, params, ajax_options).then(
            data => {
                $.each(data, function (name, value) {
                    $('#vendor-car-' + name).text(value);
                });
            },
            error => {
                $(opt.selectorVendorCarField).each(function (i) {
                    $(this).text("");
                });
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
    $(opt.selectorVendorSelectDrivers).on("change", function (e) {
        e.preventDefault();
        let href = $(this).data("href"),
            value = $(this).val();
        
        let params = {};
        let ajax_options = {type: 'GET'};
        
        getAjax(href + value, params, ajax_options).then(
            data => {
                $.each(data, function (name, value) {
                    $('#vendor-driver-' + name).text(value);
                });
            },
            error => {
                $(opt.selectorVendorDriverField).each(function (i) {
                    $(this).text("");
                });
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
    
    $(opt.selectorTenderCreateDriver).on("click", function (e) {
        e.preventDefault();
        let href = $(this).attr("href");
        GetForm(href, {}, {preloader: preloaderFormDriver});
    });
    
    $(opt.selectorTenderCreateCar).on("click", function (e) {
        e.preventDefault();
        let href = $(this).attr("href");
        GetForm(href, {}, {preloader: preloaderFormCar});
    });
    
    function preloaderFormCar(data) {
        let $form = $(data);
        $form.data("formType", "car");
        return $form;
    }
    
    function preloaderFormDriver(data) {
        let $form = $(data);
        $form.data("formType", "driver");
        return $form;
    }
    
    function SubmitVendorData(form, options) {
        let $form = $(form);
        let action = $form.attr('action').trim();
        let opt = $.extend({}, options);
        
        let $fields = $form.find(opt.fieldSelector);
        let formData = new FormData()
        
        $fields.each(function (index) {
            if ($(this).attr("type") != "file") {
                formData.append($(this).attr('name'), $(this).val());
            } else {
                let file = $(this).prop("files")[0];
                formData.append($(this).attr('name'), file);
            }
        });
        getAjax(action, formData, {type: "PATCH"}).then(
            data => {
                let $check = $(opt.selectorCheckData);
                if (!data.vendor_driver_id) {
                    $check.filter(".m--driver").children(".fas").addClass("d-none");
                } else {
                    $check.filter(".m--driver").children(".fas").removeClass("d-none");
                }
                if (!data.vendor_car_id) {
                    $check.filter(".m--car").children(".fas").addClass("d-none");
                } else {
                    $check.filter(".m--car").children(".fas").removeClass("d-none");
                }
                if (!data.vendor_car_id || !data.vendor_driver_id) {
                    toggleDocumentsVisible(false);
                } else {
                    toggleDocumentsVisible(true);
                }
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    }
    
    function toggleDocumentsVisible(show) {
        let $blockInfo = $(opt.selectorDataBlockInfo);
        if (show) {
            $blockInfo.filter('.m--achieved').removeClass('d-none');
            $blockInfo.filter('.m--not-achieved').addClass('d-none');
        } else {
            $blockInfo.filter('.m--achieved').addClass('d-none');
            $blockInfo.filter('.m--not-achieved').removeClass('d-none');
        }
    }
}

export {
    Tender
};

