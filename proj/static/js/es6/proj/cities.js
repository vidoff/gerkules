'use strict';
/* global $ */
import {ShowForm} from './form.js';
import {getAjax} from '../utils/ajax_new';

function Cities(options) {
    let opt = $.extend(true, {
        selectorGeoCaption: '.js-geo-caption',
        selectorGeoChildren: '.js-geo-children',
        selectorGeoCheckbox: '.js-geo-checkbox',
        selectorGeoCity: '.js-geo-city',
        selectorShowForm: '.js-city-create',
        selectorShowCities: '.js-cities-show',
        selectorMailSend: '.js-city-email-send',
        activeClassName: 'is-active',
        formGeo: '.js-form-geo'
    }, options);
    
    $(document).on("click", opt.selectorGeoCaption, function (e) {
        let $parent = $(this).parent();
        let $children = $parent.next().children(opt.selectorGeoChildren);
        
        if ($parent.hasClass(opt.activeClassName)) {
            $children.removeClass(opt.activeClassName);
            $parent.removeClass(opt.activeClassName);
        } else {
            $children.addClass(opt.activeClassName);
            $parent.addClass(opt.activeClassName);
        }
    });

    $(document).on("click", opt.selectorShowCities, function (e) {
        e.preventDefault();
        $(this).next().toggleClass(opt.activeClassName);
    });

    $(document).on("change", opt.selectorMailSend, function(e){
        let opt = {};
        let action = $(this).data('action');

        if($(this).prop('checked')){
            opt = JSON.stringify({'tender_created_email': true});
        } else {
            opt = JSON.stringify({'tender_created_email': false});
        }
        getAjax(action, opt, {type: "PATCH", contentType: 'application/json'}).then(
            data => {
                console.log(data);
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
    $(document).on("click", opt.selectorGeoCheckbox, function (e) {
        let $parent = $(this).parent();
        let $nextWrapper = $parent.next();
        let ret_str = "";
        if ($(this).prop("checked")) {
            $parent.addClass(opt.activeClassName);
            $nextWrapper.find(opt.selectorGeoChildren).addClass(opt.activeClassName);
            $nextWrapper.find("input:checkbox").prop("checked", true);
        } else {
            $parent.removeClass(opt.activeClassName);
            // $nextWrapper.parent().prev().removeClass(opt.activeClassName);
            $nextWrapper.find(opt.selectorGeoChildren).removeClass(opt.activeClassName)
            $nextWrapper.find("input:checkbox").prop("checked", false);
        }
    });
    
    //Добавить документы
    $(opt.selectorShowForm).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        ShowForm($form.html());
    });
    
    $(document.body).on('submit', opt.formGeo, function (e) {
        e.preventDefault();
        let res_str = "";
        let $cities = $(this).find(opt.selectorGeoCity + ':checked');
        if ($cities.length > 0) {
            $cities.each(function (e) {
                res_str += $(this).val() + ",";
            });
        }
        res_str = res_str.substring(0, res_str.length - 1);
        console.log(res_str);
        let params = $.extend(true, opt, {'res_str': res_str});
        SubmitForm(this, params);
    });
    
    function SubmitForm(el, options) {
        let $form = $(el);
        let action = $form.attr('action').trim();
        let opt = $.extend({}, options);
        let ajaxParams = $.extend({}, {type: opt.type});

        let $fields = $form.find(opt.fieldSelector);
        let formData = new FormData();
        formData.append('cities', opt.res_str);

        // $(opt.fieldErrorText).text('');

        getAjax(action, formData, ajaxParams).then(
            data => {
                if (opt.resultFunction) {
                    opt.resultFunction($form, data);
                } else {
                    $.fancybox.close();
                    location.reload();
                }
            },
            error => {
                // console.log(error.responseText);
                let errors = JSON.parse(error.responseText);
                for (let p in errors) {
                    $fields.filter('.m--name-' + p).each(function (i) {
                        $(this).addClass(opt.errorClass);
                        $(this).parents(opt.formGroup).find(opt.fieldErrorText).text(errors[p][0]);
                    });
                }
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    }
}

export {
    Cities
};
