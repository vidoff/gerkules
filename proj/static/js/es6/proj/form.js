'use strict';

/* global $ */

// import {getCookie} from '../utils/cookie.js';
import {getAjax} from '../utils/ajax_new';
import {fancyboxForm, fancyboxDefault} from '../utils/params'
import {getCookie} from '../utils/cookie.js';

let formSettings = {
    formSelector: '.js-form',
    formPatch: '.js-form-patch',
    // formUpdate: '.js-form-update',
    formHidden: '.js-form-hidden',
    formGroup: '.form-group',
    fieldErrorText: '.js-field-error',
    linkSelector: '.js-form-link',
    fieldSelector: '.js-form-field',
    errorClass: 'is-invalid',
    selectorSubmitForm: '.js-form-submit',
    selectorCheckbox: '.js-form-field__checkbox',
};


function Form(options) {
    let opt = $.extend(true, {}, formSettings, options);
    $(document.body).on('change', opt.selectorCheckbox, function (e) {
        e.preventDefault();
        if ($(this).prop("checked")) {
            $(this).val("true");
        } else {
            $(this).val("false");
        }
    });
    
    $(document.body).on('submit', opt.formHidden, function (e) {
        e.preventDefault();
        SubmitForm(this, opt);
    });
    
    $(document.body).on('submit', opt.formPatch, function (e) {
        e.preventDefault();
        let params = $.extend(true, opt, {type: "PATCH"});
        SubmitForm(this, params);
    });
    
    $(document.body).on('submit', opt.formSelector, function (e) {
        e.preventDefault();
        let params = $.extend(true, opt, {type: "POST"});
        SubmitForm(this, params);
    });
}

function ShowForm(content, options, workers=null) {
    if (workers){
        for (let worker in workers){
            content = workers[worker](content);
        }
    }
    let params = $.extend(true, options, {});
    $.fancybox.open(content, params);
}

function GetForm(action, options, workers=null) {
    let opt = $.extend({}, {format: 'form'}, options);
    
    getAjax(action, opt, {type: "GET"}).then(
        data => {
            ShowForm(data.content, {}, workers);
        },
        error => {
            console.log(error.responseText);
        }
    ).catch(function (error) {
        throw new Error(error);
    });
}

function SubmitForm(el, options) {
    let $form = $(el);
    let action = $form.attr('action').trim();
    let opt = $.extend({}, options);
    let ajaxParams = $.extend({}, {type: opt.type});
    
    let $fields = $form.find(opt.fieldSelector);
    let formData = new FormData();
    
    $fields.each(function (index) {
        $(this).removeClass(opt.errorClass);
        if ($(this).attr("type") != "file") {
            formData.append($(this).attr('name'), $(this).val());
        } else {
            let file = $(this).prop("files")[0];
            formData.append($(this).attr('name'), file);
        }
    });
    $(opt.fieldErrorText).text('');
    
    
    getAjax(action, formData, ajaxParams).then(
        data => {
            if (opt.resultFunction) {
                opt.resultFunction($form, data);
            } else {
                $.fancybox.close();
                location.reload();
            }
        },
        error => {
            // console.log(error.responseText);
            let errors = JSON.parse(error.responseText);
            for (let p in errors) {
                $fields.filter('.m--name-' + p).each(function (i) {
                    $(this).addClass(opt.errorClass);
                    $(this).parents(opt.formGroup).find(opt.fieldErrorText).text(errors[p][0]);
                });
            }
        }
    ).catch(function (error) {
        throw new Error(error);
    });
}

export {
    Form,
    ShowForm,
    GetForm,
    SubmitForm,
    formSettings,
};
