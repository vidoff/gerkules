'use strict';

/* global $ */

import {getAjax} from '../utils/ajax_new';
import {getCookie} from '../utils/cookie.js';
import {GetForm, ShowForm} from './form.js';


function Profile(options) {
    
    let opt = $.extend(true, {
        selectorVendorCreate: '.js-vendor-create',
        selectorItemEdit: '.js-edit-item',
        selectorItemDelete: '.js-delete-item',
        vendorCarCreate: '.js-car-create',
        vendorDocumentCreate: '.js-document-create',
        vendorDriverCreate: '.js-driver-create',
        vendorCityCreate: '.js-city-create',
        vendorEditPerson: '.js-vendor-edit-person',
        vendorEditCompany: '.js-vendor-edit-company',
        formHidden: '.js-form-hidden',
        formBan: '.js-form-ban',
        selectorUserEdit: '.js-user-edit',
        selectorUserBan: '.js-user-ban',
        selectorUserPassword: '.js-user-changepassword',
        selectorProfileItemsWrapper: '.js-profile-items-wrapper',
        selectorProfileItem: '.js-profile-item',
    }, options);
    
    
    //добавить поставщика
    $(opt.selectorVendorCreate).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        ShowForm($form.html());
    });
    
    //показать форму редактирования персональных данных
    $(opt.vendorEditPerson).click(function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        GetForm(href, {type: 'person'});
    });
    
    //показать форму редактирования данных компании
    $(opt.vendorEditCompany).click(function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        GetForm(href, {type: 'company'});
    });
    
    //показать форму редактирование пользователя
    $(opt.selectorUserEdit).click(function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        GetForm(href, {type: 'person'});
    });
    //сменить пароль у пользователя
    $(opt.selectorUserPassword).click(function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        GetForm(href, {type: 'password'})
    });
    
    //изменить is_active у поставщика
    $(opt.selectorUserBan).click(function (e) {
        e.preventDefault();
        let action = $(this).attr('href');
        let is_active = $(this).hasClass('m--lock');
        let opt = JSON.stringify({'is_active': is_active});
        getAjax(action, opt, {type: "PATCH", contentType: 'application/json'}).then(
            data => {
                if (is_active) {
                    $(this).removeClass('m--lock').parents('tr').removeClass('m--lock');
                } else {
                    $(this).addClass('m--lock').parents('tr').addClass('m--lock');
                }
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
    
    //Добавить документы
    $(opt.vendorDocumentCreate).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        ShowForm($form.html());
    });
    
    //Добавить машины
    $(opt.vendorCarCreate).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        ShowForm($form.html());
    });

    //Добавить город
    $(opt.vendorCityCreate).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        console.log('teset');
        // ShowForm($form.html());
    });

    
    //Добавить водителя
    $(opt.vendorDriverCreate).click(function (e) {
        e.preventDefault();
        let $form = $('.js-form-wrapper');
        ShowForm($form.html());
    });
    
    //Редактирование данных
    $(opt.selectorItemEdit).click(function (e) {
        e.preventDefault();
        GetForm($(this).attr('href'));
    });
    
    //Удаление данных
    $(document.body).on('click', opt.selectorItemDelete, function (e) {
        e.preventDefault();
        let ajax_options = {type: 'DELETE'};
        let params = {};
        let href = $(this).parents("form").attr("action").trim();
        
        if (confirm("Подтвердите удаление")) {
            getAjax(href, params, ajax_options).then(
                data => {
                    console.log($(opt.selectorProfileItem).length);
                    if ($(opt.selectorProfileItem).length === 1) {
                        $(opt.selectorProfileItemsWrapper).remove();
                    } else {
                        $("#item-" + data.pk).remove();
                    }
                    $.fancybox.close();
                },
                error => {
                    console.log(error.responseText);
                }
            ).catch(function (error) {
                throw new Error(error);
            });
            
        } else {
            console.log('false');
        }
    });
}

export {
    Profile
};

