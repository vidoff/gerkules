'use strict';

/* global $ */

import {getAjax} from '../utils/ajax_new';
import {GetForm, ShowForm, SubmitForm, formSettings} from './form.js';


function Notice(options) {
    
    let opt = $.extend(true, {
        selectorNoticeDisable: '.js-notice-disable',
        selectorNoticeCount: '.js-notice-count',
        selectorNoticesClear: '.js-notices-clear',
    }, options);
    $(document).on('click', opt.selectorNoticeDisable, function (e) {
        let action = $(this).data('action');
        let is_active = false;
        let params = JSON.stringify({'is_active': is_active});
        let ajax_params = {
            type: "PATCH",
            contentType: 'application/json',
        };
        getAjax(action, params, ajax_params).then(
            data => {
                NoticeCheck();
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
    $(document).on('click', opt.selectorNoticesClear, function (e) {
        console.log('test');
        let action = $(this).data('action');
        let is_active = false;
        let params = JSON.stringify({'is_active': is_active, 'select': 'all'});
        let ajax_params = {
            type: "DELETE",
            contentType: 'application/json',
        };
        getAjax(action, params, ajax_params).then(
            data => {
                // console.log(data)
                // NoticeCheck();
                location.reload();
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });

    function NoticeCheck() {
        let $count = $(opt.selectorNoticeCount).children();
        let num = Number($count.text());
        if (num > 1) {
            num--;
        } else {
            $count.parent().remove();
        }
        $count.text(num);
    }
}

export {
    Notice
};

