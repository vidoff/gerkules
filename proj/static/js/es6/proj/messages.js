'use strict';
/* global $ */
import {GetForm, ShowForm} from './form.js';
import {getAjax} from '../utils/ajax_new';

function Messages(options) {
    let opt = $.extend(true, {
        selectorRoomCreate: '.js-room-create',
        selectorMessageBody: '.js-message-body',
        selectorFormMessage: '.js-form-message',
        fieldSelector: '.js-form-field',
    }, options);
    
    let $messageBody = $(opt.selectorMessageBody);
    
    if ($messageBody[0]) {
        $messageBody.scrollTop($messageBody[0].scrollHeight);
    }
    
    $(document).on('click', opt.selectorRoomCreate, function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        GetForm(href, {});
    });
    
    $(document.body).on('submit', opt.selectorFormMessage, function (e) {
        e.preventDefault();
        let action = $(this).attr('action');
        let formData = new FormData();
        let $fields = $(this).find(opt.fieldSelector);
        let ajaxParams = {};
        
        $fields.each(function (index) {
            formData.append($(this).attr('name'), $(this).val());
        });
        
        getAjax(action, formData, ajaxParams).then(
            data => {
                // console.log(data);
                location.reload();
            },
            error => {
                console.log(error.responseText);
            }
        ).catch(function (error) {
            throw new Error(error);
        });
    });
}

export {
    Messages
};
