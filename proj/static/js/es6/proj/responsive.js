'use strict';

/* global $ */

import { breakpoints } from '../utils/params';

function Responsive(options) {
    let opt = $.extend(true, {
        breakpoints: breakpoints
    }, options);

    let $window = $(window),
        currentPoint = undefined;

    function watchSize() {
        let width = $window.width(),
            point = 'xs';

        if (width > opt.breakpoints.l) {
            point = 'xl';
        } else if (width > opt.breakpoints.m) {
            point = 'l';
        } else if (width > opt.breakpoints.s) {
            point = 'm';
        } else if (width > opt.breakpoints.xs) {
            point = 's';
        }

        if (!currentPoint || point != currentPoint) {
            if (currentPoint) {
                $window.trigger('Responsive.off.'+currentPoint);
            }

            $window.trigger('Responsive.on.'+point);

            currentPoint = point;
        }
    }

    $window.on('resize', watchSize);
    watchSize();
}

export {
    Responsive
};

