from django.urls import path

from .viewsets import (
    EventsListViewSet,
    EventsViewSet,
)

urlpatterns = [
    path("", EventsListViewSet.as_view({
        "get": "list",
    }), name="notices"),
    path("/<int:pk>/", EventsViewSet.as_view({
        "patch": "partial_update"
    }), name="notice_detail")
]
