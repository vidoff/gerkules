from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import timedelta
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from app.models import Tender, Bet, Task, SiteSettings
from users.models import VendorUser, VendorSettings
from django.shortcuts import reverse
from geo.models import City
from django.core.mail import send_mail as django_send_mail
import logging

RECEIVERS = (
    ("staff", "менеджеры"),
    ("vendors", "поставщики"),
    ("single", "личный"),
    ("all", "все"),
)

EVENT_TYPE = (
    ("tender_created", "Тендер создан"),
    ("tender_closed", "Тендер победа"),
    ("tender_archived", "Тендер архив"),
    ("tender_archived_win", "Тендер архив с победой"),
    ("tender_documents", "Тендер документы добавлены"),
    ("tender_confirmed", "Тендер подтвержден"),
    ("bet_beat", "Ставка перебита"),
    ("bet_win", "Ставка сыграла"),
    ("other", "другой"),
)


class Event(models.Model):
    pub_date = models.DateTimeField("Дата создания", auto_now_add=True)
    publication = models.BooleanField("Публиковать?", default=True)
    members = models.ManyToManyField(
        User, through="Notice", through_fields=("event", "user")
    )
    receivers = models.CharField(
        "Тип получателей", choices=RECEIVERS, default="all", max_length=50
    )
    type = models.CharField(
        "Тип уведомления", choices=EVENT_TYPE, default="other", max_length=50
    )

    def older_week(self):
        events = self.objects.filter(pub_date__lte=timezone.now() - timedelta(7))
        return events

    def notices_active(self, user):
        notices = self.notices.filter(user=user, is_active=True)
        if notices.count() >= 0:
            return notices
        return None

    @classmethod
    def create(cls, **kwargs):
        event = cls(**kwargs)
        event.save()
        return event

    def __str__(self):
        return self.get_type_display()

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "События"


class Notice(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="notices")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notices")
    pub_date = models.DateTimeField("Дата создания", auto_now_add=True, null=True)
    title = models.CharField("Название события", max_length=255, blank=True, null=True)
    link = models.CharField("Ссылка на объект", max_length=255, blank=True, null=True)
    description = models.TextField("Описание", blank=True, null=True)
    is_active = models.BooleanField("Флаг активности", default=True)

    class Meta:
        verbose_name = "Уведомление"
        verbose_name_plural = "Уведомления"


def create_notice(event_type, notice={}, receivers="all", **kwargs):
    user = notice.pop("user", None)
    users = User.objects.filter(is_active=True)
    if isinstance(user, User):
        receivers = "single"
        users = user
    elif receivers == "staff":
        users = users.filter(is_staff=True)
    elif receivers == "vendors":
        users = users.filter(is_staff=False)

    event = Event.create(receivers=receivers, type=event_type)
    try:
        iter(users)
    except TypeError:
        Notice.objects.create(event=event, user=users, **notice)
        return True
    else:
        notices = (Notice(event=event, user=user, **notice) for user in users)
        if Notice.objects.bulk_create(notices):
            return True
    return False


def create_log(message):
    logging.basicConfig(filename="../log/log.log", level=logging.DEBUG)
    logging.debug(message)


# Для тестирования
# функция создания, инкрементируется последний номер code
def create_tender(city_name="Челябинск"):
    last = Tender.objects.last()
    if last is not None:
        code = last.code + 1
    else:
        code = 1
    name = "TEST: {}".format(code)
    status = "new"
    date_start = timezone.now()
    date_end = timezone.now() + timedelta(1)
    bet_step = 1000
    tender = Tender.objects.create(
        code=code,
        name=name,
        status=status,
        date_start=date_start,
        date_end=date_end,
        bet_step=bet_step,
    )
    task_name = name
    task_date_start = date_start
    task_date_end = date_end
    task_point_to = city_name
    task_point_from = ("Новосибирск",)
    vehicle = ("задняя",)

    task = Task.objects.create(
        name=task_name,
        date_start=task_date_start,
        date_end=task_date_end,
        point_to=task_point_to,
        point_from=task_point_from,
        tender=tender,
        vehicle=vehicle,
    )


# Уведомления

# # отправка уведомлений при создании тендера, делаем по задаче Task
# фильтрация если у поставщика выставлены города по которым посылать уведомления
@receiver(post_save, sender=Task)
def event_created_tender(sender, instance, created, **kwargs):
    try:
        tender = Tender.objects.get(pk=instance.tender_id)
    except Tender.DoesNotExist:
        # create_log(Tender.DoesNotExist)
        pass
    else:
        if created and tender.date_end > timezone.now():
            site_settings = SiteSettings.objects.first()
            city = City.get_or_create(name=instance.point_to)
            vs = VendorSettings.objects.all()
            users = User.objects.filter(is_active=True, is_staff=False)
            message = "Создан тендер #{}, пункт назначения: {}".format(
                tender.code, instance.point_to
            )
            url = reverse("vendor:tender_detail", kwargs={"pk": tender.pk})
            link = "<a href='{}'>{}</a>".format(url, instance.name)
            if site_settings is not None:
                template = site_settings.tender_created_email_template
                message_email = template.format(
                    code=tender.code,
                    date_start=tender.date_start.strftime("%Y-%m-%d %H:%M"),
                    date_end=tender.date_end.strftime("%Y-%m-%d %H:%M"),
                    point_to=instance.point_to,
                    name=tender.name,
                    link="https://logist.gerkules.ru"
                    + reverse("vendor:tender_detail", kwargs={"pk": tender.pk}),
                )

            city_vendor_settings = city.vendorsettings_set.all()
            for settings in vs:
                # у кого проставлены города
                if settings.city_count():
                    if settings not in city_vendor_settings:
                        continue
                try:
                    user = users.get(vendoruser__settings=settings)
                except User.DoesNotExist:
                    continue
                if settings.tender_created_notice:
                    create_notice(
                        "tender_created", {"title": message, "link": link, "user": user}
                    )
                if settings.tender_created_email and site_settings is not None:
                    django_send_mail(
                        "Создан тендер {}".format(tender.name),
                        message_email,
                        "logist@gerkules.ru",
                        [user.vendoruser.email],
                        fail_silently=True,
                    )


# Перебитие ставки поставщиком другого поставщика
@receiver(pre_save, sender=Bet)
def event_save_bet(sender, instance, **kwargs):
    if instance.publication:
        try:
            bet_old = instance.tender.get_bet_min()
        except Tender.DoesNotExist:
            pass
        else:
            if bet_old is not None and bet_old.user != instance.user:
                message = "Тендер: №{} <br>Ваша ставка перебита.<br> Ставка: {}, новая ставка: {}".format(
                    instance.tender.code, bet_old.bet, instance.bet
                )
                url = reverse("vendor:tender_detail", kwargs={"pk": instance.tender.pk})
                link = "<a href='{}'>{}</a>".format(url, instance.tender.name)
                create_notice(
                    "bet_beat", {"user": bet_old.user, "title": message, "link": link}
                )
                # TODO отмена уведомления о перебитии ставки, если отменена
