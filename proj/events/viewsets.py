from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from django.core.exceptions import PermissionDenied
from rest_framework.parsers import (
    MultiPartParser,
    JSONParser,
)
from .models import Event, Notice
from .serializers import EventSerializer, NoticeSerializer


class NoticeListViewSet(viewsets.ModelViewSet):
    template_name = "app/interfaces/notices/list.html"
    serializer_class = EventSerializer
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer,)

    def get_queryset(self, user):
        # qs = Event.objects.filter(members=user).order_by("pub_date")[:10]
        qs = Notice.objects.filter(user=user, is_active=True).order_by('-event__pub_date')[:20]
        return qs

    def get_context_data(self, request, *args, **kwargs):
        context = {
            'notices': self.get_queryset(request.user),
        }
        return context

    def list(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return Response(data=context, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        user = request.user
        notices = Notice.objects.filter(user=user, is_active=True)
        notices.update(is_active = False)
        context = {
            'data': {
                'content': 'ok',
                }
        }
        return Response(data=context, status=status.HTTP_200_OK)



class NoticeViewSet(viewsets.ModelViewSet):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser, MultiPartParser,)

    def retrieve(self, request, *args, **kwargs):
        raise PermissionDenied

    def partial_update(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            notice = Notice.objects.get(user=request.user, pk=pk)
        except Notice.DoesNotExist:
            raise PermissionDenied
        serializer = NoticeSerializer(notice, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)
