from django.contrib import admin
from .models import Event, Notice


class NoticeAdmin(admin.ModelAdmin):
    list_display = ("title",)


class EventsAdmin(admin.ModelAdmin):
    list_display = ("get_type_display", "pub_date")
    ordering = ('-pub_date',)


admin.site.register(Event, EventsAdmin)
admin.site.register(Notice, NoticeAdmin)

# Register your models here.
