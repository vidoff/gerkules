from .base import *


DEBUG = True
# DEBUG = False


REST_FRAMEWORK = {
    "DEFAULT_PARSER_CLASSES": (
        # "rest_framework_xml.parsers.XMLParser",
        "rest_framework.parsers.MultiPartParser",
    ),
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
}


RUNSERVERPLUS_POLLER_RELOADER_INTERVAL = 5

ALLOWED_HOSTS = ["logist.gerkules.ru"]
CSRF_COOKIE_DOMAIN = "logist.gerkules.ru"

LOGIN_REDIRECT_URL = "/"
LOGIN_URL = "/auth/login/"

CRISPY_TEMPLATE_PACK = "bootstrap4"

INSTALLED_APPS = [
    "rest_framework",
    "rest_framework_xml",
    "django_extensions",
    # "django-autoslug",
    "rest_framework.authtoken",
    "bootstrap4",
    "geo",
    "users",
    "events",
    "chat",
    "app",
    "widget_tweaks",
    "django_filters",
    "crispy_forms",
    "django_celery_beat",
    "django_celery_results",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # 'DIRS': ['./app/templates/app', os.path.join(BASE_DIR, "../app/templates/app")],
        "DIRS": ["./app/templates/app"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "libraries": {"templatetags": "app.templatetags.templatetags"},
        },
    }
]


JS_URLS = STATIC_URL + "js/"

STATICFILES_DIRS = [os.path.join(BASE_DIR, "../static")]
