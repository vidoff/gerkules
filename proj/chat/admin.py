from django.contrib import admin
from .models import Room, RoomProfile, Message

# Register your models here.
admin.site.register(Room)
admin.site.register(RoomProfile)
admin.site.register(Message)