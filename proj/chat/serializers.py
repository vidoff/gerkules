from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Room, Message
from .models import create_message
from django.db.models import Q
from users.models import VendorUser, ManagerUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class RoomSerializer(serializers.ModelSerializer):
    members = UserSerializer(many=True)

    class Meta:
        model = Room
        fields = (
            "members",
            "message",
            "is_active",
            "created",
            "is_many",
        )

# Вывод для сотрудника
class VendorPrimaryKeyRelated(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        qs = super(VendorPrimaryKeyRelated, self).get_queryset()
        queryset = qs.filter(~Q(username='api'))
        return queryset

    def display_value(self, instance):
        try:
            instance.vendoruser is not None
        except VendorUser.DoesNotExist:
            pass
        else:
            return instance.vendoruser.company_name

        try:
            instance.manageruser is not None
        except ManagerUser.DoesNotExist:
            pass
        else:
            return instance.manageruser.name

        name = instance.username
        return name


# Вывод для поставщика
class StaffPrimaryKeyRelated(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        qs = super(StaffPrimaryKeyRelated, self).get_queryset()
        queryset = qs.filter(~Q(username='admin') & ~Q(username='api'))
        return queryset

    def display_value(self, instance):
        try:
            instance.manageruser is not None
        except ManagerUser.DoesNotExist:
            pass
        else:
            return instance.manageruser.name
        name = instance.username
        return name



class MessageCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        if isinstance(self.receiver, User) and isinstance(self.sender, User):
            instance = create_message(self.receiver, self.sender, validated_data["message"])
            return instance
        return False

    def validate(self, attrs):
        message = attrs.get("message")
        if not message:
            raise serializers.ValidationError({"message": ["Пустое сообщение"]})
        return super(MessageCreateSerializer, self).validate(attrs)

    def to_internal_value(self, data):
        message_data = data.copy()
        receiver_id = message_data.pop("receiver", None)[0]
        if receiver_id is not None:
            try:
                self.receiver = User.objects.get(pk=receiver_id)
            except User.DoesNotExist:
                raise serializers.ValidationError({"receiver": ["нет такого пользователя"]})
        request = self.context.get("request")
        self.sender = request.user
        message_data["sender"] = self.sender.pk
        return super(MessageCreateSerializer, self).to_internal_value(message_data)

    class Meta:
        model = Message
        fields = (
            "message",
            "sender",
            "is_active",
        )


class MessageOriginalSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        message = attrs.get("message")
        attrs["is_active"] = True
        if not message:
            raise serializers.ValidationError({"message": ["Пустое сообщение"]})
        return super(MessageOriginalSerializer, self).validate(attrs)

    class Meta:
        model = Message
        fields = "__all__"


# Вывод для поставщика
class MessageSerializer(serializers.ModelSerializer):
    receiver = StaffPrimaryKeyRelated(label="Менеджеры",
                                       queryset=User.objects.filter(is_active=True, is_staff=True))

    class Meta:
        model = Message
        fields = (
            "message",
            "receiver",
        )


# Вывод для сотрудника
class MessageStaffSerializer(serializers.ModelSerializer):
    receiver = VendorPrimaryKeyRelated(label="Пользователи",
                                       queryset=User.objects.filter(is_active=True).order_by('is_staff'))

    class Meta:
        model = Message
        fields = (
            "message",
            "receiver",
        )
