from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.exceptions import NotFound
from django.contrib.auth.models import User
from rest_framework.parsers import (
    MultiPartParser,
    JSONParser,
)
from .models import Room, RoomProfile, Message
from .serializers import MessageSerializer, MessageStaffSerializer, MessageCreateSerializer, MessageOriginalSerializer
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.shortcuts import reverse
from django.db.models import Q


class RoomListViewSet(viewsets.ModelViewSet):
    template_name = "app/interfaces/messages/room_list.html"
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer,)
    form_action = ""

    # def get_queryset(self, user):
    #     qs = Event.objects.filter(members=user)
    #     return qs

    def get_context_data(self, request, *args, **kwargs):
        rooms = RoomProfile.objects.filter(user=request.user, is_active=True)
        for room in rooms:
            # room.unread = room.room.messages.filter(is_active=True).exclude(sender=request.user).count()
            room.unread = room.room.messages.filter(Q(is_active=True) & ~Q(sender=request.user)).count()
        serializer = MessageSerializer()
        context = {
            'rooms': rooms,
            'serializer': serializer
        }
        return context

    def list(self, request, *args, **kwargs):
        context = self.get_context_data(request)
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = MessageCreateSerializer(data=data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            context = {
                "data": serializer.data
            }
            return Response(data=context, status=status.HTTP_200_OK)
        else:
            context = {
                "data": {"errors": serializer.errors}
            }
            return Response(data=context, status=status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        if request.user.is_staff:
            serializer = MessageStaffSerializer()
        else:
            serializer = MessageSerializer()
        if not self.form_action:
            serializer.action = reverse(
                "vendor:rooms"
            )
        else:
            serializer.action = self.form_action

        context = {
            "form": serializer,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class RoomDetailViewSet(viewsets.ModelViewSet):
    template_name = "app/interfaces/messages/room_detail.html"
    parser_classes = (JSONParser, MultiPartParser,)

    def get_context_data(self, request, *args, **kwargs):
        context = {}
        return context

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            room = Room.objects.get(pk=pk)
        except Room.DoesNotExist:
            raise NotFound
        messages = Message.objects.filter(room=room).order_by('updated')
        try:
            room_profile = RoomProfile.objects.get(user=request.user, room=room)
        except RoomProfile.DoesNotExist:
            raise NotFound

        context = self.get_context_data(request)

        message_active = messages.exclude(sender=request.user)
        for message in message_active:
            message.is_active = False
            message.save()

        context.update({
            "messages": messages,
            "room_profile": room_profile
        })
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data["sender"] = request.user.id
        serializer = MessageOriginalSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "data": "ok"
            }
            return Response(data=context, status=status.HTTP_200_OK)

        context = {
            "data": {"errors": serializer.errors}
        }
        return Response(data=context, status=status.HTTP_400_BAD_REQUEST)
