from django.db import models
from django.contrib.auth.models import User


class Room(models.Model):
    created = models.DateTimeField("Дата/Время создания", auto_now_add=True)
    # creator = models.OneToOneField(User, on_delete=models.CASCADE,related_name="room_creator")
    members = models.ManyToManyField(User, through="RoomProfile", through_fields=('room', 'user'), related_name="rooms")
    is_active = models.BooleanField("Флаг активности", default=True)
    name = models.CharField("Название комнаты", max_length=200, null=True, blank=True)
    is_many = models.BooleanField("Множественная", default=False)


class RoomProfile(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name="profiles")
    name = models.CharField("Название комнаты", max_length=200, null=True, blank=True)
    name_html = models.CharField("Название комнаты HTML", max_length=200, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="room_profiles")
    is_active = models.BooleanField("Флаг активности", default=True)


class Message(models.Model):
    room = models.ForeignKey("Room", on_delete=models.CASCADE, related_name="messages")
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="messages")
    is_active = models.BooleanField("Флаг активности", default=True)
    message = models.TextField("Сообщение", null=True, blank=True)
    updated = models.DateTimeField("Дата/Время обновления", auto_now_add=True)

    def __str__(self):
        return self.message


def get_room(receiver, sender):
    rooms = (receiver.rooms.filter(is_many=False) & sender.rooms.filter(is_many=False))
    if rooms.count() == 1:
        return rooms.first()
    return create_room(receiver, sender)


def create_room(receiver, sender, room_name=None):
    room = Room()
    room.save()
    if sender.is_superuser:
        sender_name = sender.username
    elif sender.is_staff:
        sender_name = sender.manageruser.name
    else:
        sender_name = sender.vendoruser.company_name

    if receiver.is_superuser:
        receiver_name = receiver.username
    elif receiver.is_staff:
        receiver_name = receiver.manageruser.name
    else:
        receiver_name = receiver.vendoruser.company_name

    RoomProfile.objects.bulk_create([
        RoomProfile(room=room, user=sender, name=receiver_name),
        RoomProfile(room=room, user=receiver, name=sender_name)
    ])
    return room


def create_message(receiver, sender, message):
    room = get_room(receiver, sender)
    if isinstance(room, Room) and message is not None:
        message = Message.objects.create(sender=sender, room=room, message=message)
        return message
    return None
