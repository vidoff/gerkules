import logging
from pprint import pprint


class LogFormatter(logging.Formatter):
    def uses_server_time(self):
        return self._fmt.find("{server_time}") >= 0

    def format(self, record):
        msg = record.msg
        if self.uses_server_time() and not hasattr(record, "server_time"):
            record.server_time = self.formatTime(record, self.datefmt)

        record.msg = msg
        return super().format(record)


class RequestFormatter(LogFormatter):
    def format(self, record):
        msg = record.msg
        status_code = getattr(record, "status_code", None)

        # self.myLogger.debug(vars(record))
        if status_code:
            # record.status_code = status_code
            if 200 <= status_code < 300:
                # Put 2XX first, since it should be the common case
                msg = self.style.HTTP_SUCCESS(msg)
            elif 100 <= status_code < 200:
                msg = self.style.HTTP_INFO(msg)
            elif status_code == 304:
                msg = self.style.HTTP_NOT_MODIFIED(msg)
            elif 300 <= status_code < 400:
                msg = self.style.HTTP_REDIRECT(msg)
            elif status_code == 404:
                msg = self.style.HTTP_NOT_FOUND(msg)
            elif 400 <= status_code < 500:
                msg = self.style.HTTP_BAD_REQUEST(msg)
            else:
                # Any 5XX, or any other status code
                msg = self.style.HTTP_SERVER_ERROR(msg)

        if self.uses_server_time() and not hasattr(record, "server_time"):
            record.server_time = self.formatTime(record, self.datefmt)

        record.msg = msg
        return super().format(record)


class APIFormatter(LogFormatter):
    pass


class BetFormatter(LogFormatter):
    pass


class WarningFormatter(LogFormatter):
    pass
