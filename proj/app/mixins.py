from django.shortcuts import redirect
from django.urls.base import reverse
from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import PermissionDenied


class VendorPermissionMixin(AccessMixin):
    staff_redirect_url = "staff:index"

    def get_url(self, request):
        return self.staff_redirect_url

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        elif request.user.is_staff and self.staff_redirect_url:
            return redirect(reverse(self.get_url(request)))
        return super().dispatch(request, *args, **kwargs)


class StaffPermissionMixin(AccessMixin):
    vendor_redirect_url = "vendor:index"
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        elif not request.user.is_staff and self.vendor_redirect_url:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class StaffPermissionJsonMixin(AccessMixin):
    raise_exception = True
    permission_denied_message = "Доступ запрещен"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_staff:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
