from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r"^$", views.Index.as_view(), name="index"),
    url(r"^", include("app.vendor.urls", namespace="vendor")),
    url(r"^staff/", include("app.staff.urls", namespace="staff")),
    url(r"^api/", include("app.api.urls", namespace="api")),
    url(r"^auth/", include("users.auth.urls")),
]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
