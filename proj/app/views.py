from django.shortcuts import redirect, reverse
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import TemplateView
from django.conf import settings
from app.models import Tender
from .filters import TenderListFilter, ArchiveListFilter
from django_filters.views import FilterView


class TenderList(FilterView):
    object_list = Tender.objects.exclude(status="archive").order_by("-pub_date")
    template_name = "app/interfaces/tender/tender.html"
    paginate_by = 10
    limit = None

    def get_queryset(self):
        return self.object_list

    def get_filterset_with_kwargs(self, request, queryset=None, **kwargs):
        filterset = TenderListFilter(request.GET, queryset)
        return filterset

    def get(self, request, *args, **kwargs):
        # self.filterset = TenderListFilter(request.GET, self.get_queryset())
        self.filterset = self.get_filterset_with_kwargs(request, self.get_queryset())

        if self.filterset.is_valid():
            if self.limit is not None:
                self.object_list = self.filterset.qs[:self.limit]
            else:
                self.object_list = self.filterset.qs

        self.paginate_by = self.per_page()
        self.filterset.per_page = settings.PER_PAGE
        self.filterset.default_per_page = settings.DEFAULT_PER_PAGE
        context = self.get_context_data(
            filter=self.filterset, object_list=self.object_list
        )
        return self.render_to_response(context)

    def per_page(self):
        per_page = self.request.GET.get("per_page")
        if any(per_page == k for k, v in settings.PER_PAGE):
            return per_page
        else:
            return self.paginate_by


class ArchiveList(TenderList):
    object_list = Tender.objects.filter(status="archive")
    template_name = "app/interfaces/tender/archive.html"
    limit = 500

    def get_filterset_with_kwargs(self, request, queryset=None, **kwargs):
        filterset = ArchiveListFilter(request.GET, queryset, user=request.user)
        return filterset


class Index(TemplateView):
    template_name = "app/interfaces/home_page.html"

    def get_context_data(self, **kwargs):
        form = AuthenticationForm(self.request)
        context = {"form": form}
        context.update(kwargs)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if request.user.is_authenticated:
            return redirect(reverse("vendor:index"))
        return self.render_to_response(context)
