from rest_framework import serializers
from app.models import Tender, Task, TASK_UNLOADING, Bet
from datetime import datetime, timedelta, time
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models import Min
from django.db import transaction, DatabaseError
from django.core.exceptions import ObjectDoesNotExist


class TaskSerializer(serializers.ModelSerializer):
    vehicle = serializers.CharField()
    date_start = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    date_end = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = Task
        fields = (
            "name",
            "point_from",
            "point_to",
            "date_start",
            "date_end",
            "weight",
            "vehicle",
        )

    def create(self, validated_data):
        tender_data = validated_data.pop("tender")
        tender, created = Tender.objects.get_or_create(**tender_data)
        return Task.objects.create(tender=tender, **validated_data)

    def to_representation(self, instance):
        data = super(TaskSerializer, self).to_representation(instance)
        test = "side"
        data["vehicle"] = test
        return data

    # TODO заменить тип выгрузки транспорта
    def to_internal_value(self, data):
        vehicle = data.get("vehicle")
        if vehicle == "задняя":
            data["vehicle"] = "back"
        elif vehicle == "боковая":
            data["vehicle"] = "side"
        return data


class TenderSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True)
    pub_date = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S", read_only=True, default=datetime.now
    )
    date_start = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    date_end = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    def __init__(self, *args, **kwargs):
        super(TenderSerializer, self).__init__(*args, **kwargs)
        self.qs = Tender.objects.all()

    def create(self, validated_data, *args, **kwargs):
        code = validated_data.get("code")
        try:
            tender = self.qs.get(code=code)
        except Exception:
            tasks_data = validated_data.pop("tasks")
            tender = Tender.objects.create(**validated_data)

            for task_data in tasks_data:
                Task.objects.create(tender=tender, **task_data)
            return tender

        return self.update(tender, validated_data)

    class Meta:
        model = Tender
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "is_active")


class BetSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    tender = TenderSerializer(read_only=True)

    def __init__(self, *args, **kwargs):
        super(BetSerializer, self).__init__(*args, **kwargs)

    def create(self, validated_data, *args, **kwargs):
        bet = validated_data.get("bet")
        request = self.context.get("request")
        user = request.user
        tender_pk = self.context.get("tender_pk")
        if isinstance(user, User):
            try:
                tender = Tender.objects.get(pk=tender_pk)
            except Tender.DoesNotExist:
                raise serializers.ValidationError("Нет такого тендера")
            if not tender.enabled_for_bet():
                raise serializers.ValidationError(
                    {"bet": ["Тендер закрыт для ставок, время вышло"]}
                )
            if tender.status in ["new", "played"]:

                # Проверка на условия тендера
                if tender.bet_init is not None:
                    if bet > tender.bet_init:
                        raise serializers.ValidationError(
                            {"bet": ["Ставка должна быть меньше начальной ставки"]}
                        )
                if tender.bet_step is not None and bet % tender.bet_step != 0:
                    raise serializers.ValidationError(
                        {"bet": ["Ставка должна быть кратна шагу ставки"]}
                    )

                # получаем в переменную минимальную ставку из публикуемых
                bet_min = tender.bets.filter(publication=True).aggregate(Min("bet"))[
                    "bet__min"
                ]

                # для юзера
                bet_min_user = tender.bets.filter(
                    user=user, publication=True
                ).aggregate(Min("bet"))["bet__min"]

                # если пользователь делает ставку больше чем его минимальная
                if bet_min_user and bet_min_user < bet:
                    raise serializers.ValidationError(
                        {"bet": ["Вы уже делали ставку меньше"]}
                    )

                # проверяем ставки по тендеру с приходящей ставкой
                # если есть, то проверяем пользователя, если тот же пользователь включем ставку, иначе создаем
                bets_by_bet = tender.bets.filter(bet=bet)

                if bets_by_bet.filter(publication=True).count():
                    raise serializers.ValidationError(
                        {"bet": ["Такая ставка уже существует"]}
                    )
                elif bets_by_bet.filter(user=user).count():
                    new_bet = bets_by_bet.get(user=user)
                    new_bet.publication = True
                    # new_bet.save()
                else:
                    new_bet = Bet(tender=tender, user=user, **validated_data)

                # сохраняем
                try:
                    with transaction.atomic():
                        new_bet.save()
                except DatabaseError:
                    raise serializers.ValidationError(
                        {"bet": ["Ошибка записи, попробуйте перезагрузить"]}
                    )

                # устанавливаем минимальную для тендера
                if bet_min:
                    if new_bet.bet < bet_min:
                        tender.bet_win = new_bet.bet
                else:
                    tender.bet_win = bet

                # меняем статус тендера
                if tender.status == "new":
                    tender.status = "played"

                tender.save()
                return new_bet
            else:
                raise serializers.ValidationError({"bet": ["Тендер закрыт для ставок"]})
        return super(BetSerializer, self).create(validated_data)

    def validate(self, attrs):
        try:
            bet = attrs["bet"]
        except KeyError:
            raise serializers.ValidationError({"bet": ["нет ставки"]})
        return super(BetSerializer, self).validate(attrs)

    class Meta:
        model = Bet
        fields = ("user", "tender", "bet", "pub_date")
