from django.shortcuts import redirect, reverse
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.template.loader import render_to_string
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.exceptions import NotFound
from django.db.models import Min, Q
from rest_framework.parsers import MultiPartParser, JSONParser, FormParser
from app.viewsets import BetViewSet, BetDetailViewSet
from app.mixins import VendorPermissionMixin
from app.vendor.serializers import (
    VendorUserSerializer,
    VendorDocumentSerializer,
    VendorCarSerializer,
    VendorDriverSerializer,
    VendorBetSerializer,
    VendorTenderSerializer,
    VendorSettingsSerializer,
)
from users.models import VendorUser, Document, Car, Driver, VendorSettings
from app.models import Tender
from events.viewsets import NoticeViewSet, NoticeListViewSet
from chat.viewsets import RoomListViewSet, RoomDetailViewSet
from geo.models import City as GeoCity, Region as GeoRegion, District


class VendorMixin(VendorPermissionMixin):
    @staticmethod
    def get_menu(user=None, *args, **kwargs):
        items = [
            {"name": "Тендеры", "url": "vendor:index"},
            {"name": "Архив", "url": "vendor:tender_archive"},
            {"name": "Уведомления", "url": "vendor:notices", "title": "notices"},
            {"name": "Сообщения", "url": "vendor:rooms", "title": "rooms"},
            {"name": "Мой профиль", "url": "vendor:profile"},
        ]
        menu = {"items": items}
        if user is not None and isinstance(user, User):
            notices = user.notices.filter(is_active=True)
            rooms = user.rooms.all()
            messages = rooms.filter(
                Q(messages__is_active=True) & ~Q(messages__sender=user)
            )
            # messages = user.messages.filter(is_active=True).count()
            menu.update(
                {"notices_count": notices.count(), "messages_count": messages.count()}
            )
        return menu

    @staticmethod
    def get_profile_menu(*args, **kwargs):
        profile_menu = [
            {"name": "Общая информация", "url": "vendor:profile"},
            # {"name": "Активность", "url": "vendor:activity"},
            {"name": "Договоры", "url": "vendor:documents"},
            {"name": "Города", "url": "vendor:cities"},
            {"name": "Машины", "url": "vendor:cars"},
            {"name": "Водители", "url": "vendor:drivers"},
        ]
        return profile_menu


class VendorBaseViewSet(VendorMixin, viewsets.ModelViewSet):
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer)
    parser_classes = (MultiPartParser, FormParser)

    def get_form(self, request, *args, **kwargs):
        raise PermissionDenied

    def dispatch(self, request, *args, **kwargs):
        format = request.GET.get("format")
        if format == "form":
            return self.get_form(request, *args, **kwargs)
        return super(VendorBaseViewSet, self).dispatch(request, *args, **kwargs)


class VendorTenderDetailViewSet(VendorBaseViewSet):
    template_name = "app/vendor/tender/detail.html"
    parser_classes = (JSONParser, MultiPartParser, FormParser)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            tender = Tender.objects.get(pk=pk)
        except Tender.DoesNotExist:
            raise NotFound
        if tender.status == "archive":
            raise PermissionDenied
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except:
            raise PermissionDenied
        if tender.status == "closed" and tender.user_winner != request.user:
            raise PermissionDenied
        user_bets = tender.bets.filter(user=request.user).order_by("-pub_date")
        if user_bets.count():
            tender.user_bet_min = user_bets.filter(publication=True).aggregate(
                Min("bet")
            )["bet__min"]
            try:
                tender.user_bet_last = user_bets.filter(publication=True).latest(
                    "pub_date"
                )
            except ObjectDoesNotExist:
                pass

            if tender.user_bet_min and tender.bet_win == tender.user_bet_min:
                tender.user_bet_best = tender.user_bet_min

        betSerializer = VendorBetSerializer()
        context = {
            "menu": self.get_menu(),
            "vendor": vendor,
            "tender": tender,
            "tasks": tender.tasks.all,
            "form": betSerializer,
            "bets": user_bets.all()[:5],
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied
        try:
            tender = Tender.objects.get(pk=pk)
        except Tender.DoesNotExist:
            raise NotFound
        serializer = VendorTenderSerializer(
            tender, data=request.data, context={"vendor": vendor}
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VendorArchiveTenderDetailViewSet(VendorBaseViewSet):
    template_name = "app/vendor/tender/archive_detail.html"
    parser_classes = (JSONParser, MultiPartParser, FormParser)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            tender = Tender.objects.get(pk=pk)
        except Tender.DoesNotExist:
            raise NotFound
        if tender.status != "archive":
            raise PermissionDenied
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except:
            raise PermissionDenied

        user_bets = tender.bets.filter(user=request.user).order_by("-pub_date")
        if user_bets.count():
            tender.user_bet_min = user_bets.filter(publication=True).aggregate(
                Min("bet")
            )["bet__min"]
            try:
                tender.user_bet_last = user_bets.filter(publication=True).latest(
                    "pub_date"
                )
            except ObjectDoesNotExist:
                pass

            if tender.user_bet_min and tender.bet_win == tender.user_bet_min:
                tender.user_bet_best = tender.user_bet_min

        context = {
            "menu": self.get_menu(),
            "vendor": vendor,
            "tender": tender,
            "tasks": tender.tasks.all,
            "bets": user_bets.all()[:5],
        }
        return Response(data=context, status=status.HTTP_200_OK)


class VendorBetViewSet(BetViewSet):
    pass


class VendorBetDetailViewSet(BetDetailViewSet):
    pass


class VendorDetailViewSet(VendorBaseViewSet):
    serializer_class = VendorUserSerializer
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    template_name = "app/vendor/profile/profile.html"

    def retrieve(self, request, *args, **kwargs):
        user = request.user
        if not isinstance(user, User):
            raise PermissionDenied
        try:
            vendor = VendorUser.objects.get(user=user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        context = {
            "vendor": vendor,
            # "serializer":serializer,
            "menu": self.get_menu(),
            "profile_menu": self.get_profile_menu(),
        }
        return Response(context)

    def partial_update(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        serializer = self.get_serializer(vendor, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "success_content": "Данные успешно обновлены"}
            return Response(context, status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        type = request.GET.get("type")
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        if type == "person":
            exclude_fields = [
                "password",
                "confirm_password",
                "company_name",
                "company_addr",
                "company_post_addr",
                "company_inn",
                "company_seo",
                "company_site",
                "company_phone",
            ]
        elif type == "company":
            exclude_fields = ["password", "confirm_password", "name", "email", "phone"]
        elif type == "password":
            exclude_fields = [
                "name",
                "email",
                "phone",
                "company_name",
                "company_addr",
                "company_post_addr",
                "company_inn",
                "company_seo",
                "company_site",
                "company_phone",
            ]
        else:
            raise PermissionDenied
        serializer = VendorUserSerializer(
            vendor, exclude_fields=exclude_fields, context={"request": request}
        )
        serializer.type = "patch"
        serializer.action = reverse("vendor:profile")
        context = {"form": serializer}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorDocumentsViewSet(VendorBaseViewSet):
    serializer_class = VendorDocumentSerializer
    queryset = Document.objects.all()
    parser_classes = (MultiPartParser, JSONParser)
    template_name = "app/vendor/profile/documents.html"

    def list(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        documents = Document.objects.filter(vendor=vendor).order_by("sign_date")
        serializer = VendorDocumentSerializer()
        if not documents:
            messages.add_message(request, messages.INFO, "Договоров не найдено")
        serializer.action = reverse("vendor:documents")
        context = {
            "documents": documents,
            "vendor": vendor,
            "serializer": serializer,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        serializer = VendorDocumentSerializer(
            data=request.data, context={"vendor": vendor}
        )
        if serializer.is_valid():
            context = {"errors": serializer.errors}
            serializer.save()
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# TODO удаление файла при удалении объекта
class VendorDocumentsDetailViewSet(VendorBaseViewSet):
    # queryset = Document.objects.all()
    parser_classes = (MultiPartParser, FormParser, JSONParser)
    serializer_class = VendorDocumentSerializer

    def retrieve(self, request, *args, **kwargs):
        raise PermissionDenied

    def partial_update(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        pk = kwargs["pk"]
        # file_obj = request.FILES['file_scan']
        try:
            document = Document.objects.get(vendor=vendor, pk=pk)
        except Document.DoesNotExist:
            raise PermissionDenied
        serializer = VendorDocumentSerializer(document, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            document = Document.objects.get(vendor=vendor, pk=pk)
        except Document.DoesNotExist:
            raise PermissionDenied
        document.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            document = Document.objects.get(vendor=vendor, pk=pk)
        except Document.DoesNotExist:
            raise PermissionDenied
        serializer = VendorDocumentSerializer(document)
        serializer.type = "patch"
        serializer.action = reverse("vendor:document_detail", kwargs={"pk": pk})
        context = {"form": serializer, "form_delete_button": True}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorCarsViewSet(VendorBaseViewSet):
    serializer_class = VendorCarSerializer
    queryset = Car.objects.all()
    template_name = "app/vendor/profile/cars.html"

    def list(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        cars = Car.objects.filter(vendor=vendor).order_by("brand")
        serializer = VendorCarSerializer()
        if not cars:
            messages.add_message(
                request, messages.INFO, "Машин для данного пользователя не найдены"
            )
        serializer.action = reverse("vendor:cars")
        context = {
            "cars": cars,
            "vendor": vendor,
            "serializer": serializer,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        serializer = VendorCarSerializer(data=request.data, context={"vendor": vendor})
        if serializer.is_valid():
            serializer.save()
            car_id = serializer.instance.id
            context = {"content": serializer.data, "car_id": car_id}
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        serializer = VendorCarSerializer(exclude_fields=["comment", "manager_comment"])
        serializer.action = reverse("vendor:cars")
        serializer.type = "result"
        context = {"form": serializer, "form_delete_button": False}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorCarsDetailViewSet(VendorBaseViewSet):
    queryset = Car.objects.all()
    serializer_class = VendorCarSerializer
    renderer_classes = (JSONRenderer,)

    def retrieve(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied
        pk = kwargs["pk"]
        try:
            car = Car.objects.get(pk=pk, vendor=vendor)
        except Car.DoesNotExist:
            raise PermissionDenied
        serializer = VendorCarSerializer(
            car, exclude_fields=["publication", "comment", "manager_comment"]
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        pk = kwargs["pk"]
        try:
            car = Car.objects.get(vendor=vendor, pk=pk)
        except Car.DoesNotExist:
            raise PermissionDenied
        serializer = VendorCarSerializer(car, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            car = Car.objects.get(vendor=vendor, pk=pk)
        except Car.DoesNotExist:
            raise PermissionDenied
        car.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            car = Car.objects.get(vendor=vendor, pk=pk)
        except Car.DoesNotExist:
            raise PermissionDenied
        serializer = VendorCarSerializer(car, exclude_fields=["manager_comment"])
        serializer.type = "patch"
        serializer.action = reverse("vendor:car_detail", kwargs={"pk": pk})
        context = {"form": serializer, "form_delete_button": True}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorDriversViewSet(VendorBaseViewSet):
    serializer_class = VendorDriverSerializer
    queryset = Driver.objects.all()
    template_name = "app/vendor/profile/drivers.html"

    def list(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        drivers = Driver.objects.filter(vendor=vendor).order_by("name")
        serializer = VendorDriverSerializer()
        serializer.action = reverse("vendor:drivers")
        if not drivers:
            messages.add_message(
                request, messages.INFO, "Водители для данного пользователя не найдены"
            )
        context = {
            "drivers": drivers,
            "vendor": vendor,
            "serializer": serializer,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        serializer = VendorDriverSerializer(
            data=request.data, context={"vendor": vendor}
        )
        if serializer.is_valid():
            serializer.save()
            driver_id = serializer.instance.id
            context = {"content": serializer.data, "driver_id": driver_id}
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        serializer = VendorDriverSerializer(
            exclude_fields=["comment", "manager_comment"]
        )
        serializer.action = reverse("vendor:drivers")
        serializer.type = "result"
        serializer.submit = "Сохранить"
        context = {"form": serializer, "form_delete_button": False}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorDriversDetailViewSet(VendorBaseViewSet):
    queryset = Driver.objects.all()
    serializer_class = VendorDriverSerializer
    renderer_classes = (JSONRenderer,)

    def retrieve(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied
        pk = kwargs["pk"]
        try:
            driver = Driver.objects.get(pk=pk, vendor=vendor)
        except Driver.DoesNotExist:
            raise PermissionDenied

        serializer = VendorDriverSerializer(
            driver, exclude_fields=["publication", "comment", "manager_comment"]
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        vendor = request.user.vendoruser
        pk = kwargs["pk"]
        try:
            driver = Driver.objects.get(vendor=vendor, pk=pk)
        except Driver.DoesNotExist:
            raise PermissionDenied
        serializer = VendorDriverSerializer(driver, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            driver = Driver.objects.get(vendor=vendor, pk=pk)
        except Driver.DoesNotExist:
            raise PermissionDenied
        driver.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor = request.user.vendoruser
        try:
            driver = Driver.objects.get(vendor=vendor, pk=pk)
        except Driver.DoesNotExist:
            raise PermissionDenied
        serializer = VendorDriverSerializer(driver)
        serializer.type = "patch"
        serializer.action = reverse("vendor:driver_detail", kwargs={"pk": pk})
        context = {"form": serializer, "form_delete_button": True}
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorNoticeListViewSet(NoticeListViewSet, VendorBaseViewSet):
    def get_context_data(self, request, *args, **kwargs):
        context = super(VendorNoticeListViewSet, self).get_context_data(request)
        context.update({"menu": self.get_menu(request.user)})
        return context


class VendorNoticeViewSet(NoticeViewSet, VendorBaseViewSet):
    pass


class VendorRoomListViewSet(RoomListViewSet, VendorBaseViewSet):
    def get_context_data(self, request, *args, **kwargs):
        context = super(VendorRoomListViewSet, self).get_context_data(request)
        context.update({"menu": self.get_menu(request.user)})
        return context


class VendorRoomDetailViewSet(RoomDetailViewSet, VendorBaseViewSet):
    def get_context_data(self, request, *args, **kwargs):
        context = super(VendorRoomDetailViewSet, self).get_context_data(request)
        context.update({"menu": self.get_menu(request.user)})
        return context


class VendorCitiesViewSet(VendorBaseViewSet):
    serializer_class = VendorSettingsSerializer
    # queryset = City.objects.all()
    template_name = "app/vendor/profile/cities.html"
    parser_classes = (JSONParser, MultiPartParser,)

    def list(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied
        districts = District.objects.filter(is_active=True).order_by('name')
        regions = GeoRegion.objects.filter(is_active=True).order_by('name')
        region_checked = []
        for district in districts:
            row = []
            for region in district.regions.all():
                if region.cities.count() == region.cities.filter(vendorsettings=vendor.settings).count():
                    region_checked.append(region.pk)
                    row.append(region.pk)
            if row and district.regions.count() == len(row):
                district.checked = True
            district.regions_count = len(row)

        vendor_districts_pks = vendor.settings.city_notices.values_list('region__district', flat=True).distinct()
        vendor_regions_pks = vendor.settings.city_notices.values_list('region', flat=True).distinct()
        vendor_districts = districts.filter(pk__in=vendor_districts_pks)
        for vendor_district in vendor_districts:
            vendor_district.vendor_regions = regions.filter(district__pk=vendor_district.pk, pk__in=vendor_regions_pks)
            if vendor_district.vendor_regions.count() > 0:
                for region in vendor_district.vendor_regions:
                    region.vendor_cities = vendor.settings.city_notices.filter(region__pk=region.pk)

        serializer = VendorSettingsSerializer()
        serializer.action = reverse("vendor:cities")
        context = {
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
            "districts": districts,
            "region_checked": region_checked,
            "vendor": vendor,
            "vendor_cities": vendor.settings.city_notices.all(),
            "serializer": serializer,
            "vendor_districts": vendor_districts
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied
        serializer = VendorSettingsSerializer(data=request.data, context={"request": request})
        if serializer.is_valid():
            serializer.save()
            context = {
                "data": {
                    "content": serializer.data
                }
            }
            return Response(data=context, status=status.HTTP_200_OK)
        context = {
            "data": {
                "errors": serializer.errors
            }
        }
        return Response(data=context, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, *args, **kwargs):
        test = 123
        return super(VendorCitiesViewSet, self).partial_update(request, *args, **kwargs)
