from django.forms import ModelForm
from users.models import Document, Car, Driver
from app.models import Bet
from django.forms import forms
from django.contrib import messages


class DocumentsForm(ModelForm):
    class Meta:
        model = Document
        fields = ['num', 'sign_date', 'file_scan', 'comment', 'publication']


class CarsForm(ModelForm):
    class Meta:
        model = Car
        fields = ['brand', 'gos_num', 'trailer_num', 'carrying', 'comment', 'unloading', 'axis_num', 'publication']


class DriversForm(ModelForm):
    class Meta:
        model = Driver
        fields = ['publication', 'name', 'passport', 'license', 'experience', 'comment', 'phone']


class BetForm(ModelForm):
    class Meta:
        model = Bet
        fields = ['bet']

    def clean(self):
        cleaned_data = super().clean()
        bet = cleaned_data.get('bet')

        if bet is not None:
            if self.bet_init and bet > self.bet_init:
                self.add_error('bet', 'Ставка должна быть меньше начальной ставки')

            if self.bet_step and bet % self.bet_step != 0:
                self.add_error('bet', 'Ставка должна быть кратна шагу ставки')

        return cleaned_data
