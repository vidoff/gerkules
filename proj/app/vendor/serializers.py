from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SkipField
from rest_framework.relations import PKOnlyObject

from users.models import VendorUser, Document, Car, Driver, VendorSettings
from geo.models import City as GeoCity
from app.models import Tender
from events.models import Event
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import get_default_password_validators
from collections import OrderedDict
from django.core.exceptions import ObjectDoesNotExist

from app.serializers import BetSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username",)


class VendorUserSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    password = serializers.CharField(required=True, label="Пароль для входа в систему")
    confirm_password = serializers.CharField(required=True, label="Повторите пароль")
    phone = serializers.CharField(required=True, label="Телефон пользователя")
    email = serializers.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorUserSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    class Meta:
        model = VendorUser
        fields = (
            "password",
            "confirm_password",
            "name",
            "email",
            "phone",
            "user",
            "company_name",
            "company_addr",
            "company_post_addr",
            "company_inn",
            "company_phone",
            "company_seo",
            "company_site",
        )

    @staticmethod
    def serializer_validate_password(field_name, password, user=None):
        password_validators = get_default_password_validators()
        for validator in password_validators:
            try:
                validator.validate(password, user)
            # TODO не могу перехватить ValidationError
            except Exception as e:
                raise serializers.ValidationError({field_name: [e]})
        return True

    # код из to_representation добавлена проверка на выдачу паролей
    def to_representation(self, instance):
        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            if field.field_name == "password":
                continue
            elif field.field_name == "confirm_password":
                continue

            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            check_for_none = (
                attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            )
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)
        return ret

    def update(self, instance, validated_data):
        try:
            password = validated_data.pop("password")
            confirm_password = validated_data.pop("confirm_password")
        except KeyError:
            return super(VendorUserSerializer, self).update(instance, validated_data)

        if password is not None and confirm_password is not None:
            if password != confirm_password:
                raise ValidationError({"password": ["Пароли не совпадают"]})
            user = instance.user
            if isinstance(user, User) and self.serializer_validate_password(
                    "password", password, user
            ):
                user.set_password(password)
                user.save()
            else:
                raise serializers.ValidationError(
                    {"password": ["Пользователь не найден"]}
                )

        return super(VendorUserSerializer, self).update(instance, validated_data)


class VendorDocumentSerializer(serializers.ModelSerializer):
    # file = serializers.FileField()
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorDocumentSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def create(self, validated_data):
        vendor = self.context.get("vendor")
        if isinstance(vendor, VendorUser):
            validated_data["vendor"] = vendor
        else:
            raise ValidationError("Нет такого пользователя")
        return super(VendorDocumentSerializer, self).create(validated_data)

    class Meta:
        model = Document
        fields = (
            "publication",
            "num",
            "sign_date",
            "file_scan",
            "comment",
            "manager_comment",
        )


class VendorCarSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorCarSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def create(self, validated_data):
        vendor = self.context.get("vendor")
        if isinstance(vendor, VendorUser):
            validated_data["vendor"] = vendor
        else:
            raise ValidationError("Нет такого пользователя")
        return super(VendorCarSerializer, self).create(validated_data)

    class Meta:
        model = Car
        fields = (
            "publication",
            "brand",
            "gos_num",
            "trailer_num",
            "carrying",
            "axis_num",
            "unloading",
            "comment",
            "manager_comment",
        )


class VendorDriverSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorDriverSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def create(self, validated_data):
        vendor = self.context.get("vendor")
        if isinstance(vendor, VendorUser):
            validated_data["vendor"] = vendor
        else:
            raise ValidationError("Нет такого пользователя")
        return super(VendorDriverSerializer, self).create(validated_data)

    class Meta:
        model = Driver
        fields = (
            "publication",
            "name",
            "passport",
            "license",
            "phone",
            "experience",
            "comment",
            "manager_comment",
        )


class VendorBetSerializer(BetSerializer):
    pass


class VendorTenderSerializer(serializers.ModelSerializer):
    vendor_car_id = serializers.CharField(required=True)
    vendor_driver_id = serializers.CharField(required=True)

    def update(self, instance, validated_data):
        driver_id = validated_data.pop("vendor_driver_id")
        car_id = validated_data.pop("vendor_car_id")
        return super(VendorTenderSerializer, self).update(instance, validated_data)

    def validate(self, attrs):
        attrs = super(VendorTenderSerializer, self).validate(attrs)
        vendor = self.context.get("vendor")
        car_id = attrs["vendor_car_id"]
        driver_id = attrs["vendor_driver_id"]
        try:
            car = vendor.cars.get(pk=car_id)
        except ObjectDoesNotExist:
            if int(car_id) == 0:
                car = None
            else:
                raise serializers.ValidationError(
                    {"vendor_car_id": ["Нет такой машины"]}
                )
        try:
            driver = vendor.drivers.get(pk=driver_id)
        except ObjectDoesNotExist:
            if int(driver_id) == 0:
                driver = None
            else:
                raise serializers.ValidationError(
                    {"vendor_driver_id": ["Нет такого водителя"]}
                )
        attrs["vendor_driver"] = driver
        attrs["vendor_car"] = car
        return attrs

    class Meta:
        model = Tender
        fields = ("vendor_car_id", "vendor_driver_id")


class GeoCitySerializer(serializers.ModelSerializer):
    class Meta:
        model = GeoCity
        fields = (
            "name",
            "id",
        )


class VendorSettingsSerializer(serializers.ModelSerializer):
    vendor = VendorUserSerializer(read_only=True)
    notices = GeoCitySerializer(many=True, read_only=True)

    def __init__(self, *args, **kwargs):
        return super(VendorSettingsSerializer, self).__init__(*args, **kwargs)

    def create(self, validated_data):
        cities = validated_data.pop('cities', None)
        request = self.context.get("request", None)
        vendoruser = request.user.vendoruser
        try:
            settings = VendorSettings.objects.get(vendor=vendoruser)
        except VendorSettings.DoesNotExist:
            raise serializers.ValidationError(
                {"cities": ["Нет настроек пользователя"]}
            )
        if isinstance(cities, list) and isinstance(vendoruser, VendorUser):
            geo_cities = GeoCity.objects.in_bulk(cities)
            settings.city_notices.set(geo_cities)
        elif cities is None:
            settings.city_notices.clear()

        settings.save()
        return settings

    def to_internal_value(self, data):
        cities = data.get('cities', None)
        ret_data = super(VendorSettingsSerializer, self).to_internal_value(data)
        if cities is not None and len(cities):
            ret_data['cities'] = cities.split(',')
        return ret_data

    class Meta:
        model = VendorSettings
        fields = ("vendor",
                  "notices",)
