from users.models import VendorUser, Driver, Document, Car
from app.views import TenderList, ArchiveList
from app.models import Tender, Bet, Task
from django.db.models import Q, Min
from django.core.exceptions import PermissionDenied
from app.vendor.viewsets import VendorMixin
from app.filters import ArchiveListFilter


class VendorTenderList(VendorMixin, TenderList):
    template_name = "app/vendor/tender/tender.html"

    def get_queryset(self):
        qs = Tender.objects.exclude(status="archive").order_by("-pub_date")
        object_list = qs.filter(Q(status__in=["new", "played"]) | Q(user_winner=self.request.user.id))
        return object_list

    def get_context_data(self, object_list=None, *args, **kwargs):
        try:
            vendor = VendorUser.objects.get(user=self.request.user)
        except VendorUser.DoesNotExist:
            raise PermissionDenied

        if object_list is not None:
            for tender in object_list:
                tender.user_bet = tender.bets.filter(publication=True, user=vendor.user).aggregate(Min("bet"))[
                    "bet__min"]
                tender.bet_min = tender.bets.filter(publication=True).aggregate(Min("bet"))["bet__min"]
        context = super(VendorTenderList, self).get_context_data(object_list=object_list, *args, **kwargs)
        # context.update({"vendor": vendor, "menu": self.get_menu()})
        context.update({"vendor": vendor, "menu": self.get_menu(user=self.request.user)})
        return context


class VendorArchiveList(VendorMixin, ArchiveList):
    template_name = "app/vendor/tender/archive.html"
    filterset_class = ArchiveListFilter

    def get_queryset(self):
        qs = Tender.objects.filter(status="archive").order_by("-pub_date")
        # object_list = qs.filter(Q(status__in=["archive", "played"]) | Q(user_winner=self.request.user.id))
        # object_list = qs.filter(Q(status__in=["archive", "played"]) | Q(user_winner=self.request.user.id))
        object_list = qs
        return object_list

    def get_context_data(self, object_list=None, *args, **kwargs):
        vendor = self.request.user.vendoruser
        if object_list is not None:
            for tender in object_list:
                tender.user_bet = tender.bets.filter(publication=True, user=vendor.user).aggregate(Min("bet"))[
                    "bet__min"]
                tender.bet_min = tender.bets.filter(publication=True).aggregate(Min("bet"))["bet__min"]
        context = super(VendorArchiveList, self).get_context_data(object_list=object_list, *args, **kwargs)
        context.update({"menu": self.get_menu(user=self.request.user)})
        return context
