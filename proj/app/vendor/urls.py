from django.urls import path
from . import views

# from app.viewsets import TenderViewSet
from app.vendor.viewsets import *

app_name = "vendor"

urlpatterns = [
    path("list/", views.VendorTenderList.as_view(), name="index"),
    path(
        "detail/<int:pk>/",
        VendorTenderDetailViewSet.as_view(
            {"get": "retrieve", "patch": "partial_update"}
        ),
        name="tender_detail",
    ),
    path("archive/", views.VendorArchiveList.as_view(), name="tender_archive"),
    path(
        "notices/",
        VendorNoticeListViewSet.as_view({"get": "list", "delete": "destroy"}),
        name="notices",
    ),
    path(
        "notices/<int:pk>/",
        VendorNoticeViewSet.as_view({"patch": "partial_update"}),
        name="notice_detail",
    ),
    path(
        "rooms/",
        VendorRoomListViewSet.as_view({"get": "list", "post": "create"}),
        name="rooms",
    ),
    path(
        "rooms/<int:pk>/",
        VendorRoomDetailViewSet.as_view({"get": "retrieve", "put": "create"}),
        name="room_detail",
    ),
    path(
        "archive/detail/<int:pk>/",
        VendorArchiveTenderDetailViewSet.as_view({"get": "retrieve"}),
        name="archive_tender_detail",
    ),
    path(
        "detail/<int:tender_pk>/bets/",
        VendorBetViewSet.as_view({"get": "list", "put": "create", "post": "create"}),
        name="bet_list",
    ),
    path(
        "vendors/<int:tender_pk>/bets/<int:pk>/",
        VendorBetDetailViewSet.as_view(
            {
                # "get": "retrieve",
                # "put": "update",
                # "patch": "partial_update",
                "delete": "destroy"
            }
        ),
        name="bet_detail",
    ),
    path(
        "profile/",
        VendorDetailViewSet.as_view({"get": "retrieve", "patch": "partial_update"}),
        name="profile",
    ),
    path(
        "profile/documents/",
        VendorDocumentsViewSet.as_view(
            {"get": "list", "put": "create", "post": "create"}
        ),
        name="documents",
    ),
    path(
        "profile/documents/<int:pk>/",
        VendorDocumentsDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="document_detail",
    ),
    path(
        "profile/cities/",
        VendorCitiesViewSet.as_view(
            {"get": "list", "put": "create", "patch": "partial_update"}
        ),
        name="cities",
    ),
    path(
        "profile/cars/",
        VendorCarsViewSet.as_view({"get": "list", "put": "create", "post": "create"}),
        name="cars",
    ),
    path(
        "profile/cars/<int:pk>/",
        VendorCarsDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="car_detail",
    ),
    path(
        "profile/drivers/",
        VendorDriversViewSet.as_view(
            {"get": "list", "put": "create", "post": "create"}
        ),
        name="drivers",
    ),
    path(
        "profile/drivers/<int:pk>/",
        VendorDriversDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="driver_detail",
    ),
]
