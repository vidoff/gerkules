import django_filters.rest_framework
from django.shortcuts import get_object_or_404, render, redirect, reverse
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import Http404, JsonResponse
from django.core.exceptions import PermissionDenied
from django.template.loader import render_to_string
from rest_framework import viewsets, permissions, status
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Min
from rest_framework.decorators import renderer_classes
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.exceptions import NotFound
from rest_framework.mixins import ListModelMixin
from rest_framework.parsers import (
    MultiPartParser,
    JSONParser,
    FormParser,
    FileUploadParser,
)
from .filters import TenderListFilter, ArchiveListFilter

from app.models import Tender, Task, Bet
from app.serializers import BetSerializer
from django_filters.views import FilterMixin


class BaseViewSet(viewsets.ModelViewSet):
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer)
    parser_classes = (MultiPartParser, FormParser, JSONParser)

    def dispatch(self, request, *args, **kwargs):
        format = request.GET.get("format")
        if format == "form":
            return self.get_form(request, *args, **kwargs)
        return super(BaseViewSet, self).dispatch(request, *args, **kwargs)


class BetViewSet(BaseViewSet):
    queryset = Bet.objects.all()
    serializer_class = BetSerializer
    renderer_classes = (JSONRenderer, TemplateHTMLRenderer)
    template_name = "app/interfaces/tender/bets.html"

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer()
        # context = {"serializer": serializer}
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        tender_pk = kwargs["tender_pk"]
        serializer = BetSerializer(
            data=request.data, context={"request": request, "tender_pk": tender_pk}
        )
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok"}
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BetDetailViewSet(BaseViewSet):
    serializer_class = BetSerializer
    queryset = Bet.objects.all()

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        tender_pk = kwargs["tender_pk"]
        try:
            tender = Tender.objects.get(pk=tender_pk)
        except Tender.DoesNotExist:
            raise PermissionDenied
        try:
            bet = tender.bets.get(pk=pk, user=request.user)
        except ObjectDoesNotExist:
            raise PermissionDenied

        if tender.status == "closed" or tender.status == "archive":
            raise PermissionDenied

        bet.publication = False
        bet.save()

        if tender.bets.filter(publication=True).count() == 0:
            tender.bet_win = None
            tender.status = "new"
        else:
            bet_min = tender.bets.filter(publication=True).aggregate(Min("bet"))[
                "bet__min"
            ]
            tender.bet_win = bet_min

        tender.save()
        return Response({"data": "ok"}, status=status.HTTP_200_OK)
