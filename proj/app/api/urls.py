from django.urls import path

from app.api.viewsets import (
    TenderViewSet,
    TenderDetailViewSet,
    APIViewSet,
    TenderClosedViewSet,
    BetsViewSet,
    TenderBetsViewSet,
    TenderConfirmedViewSet,
    APIPagesViewSet,
    APICategoryViewSet,
)

app_name = "api"

# tenderRouter = routers.DefaultRouter()
# tenderRouter.register('')

urlpatterns = [
    # path("", APIViewSet.as_view({"get": "retrieve"}), name="doc"),
    path("doc/", APICategoryViewSet.as_view({"get": "list"}), name="category_list"),
    path(
        "doc/<slug:category>/",
        APIPagesViewSet.as_view({"get": "list"}),
        name="page_list",
    ),
    path(
        "doc/<slug:category>/<slug:page>/",
        APIPagesViewSet.as_view({"get": "retrieve"}),
        name="page",
    ),
    path(
        "tender/",
        TenderViewSet.as_view(
            {"get": "list", "post": "create", "patch": "partial_update"}
        ),
        name="tender_list",
    ),
    path("tender/closed/", TenderClosedViewSet.as_view({"get": "list"})),
    path("tender/closed/confirmed/", TenderConfirmedViewSet.as_view({"get": "list"})),
    path("tender/bets/", TenderBetsViewSet.as_view({"get": "list"})),
    path("bets/", BetsViewSet.as_view({"get": "list"})),
    path(
        "tender/<int:code>/",
        TenderDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="tender_detail",
    ),
]
