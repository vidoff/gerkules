from rest_framework import serializers
from django.utils.timezone import datetime
from app.models import Tender, Task, Bet, Page, Category, TASK_UNLOADING, TENDER_STATUS
from users.models import Driver, VendorUser, Car
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


# import pytz


class TaskSerializer(serializers.ModelSerializer):
    vehicle = serializers.CharField()
    date_start = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    date_end = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    task_id = serializers.IntegerField(required=False)

    class Meta:
        model = Task
        fields = (
            "task_id",
            "name",
            "point_from",
            "point_to",
            "date_start",
            "date_end",
            "weight",
            "vehicle",
        )
        extra_kwargs = {
            "vehicle": {"validators": ["validate_vehicle"]}
        }

    def create(self, validated_data):
        tender_data = validated_data.pop("tender")
        tender, created = Tender.objects.get_or_create(**tender_data)
        return Task.objects.create(tender=tender, **validated_data)

    def update(self, instance, validated_data):
        validated_data["id"] = validated_data.pop("task_id", None)
        return super(TaskSerializer, self).update(instance, validated_data)

    def validate(self, attrs):
        test = 123
        return super(TaskSerializer, self).validate(attrs)

    def to_internal_value(self, data):
        task_id = data.get("task_id", None)
        if task_id:
            data["id"] = task_id
        return super(TaskSerializer, self).to_internal_value(data)

    def validate_vehicle(self, vehicle):
        for k, v in TASK_UNLOADING:
            if v == vehicle.lower():
                return k
        raise serializers.ValidationError("не поддерживаемый тип выгрузки ТС")

    def to_representation(self, instance):
        data = super(TaskSerializer, self).to_representation(instance)
        for k, v in TASK_UNLOADING:
            if k == data["vehicle"].lower():
                data["vehicle"] = v
                break
        data["task_id"] = instance.pk
        return data


class TenderListSerializer(serializers.ListSerializer):
    def update(self, instance, validated_data):
        tender_mapping = {tender.code: tender for tender in instance}
        data_mapping = {item['code']: item for item in validated_data}

        ret = []
        for tender_code, data in data_mapping.items():
            tender = tender_mapping.get(tender_code, None)
            if tender is None:
                ret.append(self.child.create(data))
            else:
                ret.append(self.child.update(tender, data))

        # # Perform deletions.
        # for code, tender in tender_mapping.items():
        #     if code not in data_mapping:
        #         tender.delete()
        return ret

        #
        # def create(self, validated_data):
        #     tenders = [Tender(**item) for item in validated_data]
        #     return Tender.objects.bulk_create(tenders)

        # @classmethod
        # def many_init(cls, *args, **kwargs):
        #     kwargs["tasks"] = cls()
        #     return


class TenderSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=False, required=False)
    name = serializers.CharField(allow_null=True)
    pub_date = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S", read_only=True, default=datetime.now()
    )
    date_start = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    date_end = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    bet_win = serializers.IntegerField(read_only=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(TenderSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)
        self.qs = Tender.objects.all()

    @staticmethod
    def set_close_tender(instance):
        pass

    def full_reset(self, instance):
        if instance.bets.count():
            # instance.bets.delete()
            bets = Bet.objects.filter(tender=instance).delete()
            instance.bet_win = None
            instance.user_winner = None
            instance.confirmed = False
            instance.vendor_car = None
            instance.vendor_driver = None
        instance.save()

    def update(self, instance, validated_data):
        # code = validated_data.pop("code", None)
        tasks = validated_data.pop("tasks", None)
        status = validated_data.pop("status", None)
        date_start = validated_data.pop("date_start", instance.date_start)
        date_end = validated_data.pop("date_end", instance.date_end)

        if isinstance(instance, Tender):

            ###################NEW!
            if instance.status == "new":
                pass

            ###################PLAYED
            elif instance.status == "played":
                if status == "new":
                    # instance.bets.delete()
                    bets = Bet.objects.filter(tender=instance).delete()
                    instance.bet_win = None
                elif status == "played":
                    pass
                elif status == "closed":
                    pass                    
                # TODO выбрать победителя
                ###выбрать победителя!!!!!!
                elif status == "archive":
                    # instance.bets.delete()
                    bets = Bet.objects.filter(tender=instance).delete()
                    instance.bet_win = None
                    if date_end is not None:
                        instance.date_end = date_end
                    else:
                        instance.date_end = datetime.now()

            ###################CLOSED
            elif instance.status == "closed":
                if status == "new":
                    instance.status = status
                    # instance.bets.delete()
                    bets = Bet.objects.filter(tender=instance).delete()
                    instance.user_winner = None
                    instance.vendor_car = None
                    instance.vendor_driver = None
                    instance.bet_win = None
                    instance.confirmed = False
                elif status == "played":
                    instance.status = status
                    instance.vendor_car = None
                    instance.vendor_driver = None
                    instance.user_winner = None
                    instance.confirmed = False
                elif status == "closed":
                    instance.status = status
                    # instance.date_end = datetime.now()
                elif status == "archive":
                    instance.status = status

            ###################ARCHIVE
            elif instance.status == "archive":
                if status == "new":
                    instance.status = status
                    bets = Bet.objects.filter(tender=instance).delete()
                    instance.user_winner = None
                    instance.vendor_car = None
                    instance.vendor_driver = None
                    instance.bet_win = None
                    instance.confirmed = False
                elif status == "played":
                    instance.status = status
                    instance.user_winner = None
                    instance.vendor_car = None
                    instance.vendor_driver = None
                    instance.confirmed = False
                elif status == "closed":
                    instance.status = status
                elif status == "archive":
                    pass

        # ------------continue
        updated = False
        if tasks:
            for task in tasks:
                task_id = task.pop("task_id", None)
                if task_id:
                    updated = True
                    try:
                        task_object = Task.objects.get(tender=instance, pk=task_id)
                    except Task.DoesNotExist:
                        raise serializers.ValidationError(
                            {"tasks": ["невозможно обновить, нет task по заданному task_id:{}".format(task_id)]})
                    self.perform_update(task_object, task)
                    # task_object.update(**task)
            if not updated:
                Task.objects.filter(tender=instance).delete()
                for task in tasks:
                    Task.objects.create(tender=instance, **task)
        if status is not None:
            instance.status = status
        instance.date_start = date_start
        instance.date_end = date_end
        instance.save()
        return super(TenderSerializer, self).update(instance, validated_data)
        # return self.perform_update(instance, validated_data)

    def create(self, validated_data, *args, **kwargs):
        code = validated_data.get("code")
        try:
            tender = Tender.objects.get(code=code)
        except Tender.DoesNotExist:
            tasks_data = validated_data.pop("tasks", None)
            try:
                tender = Tender.objects.create(**validated_data)
            except Exception as e:
                error_message = "An exception of type {0} occurred. Message:{1}"
                raise serializers.ValidationError(
                    {"tender": [error_message.format(type(e).__name__, e.args[0])]}
                )

            if tasks_data:
                for task_data in tasks_data:
                    try:
                        Task.objects.create(tender=tender, **task_data)
                    except Exception as e:
                        tender.delete()
                        error_message = "An exception of type {0} occurred. Message:{1}"
                        raise serializers.ValidationError(
                            {"tasks": [error_message.format(type(e).__name__, e.args[0])]}
                        )
            return tender
        else:
            raise serializers.ValidationError(
                {"error": ["Тендер code:{} уже существует, для обновления используйте PATCH".format(code)]})

    def perform_update(self, instance, data):
        for attr, value in data.items():
            try:
                setattr(instance, attr, value)
            except Exception as e:
                error_message = "An exception of type {0} occurred. Message:{1}"
                raise serializers.ValidationError(
                    {"tasks": [error_message.format(type(e).__name__, e.args[0])]}
                )
        instance.save()
        return instance

    def to_data(self, instance, data, *args, **kwargs):
        data["name"] = instance.name
        data["date_start"] = instance.date_start
        data["date_end"] = instance.date_end

        return data

    def to_representation(self, instance):
        data = super(TenderSerializer, self).to_representation(instance)
        for k, v in TENDER_STATUS:
            if k == data["status"].lower():
                data["status"] = v
                break
        return data

    def to_internal_value(self, data):
        code = data.get("code", None)
        status = data.pop("status", None)
        date_start = data.get("date_start", None)
        date_end = data.get("date_end", None)
        name = data.get("name", None)
        if status:
            for k, v in TENDER_STATUS:
                if v == status.lower():
                    data["status"] = k
                    break
        if code:
            try:
                tender = self.qs.get(code=code)
            except Tender.DoesNotExist:
                return super(TenderSerializer, self).to_internal_value(data)
                # raise serializers.ValidationError({"tender": ["Тендер не существует code:{}".format(code)]})
            self.instance = tender
            # data = self.to_data(tender, data)
            if name is None:
                data["name"] = self.instance.name
            if date_start is None:
                # data["date_start"] = self.instance.date_start.strftime("%Y-%m-%dT%H:%M:%S")
                data["date_start"] = self.instance.date_start
            if date_end is None:
                data["date_end"] = self.instance.date_end
                # data["date_end"] = self.instance.date_end.strftime("%Y-%m-%dT%H:%M:%S")

        return super(TenderSerializer, self).to_internal_value(data)

    # @staticmethod
    # def compare_dates(date_start, date_end):
    #     if date_start <= date_end:
    #         return False
    #     return True

    def validate(self, attrs):
        # utc = pytz.utc
        status = attrs.get("status", None)
        date_start = attrs.get("date_start", None)
        date_end = attrs.get("date_end", None)
        name = attrs.get("name", None)
        if date_start is not None:
            date_start = date_start.replace(tzinfo=None)
        if date_end is not None:
            date_end = date_end.replace(tzinfo=None)

        if date_start is not None and date_end is not None and date_start > date_end:
            raise serializers.ValidationError(
                {"date_start": ["Дата начала торгов не может быть старше даты окончания"]})

        # CREATE VALIDATION
        if status == "new":
            if attrs.get("confirmed"):
                raise serializers.ValidationError({"confirmed": [
                    "Невозможно установить confirmed {} для нового тендера".format(attrs.get("confirmed"))]})

        # UPDATE VALIDATION
        # .................
        if isinstance(self.instance, Tender):
            if name is None:
                attrs["name"] = self.instance.name
            # check status
            if self.instance.status == "new":
                if status == "new":
                    if date_end is not None and date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"date_start": ["Дата окончания торгов не может быть меньше текущей даты"]})
                elif status == "played":
                    raise serializers.ValidationError(
                        {"status": ["Невозможно изменить статус с нового на 'играет' без ставок"]})
                elif status == "closed":
                    raise serializers.ValidationError(
                        {"status": ["Невозможно изменить статус с нового на 'закрыт' без ставок, (возможно архив?)"]})
                elif status == "archive":
                    pass
                elif status is None:
                    if date_end is not None and date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"date_start": ["Дата окончания торгов не может быть меньше текущей даты без статуса"]})
            elif self.instance.status == "played":
                if status == "new":
                    if date_end is not None and date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов не может быть меньше текущей даты"]})
                elif status == "played":
                    if date_end is not None and date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов не может быть меньше текущей даты"]})
                elif status == "closed":
                    if date_end is not None and date_end >= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов больше текущей даты для смены c играет на закрыт"]})
                elif status == "archive":
                    if date_end is not None and date_end >= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов больше текущей даты для смены c играет на архив"]})
                elif status is None:
                    if date_end is not None and date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов не может быть меньше текущей даты без статуса"]})
            elif self.instance.status == "closed":
                if status == "new":
                    if not date_end:
                        raise serializers.ValidationError(
                            {"status": ["Нет даты окончания для смены статуса на новый с закрытого"]})
                    elif date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"status": ["Дата окончания меньше текущей для смены статуса с закрытого на новый"]})
                elif status == "played":
                    if not date_end:
                        raise serializers.ValidationError(
                            {"status": ["Нет даты окончания для смены статуса на играет с закрытого"]})
                    elif date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"status": ["Дата окончания меньше текущей для смены статуса с закрытого на играет"]})
                elif status == "closed":
                    pass
                elif status == "archive":
                    pass

            elif self.instance.status == "archive":
                if status == "new":
                    if not date_end:
                        raise serializers.ValidationError(
                            {"status": ["Нет даты окончания для смены статуса на новый с архива"]})
                    elif date_end <= datetime.now():
                        raise serializers.ValidationError(
                            {"status": ["Дата окончания меньше текущей для смены статуса с архива на новый"]})
                elif status == "played":
                    if self.instance.bets.count():
                        if not date_end:
                            raise serializers.ValidationError(
                                {"status": ["Нет даты окончания для смены статуса на играет с архива"]})
                        elif date_end <= datetime.now():
                            raise serializers.ValidationError(
                                {"status": ["Дата окончания меньше текущей для смены статуса с архива на играет"]})
                    else:
                        raise serializers.ValidationError(
                            {"status": ["Невозможно изменить статус на играет из архива без ставок"]})
                elif status == "closed":
                    if not self.instance.bets.count():
                        raise serializers.ValidationError(
                            {"status": ["Невозможно изменить статус на закрыт из архива без ставок"]})
                elif status == "archive":
                    if date_end is not None and date_end >= datetime.now():
                        raise serializers.ValidationError(
                            {"date_end": ["Дата окончания торгов больше текущей даты для смены в архиве"]})
                elif status is None:
                    if date_end is not None and date_end >= datetime.now():
                        raise serializers.ValidationError(
                            {"status": ["Дата окончания больше текущей для смены статуса с архива и нет статуса"]})

        return super(TenderSerializer, self).validate(attrs)

    class Meta:
        model = Tender
        list_serializer_class = TenderListSerializer
        fields = (
            "code",
            "name",
            "status",
            "pub_date",
            "date_start",
            "date_end",
            "bet_step",
            "bet_win",
            "bet_init",
            "comment",
            "confirmed",
            "tasks"
        )


class VendorDriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = (
            "name",
            "passport",
            "license",
            "phone",
            "experience",
            "comment",
            "manager_comment",
        )


class VendorCarSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super(VendorCarSerializer, self).to_representation(instance)
        for k, v in TASK_UNLOADING:
            if k == data["unloading"]:
                data["unloading"] = v
                break
        return data

    class Meta:
        model = Car
        fields = (
            "brand",
            "gos_num",
            "trailer_num",
            "carrying",
            "axis_num",
            "unloading",
            "comment",
            "manager_comment",
        )


class BetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bet
        fields = (
            "bet",
            "pub_date",
        )


class VendorUserSerializer(serializers.ModelSerializer):
    vendor_id = serializers.IntegerField()
    driver = VendorDriverSerializer(read_only=True)
    car = VendorCarSerializer(read_only=True)
    bet = serializers.IntegerField()
    bet_pub_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    def to_representation(self, instance):
        instance.vendor_id = instance.id
        return super(VendorUserSerializer, self).to_representation(instance)

    class Meta:
        model = VendorUser
        fields = (
            "vendor_id",
            "name",
            "email",
            "phone",
            "company_name",
            "company_addr",
            "company_post_addr",
            "company_inn",
            "company_phone",
            "company_seo",
            "company_site",
            "car",
            "driver",
            "bet",
            "bet_pub_date"
        )


class TenderClosedSerializer(TenderSerializer):
    winner = VendorUserSerializer(read_only=True)

    def to_representation(self, instance):
        if instance.user_winner is not None:
            instance.winner = instance.user_winner.vendoruser
            instance.winner.driver = instance.vendor_driver
            instance.winner.car = instance.vendor_car
        if instance.bet_win is not None and instance.bet_win is not None:
            try:
                bet = Bet.objects.get(bet=instance.bet_win, tender=instance, user=instance.user_winner,
                                      publication=True)
            except Bet.DoesNotExist:
                instance.winner.bet = None
                instance.winner.bet_pub_date = None
                # raise serializers.ValidationError({"error": ["BET does not exist"]})
            else:
                instance.winner.bet = bet.bet
                instance.winner.bet_pub_date = bet.pub_date
        return super(TenderClosedSerializer, self).to_representation(instance)

    class Meta:
        model = Tender
        fields = (
            "code",
            "name",
            "status",
            "pub_date",
            "date_start",
            "date_end",
            "bet_step",
            "bet_win",
            "bet_init",
            "confirmed",
            "comment",
            "winner",
            "tasks",
        )


class VendorUserBetSerializer(serializers.ModelSerializer):
    vendor_id = serializers.IntegerField()
    username = serializers.CharField(allow_null=True)

    def to_representation(self, instance):
        instance.vendor_id = instance.id
        if isinstance(instance.user, User):
            try:
                instance.username = instance.user.username
            except ObjectDoesNotExist:
                instance.username = ""
        return super(VendorUserBetSerializer, self).to_representation(instance)

    class Meta:
        model = VendorUser
        fields = (
            "vendor_id",
            "username",
            "name",
            "email",
            "phone",
        )


class BetTenderSerializer(serializers.ModelSerializer):
    vendor = VendorUserBetSerializer(read_only=True, allow_null=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(BetTenderSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def to_representation(self, instance):
        if isinstance(instance.user, User):
            try:
                instance.vendor = VendorUser.objects.get(user=instance.user)
            except VendorUser.DoesNotExist:
                instance.vendor = None
        return super(BetTenderSerializer, self).to_representation(instance)

    class Meta:
        model = Bet
        fields = ("vendor", "bet", "pub_date")


class TenderBetSerializer(serializers.ModelSerializer):
    bets = BetTenderSerializer(many=True, read_only=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(TenderBetSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def to_representation(self, instance):
        data = super(TenderBetSerializer, self).to_representation(instance)
        for k, v in TENDER_STATUS:
            if k == data["status"].lower():
                data["status"] = v
                break
        return data

    class Meta:
        model = Tender
        fields = (
            "code",
            "name",
            "status",
            "pub_date",
            "date_start",
            "date_end",
            "bet_step",
            "bet_win",
            "bet_init",
            "confirmed",
            "bets",
        )


class BetSerializer(serializers.ModelSerializer):
    vendor = VendorUserBetSerializer(read_only=True, allow_null=True)
    tender = TenderBetSerializer(read_only=True, allow_null=True, exclude_fields=["bets"])

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(BetSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def to_representation(self, instance):
        if isinstance(instance.user, User):
            try:
                instance.vendor = VendorUser.objects.get(user=instance.user)
            except VendorUser.DoesNotExist:
                instance.vendor = None
        return super(BetSerializer, self).to_representation(instance)

    class Meta:
        model = Bet
        fields = ("vendor", "tender", "bet", "pub_date")


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = "__all__"
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {
                'lookup_field': 'slug'
            }
        }


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {
                'lookup_field': 'slug'
            }
        }
