from django.http import Http404
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import viewsets, status, permissions, authentication, exceptions
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser

from rest_framework_xml.parsers import XMLParser
from rest_framework_xml.renderers import XMLRenderer

from app.models import Tender, Bet, Page, Category

from .serializers import (
    TenderSerializer,
    TenderClosedSerializer,
    BetSerializer,
    TenderBetSerializer,
    PageSerializer,
    CategorySerializer,
)
from rest_framework.response import Response
from datetime import datetime, timedelta


class ApiPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if request.user.username == "admin" or request.user.username == "api":
            return request.user and request.user.is_authenticated
        return False


class BaseViewSet(viewsets.ModelViewSet):
    pass
#    permission_classes = [ApiPermission, ]
#    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)


class APIViewSet(BaseViewSet):
    template_name = "app/interfaces/api/documentation/index.html"
    renderer_classes = (TemplateHTMLRenderer,)

    def retrieve(self, request, **kwargs):
        return Response(status=status.HTTP_200_OK)


class APICategoryViewSet(BaseViewSet):
    template_name = "app/interfaces/pages/list.html"
    renderer_classes = (TemplateHTMLRenderer,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = "slug"

    def get_object(self):
        category = self.kwargs.get("category")
        page = self.kwargs.get("page")
        qs = self.queryset.filter(slug=category, is_active=True)
        return qs.first()

    def retrieve(self, request, **kwargs):
        object = self.get_object()
        context = {"object": object}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_queryset(self, request, *args, **kwargs):
        object_list = Category.objects.filter(parent__name="api")
        return object_list

    def list(self, request, *args, **kwargs):
        context = {"category_list": self.get_queryset(request)}
        return Response(data=context, status=status.HTTP_200_OK)


class APIPagesViewSet(BaseViewSet):
    template_name = "app/interfaces/pages/list.html"
    renderer_classes = (TemplateHTMLRenderer,)
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    lookup_field = "page"

    def get_categories(self):
        return Category.objects.filter(parent__name="api")

    def get_object(self, *args, **kwargs):
        page_slug = self.kwargs.get("page")
        try:
            page = Page.objects.get(slug=page_slug)
        except Page.DoesNotExist:
            raise Http404
        return page

    def get_queryset(self, *args, **kwargs):
        category_slug = self.kwargs.get('category', None)
        try:
            category = Category.objects.get(slug=category_slug)
        except Category.DoesNotExist:
            raise Http404
        qs = self.queryset.filter(category=category, is_active=True)
        return qs

    def retrieve(self, request, **kwargs):
        object = self.get_object()
        context = {
            "page": object,
            "category_list": self.get_categories(),
            "parent_category": Category.objects.get(name="api"),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        context = {
            "pages_list": self.get_queryset(),
            "category_list": self.get_categories(),
        }
        return Response(data=context, status=status.HTTP_200_OK)


def perform_create(self, serializer):
    serializer.save(owner=self.request.user)


class TenderViewSet(BaseViewSet):
    serializer_class = TenderSerializer
    queryset = Tender.objects.all()

    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (JSONRenderer, XMLRenderer)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ("code", "confirmed", "status")

    def get_queryset(self):
        return self.queryset.order_by("-pub_date")

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())[:500]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, many=isinstance(request.data, list)
        )

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, *args, **kwargs):
        data = request.data
        # instances = Tender.objects.filter(code__)
        codes = list()
        for key in data:
            codes.append(key["code"])
        if len(codes) > 0:
            instances = Tender.objects.filter(code__in=codes)
        if instances.count():
            serializer = TenderSerializer(instances, data=request.data, many=True)
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {"error": "нет значений по code"}, status=status.HTTP_400_BAD_REQUEST
        )


class TenderClosedViewSet(BaseViewSet):
    serializer_class = TenderClosedSerializer
    queryset = Tender.objects.all()
    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (XMLRenderer, JSONRenderer,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ("code", "confirmed",)

    def get_queryset(self):
        return self.queryset.filter(status='closed').order_by("-pub_date")

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())[:500]
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TenderConfirmedViewSet(BaseViewSet):
    serializer_class = TenderClosedSerializer
    queryset = Tender.objects.all()
    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (JSONRenderer, XMLRenderer)

    def get_queryset(self):
        return self.queryset.filter(status="closed", confirmed=True).order_by(
            "-pub_date"
        )

    def list(self, request, *args, **kwargs):
        object_list = self.get_queryset()
        serializer = TenderClosedSerializer(object_list, many=True)
        return Response(serializer.data)


class BetsViewSet(BaseViewSet):
    serializer_class = BetSerializer
    queryset = Bet.objects.filter(pub_date__gte=datetime.now() - timedelta(days=7))
    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (JSONRenderer, XMLRenderer)

    def get_queryset(self):
        return self.queryset.filter(publication=True).order_by("pub_date")

    def list(self, request, *args, **kwargs):
        object_list = self.get_queryset()
        serializer = BetSerializer(object_list, many=True)
        return Response(serializer.data)


class TenderBetsViewSet(BaseViewSet):
    serializer_class = TenderBetSerializer
    queryset = Tender.objects.filter(pub_date__gte=datetime.now() - timedelta(days=7))
    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (JSONRenderer, XMLRenderer)

    def get_queryset(self):
        return self.queryset.all().order_by("pub_date")

    def list(self, request, *args, **kwargs):
        object_list = self.get_queryset()
        serializer = TenderBetSerializer(object_list, many=True)
        return Response(serializer.data)


class TenderDetailViewSet(BaseViewSet):
    serializer_class = TenderSerializer
    queryset = Tender.objects.all()
    parser_classes = (MultiPartParser, FormParser, XMLParser, JSONParser)
    renderer_classes = (JSONRenderer, XMLRenderer)

    def retrieve(self, request, *args, **kwargs):
        code = kwargs["code"]
        try:
            object = Tender.objects.get(code=code)
        except Tender.DoesNotExist:
            raise Http404

        serializer = TenderSerializer(object)
        return Response(serializer.data, status=status.HTTP_200_OK)
