import django_filters
from . import models
from datetime import timedelta
from django.contrib.auth.models import User
from django.conf import settings
from decimal import Decimal

# from datetime import timedelta

TENDER_STATUS = (
    ("other", "любой"),
    ("new", "новый"),
    ("played", "играет"),
    ('closed', 'есть победа'),
)
ARCHIVE_TENDER_STATUS = (
    ("other", "любой"),
    ("win", "есть победа"),
    ("lose", "нет победы"),
    ("user_win", "ваша победа"),
    ("user_loose", "принимал участие"),
)
STAFF_ARCHIVE_TENDER_STATUS = (
    ("other", "любой"),
    ("win", "есть победа"),
    ("lose", "нет победы"),
)

TASK_UNLOADING = (("other", "любая"), ("back", "задняя"), ("top", "верхняя"), ("side", "боковая"))


class TenderListFilter(django_filters.FilterSet):
    code = django_filters.NumberFilter(lookup_expr="exact")
    date_start = django_filters.DateTimeFilter(method="date_contains")
    vehicle = django_filters.ChoiceFilter(
        method="filter_vehicle", choices=TASK_UNLOADING, empty_label=None
    )
    status = django_filters.ChoiceFilter(
        method="filter_status", choices=TENDER_STATUS, empty_label=None
    )

    class Meta:
        model = models.Tender
        fields = ["code", "date_start", "status", "vehicle"]

    def date_contains(self, queryset, name, value):
        value = value.replace(minute=00, hour=00, second=00)
        date_to = value + timedelta(1)
        date_from = value
        return queryset.filter(
            **{"date_start__lte": date_to, "date_start__gte": date_from}
        )

    def filter_vehicle(self, queryset, name, value):
        if value == "other":
            return queryset.filter(**{})
        return queryset.filter(**{"tasks__vehicle": value})

    def filter_status(self, queryset, name, value):
        self.queryset = models.Tender.objects.filter(status=value).distinct()
        if value == "other":
            return queryset.filter(**{})
        return queryset.filter(**{"status": value})


class ArchiveListFilter(django_filters.FilterSet):
    code = django_filters.NumberFilter(lookup_expr="exact")
    date_start = django_filters.DateTimeFilter(method="date_contains")
    vehicle = django_filters.ChoiceFilter(
        method="filter_vehicle", choices=TASK_UNLOADING, empty_label=None
    )
    status = django_filters.ChoiceFilter(
        method="filter_status", choices=ARCHIVE_TENDER_STATUS, empty_label=None
    )
    limit = django_filters.NumberFilter(method="limit_qs", field_name="Лимит")

    def __init__(self, data, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        if not data.get('limit'):
            data = data.copy()
            if settings.DEFAULT_ARCHIVE_LENGTH is not None:
                data['limit'] = settings.DEFAULT_ARCHIVE_LENGTH
            else:
                data['limit'] = '500'
        return super(ArchiveListFilter, self).__init__(data, *args, **kwargs)

    class Meta:
        model = models.Tender
        fields = ["code", "date_start", "status", "vehicle", "limit"]

    # def filter_queryset(self, queryset):
    #     if settings.DEFAULT_ARCHIVE_LENGTH:
    #         data = self.form.data.copy()
    #         data['limit'] = settings.DEFAULT_ARCHIVE_LENGTH
    #         self.form.data = data
    #     return super(ArchiveListFilter, self).filter_queryset(queryset)

    def date_contains(self, queryset, name, value):
        value = value.replace(minute=00, hour=00, second=00)
        date_to = value + timedelta(1)
        date_from = value
        return queryset.filter(
            **{"date_start__lte": date_to, "date_start__gte": date_from}
        )

    def limit_qs(self, queryset, name, value):
        # if int(Decimal(value)) > 500:
        #     data = self.form.data.copy()
        #     data["limit"] = settings.DEFAULT_ARCHIVE_LENGTH
        #     self.form.data = data
        #     return queryset.filter(**{})[:settings.DEFAULT_ARCHIVE_LENGTH]
        return queryset.filter(**{})[:value]

    def filter_vehicle(self, queryset, name, value):
        if value == "other":
            return queryset.filter(**{})
        return queryset.filter(**{"tasks__vehicle": value})

    def filter_status(self, queryset, name, value):
        # self.queryset = models.Tender.objects.filter(status=value).distinct()
        if value == "win":
            return queryset.filter(**{"user_winner__isnull": False})
        elif value == "lose":
            return queryset.filter(**{"user_winner__isnull": True})
        elif value == "user_win" and isinstance(self.user, User):
            return queryset.filter(**{"user_winner": self.user})
        elif value == "user_loose" and isinstance(self.user, User):
            return queryset.filter(**{"bets__user": self.user}).exclude(**{"user_winner": self.user})

        return queryset.filter(**{})


class StaffArchiveListFilter(ArchiveListFilter):
    status = django_filters.ChoiceFilter(
        method="filter_status", choices=STAFF_ARCHIVE_TENDER_STATUS, empty_label=None
    )

    # def filter_queryset(self, queryset):
    #
    #     return super(StaffArchiveListFilter, self).filter_queryset(qs)
