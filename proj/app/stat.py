from app.models import Tender, Bet


def stat():
    tenders = Tender.objects.filter(
        confirmed=True, bet_init__isnull=False, bet_win__isnull=False
    )
    print("tenders count {}".format(tenders.count()))
    sum = 0
    init_sum = 0
    for t in tenders:
        init_sum = init_sum + t.bet_init
        sum = sum + t.bet_win

    print("init sum: {}".format(init_sum))
    print("sum: {}".format(sum))
    print("total: {}".format(init_sum-sum))
