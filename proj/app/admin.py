from django.contrib import admin
from .models import *

# Register your models here.
class TenderAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "id")
    search_fields = ("code", "id")


class BetAdmin(admin.ModelAdmin):
    list_display = ("tender", "bet", "user")
    search_fields = ("tender__code",)


admin.site.register(Tender, TenderAdmin)
admin.site.register(Task)
admin.site.register(Bet, BetAdmin)
admin.site.register(Category)
admin.site.register(Page)
admin.site.register(SiteSettings)
