from django.shortcuts import get_object_or_404, redirect, reverse
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import Http404, JsonResponse
from django.db.models import Min, Q
from django.core.exceptions import PermissionDenied
from django.template.loader import render_to_string
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.renderers import (
    TemplateHTMLRenderer,
    JSONRenderer,
)
from rest_framework.exceptions import NotFound
from rest_framework.parsers import (
    MultiPartParser,
    JSONParser,
    FormParser,
)
from app.models import Tender, Task, Bet
from app.staff.serializers import (
    TenderSerializer,
    TaskSerializer,
    VendorUserSerializer,
    VendorDocumentSerializer,
    VendorCarSerializer,
    VendorDriverSerializer,
    ManagerUserSerializer,
    StaffTenderSerializer,
)
from users.models import ManagerUser, VendorUser, Document, Car, Driver
from app.mixins import StaffPermissionMixin
from events.viewsets import NoticeViewSet, NoticeListViewSet
from chat.viewsets import RoomListViewSet, RoomDetailViewSet
import logging


class StaffMixin(StaffPermissionMixin):
    def get_menu(self):
        user = self.request.user
        if user.is_superuser:
            items = [
                {"name": "Тендеры", "url": "staff:index"},
                {"name": "Архив", "url": "staff:tender_archive"},
                {"name": "Уведомления", "url": "staff:notices", "title": "notices"},
                {"name": "Сообщения", "url": "staff:rooms", "title": "rooms"},
                {"name": "Поставщики", "url": "staff:vendor_list"},
                {"name": "Менеджеры", "url": "staff:manager_list"},
                {"name": "Мой профиль", "url": "staff:profile"},
            ]
        else:
            items = [
                {"name": "Тендеры", "url": "staff:index"},
                {"name": "Архив", "url": "staff:tender_archive"},
                {"name": "Поставщики", "url": "staff:vendor_list"},
                {"name": "Уведомления", "url": "staff:notices", "title": "notices"},
                {"name": "Сообщения", "url": "staff:rooms", "title": "rooms"},
                {"name": "Мой профиль", "url": "staff:profile"}
            ]
        menu = {
            'items': items
        }
        if user is not None and isinstance(user, User):
            notices = user.notices.filter(is_active=True)
            rooms = user.rooms.all()
            messages = rooms.filter(Q(messages__is_active=True) & ~Q(messages__sender=user))
            menu.update({
                'notices_count': notices.count(),
                'messages_count': messages.count()
            })
        return menu

    @staticmethod
    def get_profile_menu():
        profile_menu = [
            {"name": "Общая информация", "url": "staff:vendor_detail"},
            {"name": "Машины", "url": "staff:vendor_cars"},
            {"name": "Документы", "url": "staff:vendor_documents"},
            {"name": "Водители", "url": "staff:vendor_drivers"},
        ]
        return profile_menu


class StaffBaseViewSet(StaffMixin, viewsets.ModelViewSet):
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer)
    parser_classes = (MultiPartParser, FormParser, JSONParser,)

    def get_form(self, request, *args, **kwargs):
        raise PermissionDenied

    def dispatch(self, request, *args, **kwargs):
        format = request.GET.get("format")
        if format == "form":
            return self.get_form(request, *args, **kwargs)
        return super(StaffBaseViewSet, self).dispatch(request, *args, **kwargs)


class ProfileViewSet(StaffBaseViewSet):
    template_name = "app/staff/staff_profile.html"

    def retrieve(self, request, *args, **kwargs):
        context = {"menu": self.get_menu()}
        if request.user.is_superuser:
            self.template_name = "app/staff/admin_profile.html"
            return Response(context)
        try:
            manager = ManagerUser.objects.get(user=request.user)
        except ManagerUser.DoesNotExist:
            raise PermissionDenied
        context.update({"manager": manager})

        return Response(context)


class StaffTenderDetailViewSet(StaffBaseViewSet):
    template_name = "app/staff/tender/detail.html"
    serializer_class = StaffTenderSerializer
    parser_classes = (JSONParser, MultiPartParser, FormParser,)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            tender = Tender.objects.get(pk=pk)
        except Tender.DoesNotExist:
            raise NotFound
        vendor = tender.user_winner.vendoruser if tender.user_winner else None
        bets = tender.bets.all().order_by("-pub_date")
        # logger = logging.getLogger('debug')
        # logger = logging.getLogger('gerkules.request')
        # logger.debug('somthing wrong 1')
        if bets.count():
            tender.bet_min = bets.filter(publication=True).aggregate(Min("bet"))["bet__min"]

        context = {
            "menu": self.get_menu(),
            "vendor": vendor,
            "tender": tender,
            "tasks": tender.tasks.all,
            "bets": bets
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        try:
            tender = Tender.objects.get(pk=pk)
        except Tender.DoesNotExist:
            raise NotFound
        serializer = StaffTenderSerializer(tender, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VendorViewSet(StaffBaseViewSet):
    queryset = VendorUser.objects.all()
    serializer_class = VendorUserSerializer
    template_name = "app/staff/vendor/vendor_list.html"

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer()
        vendor_list = VendorUser.objects.all()
        context = {
            "vendor_list": vendor_list,
            "serializer": serializer,
            "menu": self.get_menu(),
        }
        return Response(context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = VendorUserSerializer(
            data=request.data, context={"request": request}
        )
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok"}
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# TODO доделать меню профиля
class VendorDetailViewSet(StaffBaseViewSet):
    queryset = VendorUser.objects.all()
    serializer_class = VendorUserSerializer
    # parser_classes = (JSONParser, MultiPartParser, FormParser)
    template_name = "app/staff/vendor/profile.html"

    def retrieve(self, request, *args, **kwargs):
        object = self.get_object()
        context = {
            "vendor": object,
            "menu": self.get_menu(),
            "profile_menu": self.get_profile_menu(),
        }
        return Response(context)

    def partial_update(self, request, *args, **kwargs):
        object = self.get_object()
        serializer = self.get_serializer(object, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok"}
            return Response(context, status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        type = request.GET.get("type")
        object = self.get_object()
        if type == "person":
            serializer = VendorUserSerializer(
                object,
                exclude_fields=[
                    "is_active",
                    "username",
                    "password",
                    "confirm_password",
                    "company_name",
                    "company_addr",
                    "company_post_addr",
                    "company_inn",
                    "company_seo",
                    "company_site",
                    "company_phone",
                ],
            )
        elif type == "company":
            serializer = VendorUserSerializer(
                object,
                exclude_fields=[
                    "is_active",
                    "username",
                    "password",
                    "confirm_password",
                    "name",
                    "email",
                    "phone",
                ],
            )
        elif type == "password":
            serializer = VendorUserSerializer(
                object,
                exclude_fields=[
                    "is_active",
                    "username",
                    "name",
                    "email",
                    "phone",
                    "company_name",
                    "company_addr",
                    "company_post_addr",
                    "company_inn",
                    "company_seo",
                    "company_site",
                    "company_phone",
                ],
            )
        else:
            raise PermissionDenied
        serializer.type = "patch"
        serializer.action = reverse("staff:vendor_detail", kwargs={"pk": pk})
        context = {
            "form": serializer,
            # "form_delete_button": True,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorDocumentsViewSet(StaffBaseViewSet):
    serializer_class = VendorDocumentSerializer
    queryset = Document.objects.all()
    template_name = "app/staff/vendor/documents.html"

    def list(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        try:
            vendor = VendorUser.objects.get(pk=vendor_pk)
        except VendorUser.DoesNotExist:
            raise Http404

        documents = Document.objects.filter(vendor=vendor).order_by("sign_date")
        form = VendorDocumentSerializer()
        if not documents:
            messages.add_message(request, messages.INFO, "Договоров не найдено")
        context = {
            "documents": documents,
            "vendor": vendor,
            "serializer": form,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        serializer = VendorDocumentSerializer(
            data=request.data, context={"pk": vendor_pk}
        )
        if serializer.is_valid():
            context = {"errors": serializer.errors}
            serializer.save()
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VendorDocumentsDetailViewSet(StaffBaseViewSet):
    queryset = Document.objects.all()
    serializer_class = VendorDocumentSerializer

    def retrieve(self, request, *args, **kwargs):
        raise PermissionDenied

    def partial_update(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        document = get_object_or_404(Document, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorDocumentSerializer(document, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        object = self.get_object()
        object.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        document = get_object_or_404(Document, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorDocumentSerializer(
            document
        )
        serializer.type = "patch"
        serializer.action = reverse(
            "staff:vendor_document", kwargs={"vendor_pk": vendor_pk, "pk": pk}
        )
        context = {
            "form": serializer,
            "form_delete_button": True,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorCarsViewSet(StaffBaseViewSet):
    serializer_class = VendorCarSerializer
    queryset = Car.objects.all()
    template_name = "app/staff/vendor/cars.html"

    def list(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        try:
            vendor = VendorUser.objects.get(pk=vendor_pk)
        except VendorUser.DoesNotExist:
            raise Http404

        cars = Car.objects.filter(vendor=vendor).order_by("brand")
        form = VendorCarSerializer()
        if not cars:
            messages.add_message(
                request, messages.INFO, "Машин для данного пользователя не найдены"
            )
        context = {
            "cars": cars,
            "vendor": vendor,
            "serializer": form,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        serializer = VendorCarSerializer(data=request.data, context={"pk": vendor_pk})
        if serializer.is_valid():
            context = {"errors": serializer.errors}
            serializer.save()
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VendorCarsDetailViewSet(StaffBaseViewSet):
    queryset = Car.objects.all()
    serializer_class = VendorCarSerializer

    # renderer_classes = (JSONRenderer,)

    def retrieve(self, request, *args, **kwargs):
        raise PermissionDenied

    def partial_update(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        car = get_object_or_404(Car, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorCarSerializer(car, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        object = self.get_object()
        object.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        vendor_pk = kwargs["vendor_pk"]
        car = get_object_or_404(Car, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorCarSerializer(
            car
        )
        serializer.type = "patch"
        serializer.action = reverse(
            "staff:vendor_car", kwargs={"vendor_pk": vendor_pk, "pk": pk}
        )
        context = {
            "form": serializer,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class VendorDriversViewSet(StaffBaseViewSet):
    serializer_class = VendorDriverSerializer
    queryset = Driver.objects.all()
    template_name = "app/staff/vendor/drivers.html"

    def list(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        try:
            vendor = VendorUser.objects.get(pk=vendor_pk)
        except VendorUser.DoesNotExist:
            raise Http404

        drivers = Driver.objects.filter(vendor=vendor).order_by("name")
        form = VendorDriverSerializer()
        if not drivers:
            messages.add_message(
                request, messages.INFO, "Водители для данного пользователя не найдены"
            )
        context = {
            "drivers": drivers,
            "vendor": vendor,
            "serializer": form,
            "profile_menu": self.get_profile_menu(),
            "menu": self.get_menu(),
        }
        return Response(data=context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        serializer = VendorDriverSerializer(
            data=request.data, context={"pk": vendor_pk}
        )
        if serializer.is_valid():
            context = {"errors": serializer.errors}
            serializer.save()
            return Response(data=context, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VendorDriversDetailViewSet(StaffBaseViewSet):
    queryset = Driver.objects.all()
    serializer_class = VendorDriverSerializer

    def retrieve(self, request, *args, **kwargs):
        return redirect(reverse("staff:index"))
        # raise PermissionDenied

    def partial_update(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        driver = get_object_or_404(Driver, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorDriverSerializer(driver, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok", "content": serializer.data}
            return Response(data=context, status=status.HTTP_200_OK)
        context = {"errors": serializer.errors}
        return Response(context, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        object = self.get_object()
        object.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def get_form(self, request, *args, **kwargs):
        vendor_pk = kwargs["vendor_pk"]
        pk = kwargs["pk"]
        driver = get_object_or_404(Driver, vendor__pk=vendor_pk, pk=pk)
        serializer = VendorDriverSerializer(
            driver
        )
        serializer.type = "patch"
        serializer.action = reverse(
            "staff:vendor_driver", kwargs={"vendor_pk": vendor_pk, "pk": pk}
        )
        context = {
            "form": serializer,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class ManagerViewSet(StaffBaseViewSet):
    queryset = ManagerUser.objects.all()
    serializer_class = ManagerUserSerializer
    template_name = "app/staff/admin/manager_list.html"

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer()
        manager_list = ManagerUser.objects.all().order_by("user__username")
        context = {
            "manager_list": manager_list,
            "serializer": serializer,
            "menu": self.get_menu(),
        }
        return Response(context, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = ManagerUserSerializer(
            data=request.data, context={"request": request}
        )
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok"}
            return Response(data=context, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ManagerDetailViewSet(StaffBaseViewSet):
    queryset = ManagerUser.objects.all()
    serializer_class = ManagerUserSerializer
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    renderer_classes = (JSONRenderer,)
    template_name = "app/staff/vendor/profile.html"

    def retrieve(self, request, *args, **kwargs):
        object = self.get_object()
        # serializer = VendorUserSerializer()
        context = {
            "vendor": object,
            "menu": self.get_menu(),
            "profile_menu": self.profile_menu,
            # 'serializer':serializer,
        }
        return Response(context)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        object = self.get_object()
        try:
            user = User.objects.get(manageruser=object)
        except User.DoesNotExist:
            return Response(
                {"errors": "user_does_not_exist"}, status=status.HTTP_400_BAD_REQUEST
            )
        user.delete()
        # object.delete()
        context = {"pk": pk}
        return Response(data=context, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        object = self.get_object()
        serializer = self.get_serializer(object, request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {"data": "ok"}
            return Response(context, status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def get_form(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        object = self.get_object()
        type = request.GET.get("type")

        if type == "person":
            serializer = ManagerUserSerializer(
                object,
                exclude_fields=[
                    "is_active",
                    "username",
                    "password",
                    "confirm_password",
                ],
            )
        elif type == "password":
            serializer = ManagerUserSerializer(
                object,
                exclude_fields=["is_active", "username", "name", "email", "phone"],
            )
        else:
            raise PermissionDenied

        serializer.type = "patch"
        serializer.action = reverse("staff:manager_detail", kwargs={"pk": pk})
        context = {
            "form": serializer,
            "form_delete_button": True if type == "person" else False,
        }
        content = render_to_string(
            template_name="app/includes/forms/form.html", context=context
        )
        return JsonResponse({"data": "ok", "content": content})


class StaffNoticeListViewSet(NoticeListViewSet, StaffBaseViewSet):
    template_name = "app/staff/notices/list.html"

    def get_context_data(self, request, *args, **kwargs):
        context = super(StaffNoticeListViewSet, self).get_context_data(request)
        context.update({
            'menu': self.get_menu()
        })
        return context


class StaffNoticeViewSet(NoticeViewSet, StaffBaseViewSet):
    pass


class StaffRoomListViewSet(RoomListViewSet, StaffBaseViewSet):
    template_name = "app/staff/messages/room_list.html"

    def __init__(self, *args, **kwargs):
        self.form_action = reverse("staff:rooms")
        super(StaffRoomListViewSet, self).__init__(*args, **kwargs)

    def get_context_data(self, request, *args, **kwargs):
        context = super(StaffRoomListViewSet, self).get_context_data(request)
        context.update({
            'menu': self.get_menu()
        })
        return context


class StaffRoomDetailViewSet(RoomDetailViewSet, StaffBaseViewSet):
    template_name = "app/staff/messages/room_detail.html"

    def get_context_data(self, request, *args, **kwargs):
        context = super(StaffRoomDetailViewSet, self).get_context_data(request)
        context.update({
            'menu': self.get_menu()
        })
        return context
