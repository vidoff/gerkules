from rest_framework import serializers
from rest_framework.fields import SkipField
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PKOnlyObject
from app.models import Tender, Task
from users.models import ManagerUser, VendorUser, Document, Car, Driver
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.password_validation import get_default_password_validators

from collections import OrderedDict


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = (
            "name",
            "point_from",
            "point_to",
            "date_start",
            "date_end",
            "weight",
            "vehicle",
        )


class TenderSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = Tender

        fields = ("code", "name", "date_start", "pub_date", "bet_step", "tasks")


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "is_active")


class VendorUserSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    username = serializers.CharField(required=True, label="Имя пользователя(лат.)")
    password = serializers.CharField(required=True, label="Пароль для входа в систему")
    confirm_password = serializers.CharField(required=True, label="Повторите пароль")
    phone = serializers.CharField(required=True, label="Телефон пользователя")
    email = serializers.EmailField(required=True)
    is_active = serializers.BooleanField(default=True, label="Включить?")

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorUserSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    class Meta:
        model = VendorUser
        fields = (
            "is_active",
            "username",
            "password",
            "confirm_password",
            "name",
            "email",
            "phone",
            "user",
            "company_name",
            "company_addr",
            "company_post_addr",
            "company_inn",
            "company_phone",
            "company_seo",
            "company_site",
        )

    @staticmethod
    def serializer_validate_password(field_name, password, user=None):
        password_validators = get_default_password_validators()
        for validator in password_validators:
            try:
                validator.validate(password, user)
            # TODO не могу перехватить ValidationError
            except Exception as e:
                raise serializers.ValidationError({field_name: [e]})
        return True

    # код из to_representation добавлена проверка на выдачу паролей
    def to_representation(self, instance):
        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            if field.field_name == "password":
                continue
            elif field.field_name == "confirm_password":
                continue

            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            check_for_none = (
                attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            )
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)
        return ret

    def update(self, instance, validated_data):
        try:
            is_active = validated_data.pop("is_active")
        except KeyError:
            pass
        else:
            if isinstance(is_active, bool):
                user = User.objects.get(vendoruser=instance)
                user.is_active = is_active
                user.save()
        try:
            password = validated_data.pop("password")
            confirm_password = validated_data.pop("confirm_password")
        except KeyError:
            return super(VendorUserSerializer, self).update(instance, validated_data)

        if password is not None and confirm_password is not None:
            if password != confirm_password:
                raise ValidationError({"password": ["Пароли не совпадают"]})
            try:
                user = User.objects.get(vendoruser=instance)
            except User.DoesNotExist:
                raise ValidationError("пользователь не найден")
            if self.serializer_validate_password("password", password, user):
                user.set_password(password)
                user.save()

        return super(VendorUserSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        username = validated_data.pop("username")
        password = validated_data.pop("password")
        confirm_password = validated_data.pop("confirm_password")
        is_active = validated_data.pop("is_active")
        email = validated_data["email"]

        self.serializer_validate_password("password", password)

        if password != confirm_password:
            raise ValidationError(
                {"password": ["Пароли не совпадают"], "confirm_password": [""]}
            )

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            try:
                manager_group = Group.objects.get(name="vendors")
            except Group.DoesNotExist:
                raise ValidationError("группы vendors не существует")

            user = User.objects.create_user(
                username=username, password=password, email=email, is_active=is_active
            )
            manager_group.user_set.add(user)
            validated_data["user"] = user
            return super(VendorUserSerializer, self).create(validated_data)
        raise ValidationError(
            {"username": ["Пользователь с таким именем уже существует"]}
        )


class VendorDocumentSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorDocumentSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    def create(self, validated_data):
        pk = self.context.get("pk")
        try:
            vendor = VendorUser.objects.get(pk=pk)
        except VendorUser.DoesNotExist:
            raise ValidationError("Нет такого пользователя")

        validated_data["vendor"] = vendor
        return super().create(validated_data)

    class Meta:
        model = Document
        fields = (
            "publication",
            "num",
            "sign_date",
            "file_scan",
            "comment",
            "manager_comment",
        )


class VendorCarSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorCarSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def create(self, validated_data):
        pk = self.context.get("pk")
        try:
            vendor = VendorUser.objects.get(pk=pk)
        except VendorUser.DoesNotExist:
            raise ValidationError("Нет такого пользователя")

        validated_data["vendor"] = vendor
        return super().create(validated_data)

    class Meta:
        model = Car
        fields = (
            "publication",
            "brand",
            "gos_num",
            "trailer_num",
            "carrying",
            "axis_num",
            "unloading",
            "comment",
            "manager_comment",
        )


class VendorDriverSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(VendorDriverSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    def create(self, validated_data):
        pk = self.context.get("pk")
        try:
            vendor = VendorUser.objects.get(pk=pk)
        except VendorUser.DoesNotExist:
            raise ValidationError("Нет такого пользователя")

        validated_data["vendor"] = vendor
        return super().create(validated_data)

    class Meta:
        model = Driver
        fields = (
            "publication",
            "name",
            "passport",
            "license",
            "phone",
            "experience",
            "comment",
            "manager_comment",
        )


class ManagerUserSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    is_active = serializers.BooleanField(default=True, label="Включить?")
    username = serializers.CharField(required=True, label="Имя пользователя(лат.)")
    password = serializers.CharField(label="Пароль для входа в систему", required=True)
    confirm_password = serializers.CharField(label="Повторите пароль", required=True)
    phone = serializers.CharField(required=True, label="Телефон пользователя")
    email = serializers.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop("exclude_fields", None)
        super(ManagerUserSerializer, self).__init__(*args, **kwargs)
        if exclude_fields:
            for field_name in exclude_fields:
                self.fields.pop(field_name)

    class Meta:
        model = ManagerUser
        fields = (
            "is_active",
            "username",
            "password",
            "confirm_password",
            "name",
            "email",
            "phone",
            "user",
        )

    @staticmethod
    def serializer_validate_password(field_name, password, user=None):
        password_validators = get_default_password_validators()
        for validator in password_validators:
            try:
                validator.validate(password, user)
            # TODO не могу перехватить ValidationError
            except Exception as e:
                raise serializers.ValidationError({field_name: [e]})
        return True

    # код из to_representation добавлена проверка на выдачу паролей
    def to_representation(self, instance):
        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            if field.field_name == "password":
                continue
            elif field.field_name == "confirm_password":
                continue

            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            check_for_none = (
                attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            )
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                ret[field.field_name] = field.to_representation(attribute)
        return ret

    def update(self, instance, validated_data):
        try:
            is_active = validated_data.pop("is_active")
        except KeyError:
            pass
        else:
            if isinstance(is_active, bool):
                user = User.objects.get(manageruser=instance)
                user.is_active = is_active
                user.save()
        try:
            password = validated_data.pop("password")
            confirm_password = validated_data.pop("confirm_password")
        except KeyError:
            return super(ManagerUserSerializer, self).update(instance, validated_data)

        if password is not None and confirm_password is not None:
            if password != confirm_password:
                raise ValidationError({"password": ["Пароли не совпадают"]})
            try:
                user = User.objects.get(manageruser=instance)
            except User.DoesNotExist:
                raise ValidationError("пользователь не найден")
            if self.serializer_validate_password("password", password, user):
                user.set_password(password)
                user.save()

        return super(ManagerUserSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        username = validated_data.pop("username")
        password = validated_data.pop("password")
        confirm_password = validated_data.pop("confirm_password")
        is_active = validated_data.pop("is_active")
        email = validated_data["email"]

        self.serializer_validate_password("password", password)

        if password != confirm_password:
            raise ValidationError(
                {"password": ["Пароли не совпадают"], "confirm_password": [""]}
            )

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            try:
                manager_group = Group.objects.get(name="managers")
            except Group.DoesNotExist:
                raise ValidationError("группы managers не существует")

            user = User.objects.create_user(
                username=username,
                password=password,
                email=email,
                is_active=is_active,
                is_staff=True,
            )
            manager_group.user_set.add(user)
            validated_data["user"] = user
            return super(ManagerUserSerializer, self).create(validated_data)
        raise ValidationError(
            {"username": ["Пользователь с таким именем уже существует"]}
        )


class StaffTenderSerializer(serializers.ModelSerializer):
    confirmed = serializers.BooleanField(required=True)

    class Meta:
        model = Tender
        fields = (
            "confirmed",
        )
