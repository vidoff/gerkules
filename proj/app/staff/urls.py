from django.urls import path
from . import views
from app.staff.viewsets import *

app_name = "staff"

urlpatterns = [
    path("", views.StaffTenderList.as_view(), name="index"),
    # path("detail/<int:pk>/", views.StaffTenderDetail.as_view(), name="tender_detail"),
    path("detail/<int:pk>/close", views.CloseTender.as_view(), name="tender_close"),
    path(
        "detail/<int:pk>/",
        StaffTenderDetailViewSet.as_view(
            {
                "get": "retrieve",
                "patch": "partial_update",
            }
        ),
        name="tender_detail",
    ),
    path("archive/", views.StaffArchiveList.as_view(), name="tender_archive"),
    path("notices/", StaffNoticeListViewSet.as_view({
        "get": "list",
        "delete": "destroy",
    }), name="notices"),
    path("notices/<int:pk>/", StaffNoticeViewSet.as_view({
        "patch": "partial_update",
    }), name="notice_detail"),
    path("rooms/", StaffRoomListViewSet.as_view({
        "get": "list",
        "post": "create",
    }), name="rooms"),
    path(
        "rooms/<int:pk>/",
        StaffRoomDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "create",
            }
        ),
        name="room_detail",
    ),
    path(
        "profile/",
        ProfileViewSet.as_view({"get": "retrieve", "patch": "partial_update"}),
        name="profile",
    ),
    path(
        "vendors/",
        VendorViewSet.as_view({"get": "list", "post": "create", "put": "create"}),
        name="vendor_list",
    ),
    path(
        "vendors/<int:pk>/",
        VendorDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="vendor_detail",
    ),
    path(
        "vendors/<int:vendor_pk>/documents/",
        VendorDocumentsViewSet.as_view(
            {"get": "list", "put": "create", "post": "create"}
        ),
        name="vendor_documents",
    ),
    path(
        "vendors/<int:vendor_pk>/documents/<int:pk>/",
        VendorDocumentsDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="vendor_document",
    ),
    path(
        "vendors/<int:vendor_pk>/cars/",
        VendorCarsViewSet.as_view({"get": "list", "put": "create", "post": "create"}),
        name="vendor_cars",
    ),
    path(
        "vendors/<int:vendor_pk>/cars/<int:pk>/",
        VendorCarsDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="vendor_car",
    ),
    path(
        "vendors/<int:vendor_pk>/drivers/",
        VendorDriversViewSet.as_view(
            {"get": "list", "put": "create", "post": "create"}
        ),
        name="vendor_drivers",
    ),
    path(
        "vendors/<int:vendor_pk>/driver/<int:pk>/",
        VendorDriversDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="vendor_driver",
    ),
    path(
        "manager/",
        ManagerViewSet.as_view({"get": "list", "put": "create", "post": "create"}),
        name="manager_list",
    ),
    path(
        "manager/<int:pk>/",
        ManagerDetailViewSet.as_view(
            {
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="manager_detail",
    ),
]
