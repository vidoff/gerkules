from django.views.generic import View
from django.shortcuts import redirect, reverse
from django.http import Http404
from django.contrib import messages
from django.conf import settings

from app.models import Tender, Bet, Task
from app.views import TenderList, ArchiveList
from app.staff.viewsets import StaffMixin
from app.filters import ArchiveListFilter, StaffArchiveListFilter


class StaffTenderList(StaffMixin, TenderList):
    template_name = "app/staff/tender/tender.html"

    def get_queryset(self):
        qs = Tender.objects.exclude(status="archive").order_by("-pub_date")
        return qs

    def get_context_data(self, object_list=None, *args, **kwargs):
        if object_list is not None:
            for tender in object_list:
                if tender.status == "played" or tender.status == "closed":
                    bet_min_object = tender.bets.filter(publication=True).order_by('bet').first()
                    # tender.bet_min = tender.bets.filter(publication=True).aggregate(Min("bet"))["bet__min"]
                    if bet_min_object is not None:
                        tender.bet_min = bet_min_object.bet
                        tender.vendor = bet_min_object.user.vendoruser
        context = super(StaffTenderList, self).get_context_data(object_list=object_list, *args, **kwargs)
        context.update({"menu": self.get_menu()})
        return context


class StaffArchiveList(StaffMixin, ArchiveList):
    template_name = "app/staff/tender/archive.html"
    filterset_class = StaffArchiveListFilter

    def get_filterset_with_kwargs(self, request, queryset=None, **kwargs):
        filterset = StaffArchiveListFilter(request.GET, queryset)
        return filterset

    def get_queryset(self):
        qs = Tender.objects.filter(status="archive").order_by("-pub_date")
        return qs

    def get_context_data(self, object_list=None, *args, **kwargs):
        if object_list is not None:
            for tender in object_list:
                bet_min_object = tender.bets.filter(publication=True).order_by('bet').first()
                if bet_min_object is not None:
                    tender.bet_min = bet_min_object.bet
                    # tender.vendor = bet_min_object.user.vendoruser

        context = super(StaffArchiveList, self).get_context_data(object_list=object_list, *args, **kwargs)
        context.update({"menu": self.get_menu()})
        return context


class CloseTender(StaffMixin, View):
    def post(self, request, *args, **kwargs):
        pk = int(self.kwargs["pk"])
        if pk is not None:
            try:
                tender = Tender.objects.get(pk=pk)
            except Tender.DoesNotExist:
                raise Http404

            tender.status = "closed"
            tender.save()

            messages.add_message(request, messages.SUCCESS, "Тендер успешно закрыт")

        return redirect(reverse("staff:index"))
