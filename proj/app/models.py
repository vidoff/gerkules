from django.db import models
from django.utils import timezone
from django.utils.timezone import datetime
from django.contrib.auth.models import User
from datetime import timedelta, time
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, post_delete
import logging
# import ipdb

DEFAULT_BET_STEP = 500

TENDER_STATUS = (
    ("new", "новый"),
    ("played", "играет"),
    ("closed", "закрыт"),
    ("archive", "архив"),
)

TASK_UNLOADING = (
    ("back", "задняя"),
    ("side", "боковая"),
    ("top", "верхняя"),
    ("other", "любая"),
)


class Tender(models.Model):
    publication = models.BooleanField("Публиковать?", default=True)
    code = models.IntegerField("CODE", null=True, blank=True)
    name = models.CharField("Название тендера", max_length=255, blank=False, null=False)
    status = models.CharField(
        "Статус", max_length=20, choices=TENDER_STATUS, default="new"
    )
    pub_date = models.DateTimeField("Дата создания", default=timezone.now)
    date_start = models.DateTimeField(
        "Начало торгов", null=False, blank=False, default=timezone.now
    )
    date_end = models.DateTimeField(
        "Окончание торгов", blank=False, null=False, default=timezone.now
    )
    prolong = models.IntegerField(
        "Продление торгов в секундах", default=0, null=True, blank=True
    )
    bet_step = models.IntegerField("Шаг ставки", default=DEFAULT_BET_STEP)
    bet_win = models.IntegerField("Лучшая ставка", null=True, blank=True)
    bet_init = models.IntegerField("Начальная ставка", blank=True, null=True)
    confirmed = models.BooleanField("Подтвержден", default=False)
    comment = models.TextField("Комментарий", null=True, blank=True)
    user_winner = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True
    )
    vendor_car = models.ForeignKey(
        "users.Car",
        related_name="tenders",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    vendor_driver = models.ForeignKey(
        "users.Driver",
        related_name="tenders",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def get_bet_min(self):
        if self.bet_win is not None:
            # ipdb.set_trace()
            bet_min = self.bets.filter(publication=True, bet=self.bet_win).latest("bet")
            # bet_min = self.bets.filter(publication=True, bet=self.bet_win)
            if bet_min is not None:
                return bet_min
        return None

    def bet_enabled_by_status(self):
        if self.status in ("played", "new"):
            return True
        return False

    def achieved_data(self):
        if self.vendor_car is not None and self.vendor_driver is not None:
            return True
        return False

    def enabled_for_bet(self):
        if self.date_end > timezone.now():
            return True
        return False

    def enabled_for_show(self):
        return self.date_start < timezone.now() and self.date_end > timezone.now()

    def get_prolong(self):
        prolong = {
            "hour": self.prolong // 3600,
            "minute": self.prolong // 60 - (self.prolong // 3600) * 60,
        }
        return prolong

    def __str__(self):
        return "#{} {}".format(self.code, self.name)

    class Meta:
        verbose_name = "Тендер"
        verbose_name_plural = "Тендеры"


class Task(models.Model):
    publication = models.BooleanField("Публиковать?", default=True)
    name = models.CharField(
        "Наименование груза", max_length=255, null=False, blank=False
    )
    point_from = models.CharField(
        "Пункт отправления", max_length=50, null=True, blank=True
    )
    point_to = models.CharField(
        "Пункт назначения", max_length=50, null=True, blank=True
    )
    date_start = models.DateTimeField("Дата погрузки", null=True, default=timezone.now)
    date_end = models.DateTimeField("Дата выгрузки", null=True)
    weight = models.CharField("Вес", max_length=255, null=True, blank=True)
    vehicle = models.CharField(
        "Тип ТС", choices=TASK_UNLOADING, null=True, blank=True, max_length=20
    )
    tender = models.ForeignKey("Tender", related_name="tasks", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = "Задачи"


class Bet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tender = models.ForeignKey("Tender", on_delete=models.CASCADE, related_name="bets")
    bet = models.PositiveIntegerField("Ставка", null=False, blank=False)
    pub_date = models.DateTimeField("Дата/время ставки", default=timezone.now)
    publication = models.BooleanField("Публиковать?", default=True)

    class Meta:
        verbose_name = "Ставка"
        verbose_name_plural = "Ставки"


class Category(models.Model):
    name = models.CharField("Название", max_length=255)
    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="children",
        verbose_name="Родительская категория",
        on_delete=models.CASCADE,
    )
    slug = models.SlugField("URL", unique=True, default="")
    pub_date = models.DateTimeField("Дата создания", auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Page(models.Model):
    title = models.CharField("Заголовок", max_length=255, null=False)
    text = models.TextField("Текст", null=True, blank=True)
    is_active = models.BooleanField("Флаг активности", default=True)
    pub_date = models.DateTimeField("Дата создания", auto_now_add=True)
    slug = models.SlugField("URL", max_length=50, unique=True, default="")
    category = models.ForeignKey(
        "Category",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="pages",
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"


class SiteSettings(models.Model):
    tender_created_email_template = models.TextField(
        "Шаблон для письма на созданный тендер", null=True, blank=True
    )

    class Meta:
        verbose_name = "Настройки сайта"
        verbose_name_plural = "Настройки сайта"


# продление тендера, запись лога ставки
@receiver(pre_save, sender=Bet)
def bet_save(sender, instance, **kwargs):
    logger = logging.getLogger("gerkules.bets")
    less20 = False
    change_date_end = False
    tender = instance.tender
    if not instance.pk:
        diff = tender.date_end - timezone.now()
        diff_minutes = diff.seconds // 60
        old_date_end = tender.date_end
        if diff.days == 0 and 0 <= diff_minutes <= 20:
            less20 = True
            if (
                not tender.bets.filter(publication=True).count()
                or not tender.bets.filter(user=instance.user, publication=True).count()
            ):
                change_date_end = True
                prolong = 60 * 20
                tender.date_end = tender.date_end + timedelta(0, prolong)
                try:
                    tender.prolong = tender.prolong + prolong
                except TypeError:
                    tender.prolong = prolong

        log = """
        less20: {less}
        diff (minutes): {diff},
        timezone.now: {timezone}
        old_date_end: {old_date_end}
        tender_date_end: {tender_date_end}
        bet: {bet},
        tender_code: {tender_code},
        username: {username},
        change_date_end_by_user: {change_date_end}
        """.format(
            less=less20,
            diff=diff_minutes,
            timezone=timezone.now().strftime("%Y-%m-%d %H:%M:%S"),
            old_date_end=old_date_end.strftime("%Y-%m-%d %H:%M:%S"),
            tender_date_end=tender.date_end.strftime("%Y-%m-%d %H:%M:%S"),
            bet=instance.bet,
            tender_code=tender.code,
            username=instance.user.username,
            change_date_end=change_date_end
        )
        logger.info(log)


# DEV
# удаление ставок у играющего тендера
# установить статус тендера на новый
@receiver(post_delete, sender=Bet)
def tender_set_bet_default(sender, instance, **kwargs):
    tender = instance.tender
    if tender.status == "played":
        bets = Bet.objects.filter(tender=tender, publication=True)
        if not bets:
            tender.bet_win = None
            tender.status = "new"
            tender.save()
