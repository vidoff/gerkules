from __future__ import absolute_import, unicode_literals
from celery import task

from .models import Tender, Bet
from django.utils import timezone
from datetime import timedelta
from events.models import create_notice, Event
from django.shortcuts import reverse


@task(name="tenders_life_time")
def tenders_life_time():
    tender_list = Tender.objects.filter(status__in=['new', 'played'])
    for tender in tender_list:
        if tender.date_end <= timezone.now():
            if tender.status == "new":
                tender.status = "archive"

                url = reverse("staff:tender_detail", kwargs={"pk": tender.pk})
                link = "<a href='{}'>{}</a>".format(url, tender.name)
                message = "Тендер №{} закрыт без ставок, переведен в архив".format(tender.code)
                create_notice("tender_archived", {"title": message, "link": link}, "staff")

            elif tender.status == "played":
                try:
                    bet = Bet.objects.get(tender=tender, bet=tender.bet_win, publication=True)
                except Bet.DoesNotExist:
                    tender.status = "archive"
                    url = reverse("staff:tender_detail", kwargs={"pk": tender.pk})
                    link = "<a href='{}'>{}</a>".format(url, tender.name)
                    message = "Тендер №{} закрыт без ставок, переведен в архив".format(tender.code)
                    create_notice("tender_archived", {"title": message, "link": link}, "staff")
                    continue
                else:
                    tender.user_winner = bet.user
                    tender.status = "closed"

                    url = reverse("staff:tender_detail", kwargs={"pk": tender.pk})
                    link = "<a href='{}'>{}</a>".format(url, tender.name)
                    message = "Определился победитель по тендеру {}, поставщик: {}".format(tender.code,
                                                                                           bet.user.vendoruser.company_name)
                    create_notice("tender_closed", {"title": message, "link": link}, "staff")

                    url = reverse("vendor:tender_detail", kwargs={"pk": tender.pk})
                    link = "<a href='{}'>{}</a>".format(url, tender.name)
                    message = "Ваша ставка выиграла, тенедер №{}".format(tender.code)
                    create_notice("tender_closed", {"title": message, "link": link, "user": bet.user})
            tender.save()


@task(name="notices_clear_older_week")
def notices_clear_older_week():
    old_events = Event.objects.filter(pub_date__lte=timezone.now() - timedelta(7))
    old_events.delete()
