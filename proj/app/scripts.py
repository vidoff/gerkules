from users.models import VendorUser, VendorSettings
from django.contrib.auth.models import User


def fillSettings():
    vendors = VendorUser.objects.all()
    for vendor in vendors:
        try:
            settings = vendor.settings
        except VendorSettings.DoesNotExist:
            VendorSettings.objects.create(vendor=vendor)
            print("create settings")
        else:
            print("exist")
