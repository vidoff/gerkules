from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def save_page_get_params(context):
    get_param = []
    request = context['request']
    for key, v in request.GET.items():
        value_list = request.GET.getlist(key)
        get_param.extend(['%s=%s' % (key, val) for val in value_list if key != 'page'])
    return '&'.join(get_param)


@register.simple_tag
def get_bootstrap_alert_msg(tags):
    return 'danger' if tags == 'error' else tags


@register.simple_tag
def define(val=None):
    return val


@register.simple_tag
def count_notices_active(notices):
    active = notices.filter(is_active=True)
    if active.count() > 0:
        return active.count()
    return None

@register.filter
def sort_by(queryset, order):
    return queryset.order_by(order)

