from django.db import models


class Country(models.Model):
    name = models.CharField("Название", max_length=50)
    crt_date = models.DateTimeField("Дата создания", auto_now_add=True)

    @classmethod
    def get_or_create(cls, name):
        try:
            instance = cls.objects.get(name=name)
        except cls.DoesNotExist:
            instance = cls.objects.create(name=name)
        return instance

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Страна"
        verbose_name_plural = "Страны"


class District(models.Model):
    name = models.CharField("Название", max_length=50)
    is_active = models.BooleanField("Флаг активности", default=True)
    crt_date = models.DateTimeField("Дата создания", auto_now_add=True)

    @classmethod
    def get_or_create(cls, name):
        try:
            instance = cls.objects.get(name=name)
        except cls.DoesNotExist:
            instance = cls.objects.create(name=name)
        return instance

    def __str__(self):
        return self.name

    def get_regions(self, is_active=True):
        regions = self.regions.filter(is_active=is_active).order_by('name')
        return regions

    class Meta:
        verbose_name = "Регион"
        verbose_name_plural = "Регионы"


class Region(models.Model):
    is_active = models.BooleanField("Флаг активности", default=True)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, related_name="regions"
    )
    district = models.ForeignKey(
        District,
        on_delete=models.CASCADE,
        related_name="regions",
        null=True,
        blank=True,
    )
    name = models.CharField("Название", max_length=255)
    crt_date = models.DateTimeField("Дата создания", auto_now_add=True)

    @classmethod
    def get_or_create(cls, name):
        try:
            instance = cls.objects.get(name=name)
        except cls.DoesNotExist:
            country = Country.get_or_create(name='Россия')
            district = District.get_or_create(name='Другое')
            instance = cls.objects.create(name=name, district=district, country=country)
        return instance

    def __init__(self, *args, **kwargs):
        self.geo_city = City.objects.all()
        return super(Region, self).__init__(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_cities(self, is_active=True):
        cities = self.geo_city.filter(region__pk=self.pk).order_by('name')
        return cities

    class Meta:
        verbose_name = "Область/Край"
        verbose_name_plural = "Области"


class City(models.Model):
    is_active = models.BooleanField("Флаг активности", default=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name="cities")
    name = models.CharField("Название", max_length=200)
    crt_date = models.DateTimeField("Дата создания", auto_now_add=True)

    def __str__(self):
        return self.name

    @classmethod
    def get_or_create(cls, name):
        try:
            instance = cls.objects.get(name=name)
        except cls.DoesNotExist:
            region = Region.get_or_create(name='Другое')
            instance = cls.objects.create(name=name, region=region)
        return instance

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"
