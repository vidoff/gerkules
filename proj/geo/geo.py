import psycopg2
from .models import Country, Region, City


def config():
    try:
        conn = psycopg2.connect("dbname=geo user=geo password=123")
    except Exception as e:
        print(e)

    return conn


def import_country():
    conn = config()
    cur = conn.cursor()
    # cur.execute('SELECT version()')
    cur.execute("""SELECT * from geo_country""")
    rows = cur.fetchall()

    #########COUNTRY
    counties = []

    count = 0
    for row in rows:
        counties.append(Country(name=row[1], crt_date=row[2]))
        print("ok: {}".format(count))
        count += 1

    Country.objects.bulk_create(counties)

    cur.close()
    conn.close()


def import_region():
    conn = config()
    cur = conn.cursor()
    # cur.execute('SELECT version()')
    cur.execute("""SELECT * from geo_region""")
    rows = cur.fetchall()

    try:
        country = Country.objects.get(id=1)
    except Country.DoesNotExist:
        return

    #########REGIONS
    data = []
    count = 0
    for row in rows:
        data.append(Region(name=row[2], crt_date=row[3], country=country))
        # print("ok: {}".format(count))
        print("{},{},{}, ok: {}".format(row[0], row[1], row[2], count))
        count += 1

    Region.objects.bulk_create(data)

    cur.close()
    conn.close()


def import_city():
    conn = config()
    cur = conn.cursor()
    # cur.execute('SELECT version()')


    try:
        country = Country.objects.get(id=1)
    except Country.DoesNotExist:
        return

    regions = Region.objects.all()
    count =0
    data = []
    for r in regions:
        id = r.pk
        id -=1
        print("{} {}".format(id, r.name))

        if id == 78:
            id == 88

        cur.execute("SELECT * from geo_city WHERE region_id={}".format(id))
        rows = cur.fetchall()

        for row in rows:
            data.append(City(name=row[2], crt_date=row[3], region=r))
            # print("ok: {}".format(count))
            print("{},{},{}, ok: {}".format(row[0], row[1], row[2], count))
            count += 1
        print("\n")

    City.objects.bulk_create(data)
    cur.close()
    conn.close()
