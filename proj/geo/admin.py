from django.contrib import admin
from .models import Country, Region, City, District
admin.site.register(Country)
admin.site.register(Region)
admin.site.register(City)
admin.site.register(District)

